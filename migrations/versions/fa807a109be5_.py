"""empty message

Revision ID: fa807a109be5
Revises: 43af422ba5ab
Create Date: 2025-01-06 16:03:32.061846

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'fa807a109be5'
down_revision = '43af422ba5ab'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('user', sa.Column('bke_certification', sa.Boolean(), nullable=False, default=False, server_default='false'))


def downgrade():
    op.drop_column('user', 'bke_certification')
