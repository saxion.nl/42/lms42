"""empty message

Revision ID: bb959467c552
Revises: 693ae53d1dad
Create Date: 2024-09-06 09:42:48.187586

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'bb959467c552'
down_revision = '693ae53d1dad'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('fk_registration_template_group_id_group', 'registration_template', type_='foreignkey')
    op.create_foreign_key(op.f('fk_registration_template_group_id_group'), 'registration_template', 'group', ['group_id'], ['id'], ondelete='CASCADE')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(op.f('fk_registration_template_group_id_group'), 'registration_template', type_='foreignkey')
    op.create_foreign_key('fk_registration_template_group_id_group', 'registration_template', 'group', ['group_id'], ['id'])
    # ### end Alembic commands ###
