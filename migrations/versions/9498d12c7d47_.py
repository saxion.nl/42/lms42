"""empty message

Revision ID: 9498d12c7d47
Revises: 4ed2bbb00751, fe064bd4f3a8
Create Date: 2024-07-01 11:28:41.843503

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '9498d12c7d47'
down_revision = ('12604884ac2e')
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('user_flashcard',
        sa.Column('user_id', sa.Integer(), nullable=False),
        sa.Column('flashcard_id', sa.String(), nullable=False),
        sa.Column('node_id', sa.String(), nullable=False),
        sa.Column('repetitions', sa.Integer(), nullable=False),
        sa.Column('interval', sa.Integer(), nullable=False),
        sa.Column('next_view', sa.Date(), nullable=True),
        sa.Column('easiness_factor', sa.Float(), nullable=False),
    )
    op.create_index('flashcard_id_next_view', 'user_flashcard', ['user_id', 'next_view'], unique=False)
    op.create_foreign_key(op.f('fk_user_flashcard_user_id_user'), 'user_flashcard', 'user', ['user_id'], ['id'])
    

def downgrade():
    op.drop_index('flashcard_id_next_view', table_name='user_flashcard')
    op.drop_table('user_flashcard')
    op.drop_constraint(op.f('fk_user_flashcard_user_id_user'), 'user_flashcard', type_='foreignkey')
