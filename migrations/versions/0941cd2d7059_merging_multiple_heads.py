"""Merging multiple heads

Revision ID: 0941cd2d7059
Revises: 827903ee7012, fa807a109be5
Create Date: 2025-01-31 09:09:12.933071

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0941cd2d7059'
down_revision = ('827903ee7012', 'fa807a109be5')
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
