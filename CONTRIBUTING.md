# Contributing to LMS42

First off, thanks for taking the time to contribute!

The following is a set of guidelines for contributing to LMS42, [hosted on GitLab](https://gitlab.com/saxionnl/42/lms42). These are mostly guidelines, not rules. Use your best judgement, and feel free to propose changes to this document in a Merge Request.

#### Table Of Contents

- [Code of Conduct](#code-of-conduct)
- [How Can I Contribute?](#how-can-i-contribute)
  * [Reporting Bugs](#reporting-bugs)
  * [Suggesting Enhancements](#suggesting-enhancements)
  * [Contributing Code](#contributing-code)
- [Styleguide](#styleguide)
  * [Python](#python)


## Code of Conduct

This project and everyone participating in it is governed by the [LMS42 Code of Conduct](CODE-OF-CONDUCT.md). By participating, you are expected to uphold this code. Please report unacceptable behavior to [t.e.sealy@saxion.nl](mailto:t.e.sealy@saxion.nl).


## How Can I Contribute?

### Reporting Bugs

This section guides you through submitting a bug report for LMS42. Following these guidelines helps maintainers and the community understand your report :pencil:, reproduce the behavior :computer: :computer:, and find related reports :mag_right:.

Before creating bug reports, please perform a cursory search in the [GitLab issue list](https://gitlab.com/saxionnl/42/lms42/-/issues) to see if the problem has already been reported. If it has **and the issue is still open**, add a comment to the existing issue instead of opening a new one. If you find a **closed** issue that seems like it is the same thing that you're experiencing, open a new issue and include a link to the original issue in the body of your new one.

When you are creating a bug report, please include as many details as possible:

- A clear and descriptive title
- Detailed steps to reproduce 
- Expected behavior
- Actual behavior
- Screenshot demonstrating the problem, if applicable
- If you're using the production deployment (on sd42.nl) or a development instance (please list the Git SHA)
- Anything else that may be relevant


### Suggesting Enhancements

This section guides you through submitting an enhancement suggestion for LMS42, including completely new features and minor improvements to existing functionality. Following these guidelines helps maintainers and the community understand your suggestion :pencil: and find related suggestions :mag_right:.

Before creating enhancement suggestions, please perform a cursory search in the [GitLab issue list](https://gitlab.com/saxionnl/42/lms42/-/issues?scope=all&state=alls) to see if the feature has already been proposed before. Also include **closed** issues in your search. If the issue is still **open**, you may want to add your specific reasons for wanting the feature as a comment. If it is **closed**, the comments will hopefully explain why this feature was not implemented. If you have new information that may change that decision, you can open a new issue and include a link to the original issue in the body of your new one.

When you are creating a feature request, please include as many details as possible:

- A clear and descriptive title
- Summary
- Motivation
- Anything else that may be relevant


### Contributing code

#### Request access

On the project's [main GitLab page](https://gitlab.com/saxionnl/42/lms42) click the three dots (overflow menu) in the top right, and select 'Request access'. One of the maintainers (teachers) will have to manually approve you; ask one of them about it if this is holding you back. You'll need the access when you're at the *Create an empty Merge Request* step, below.

#### Pick an issue to work on

Unsure where to begin contributing to LMS42? You can start by looking through the `first-issue` and `help-wanted` issues:

* [First issues](https://gitlab.com/saxionnl/42/lms42/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=first-issue&first_page_size=100) - issues which should only require a few lines of code, and a test or two.
* [Help wanted issues](https://gitlab.com/saxionnl/42/lms42/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=help-wanted&first_page_size=100) - issues which should be a bit more involved than `beginner` issues.

If you want to contribute something else, please create an issue first.

#### Get feedback on your solution

If the issue is even a little vage, if it's unclear if the maintainers find the feature desirable, or if the issue requires choices (about UX or technical approach) to be made: post a detailed description of your imagined solution (possbly including a quick mock-up screenshot) to the comments, and wait for some positive feedback **before** you actually dive into the code. Yes, waiting for feedback can take a couple of days (during which you can do other things), but may save you a week of working in the wrong direction!

#### Create an empty *Merge Request*

When you start working on an issue, click the 'Create Merge Request' button that should be visible below the issue description (*if* you have been given access to the project). This will...

- Create a new feature branch that you can push your commits to after you've created them.
- Create the actual *Merge Request*, which is a 'formal' request to the maintainers of the project to merge your commits into the *master* branch (something which only they are allowed to do). The request will initially be marked as a *draft* (so that the maintainers know that you're still working on this), as it's still empty.
- Show within the issue that it is being worked on, providing a link to the *Merge Request*.

Next, you should be able to do a local checkout of your feature branch.

#### Getting started with the LMS42 source code

The [README.md](README.md) contains the most important information on getting an LMS42 development environment up and running. It also provides you with a brief tour of the directory structure, which will hopefully help you get your bearings quickly. Documentation could most definitely see some improvement though (hint!), but in the mean time you should feel free to liberally ask questions to the core contributors. Preferably, this should be done using an `@mention` in a comment to the related issue or Merge Request.

#### Stick with what's there

As with any project, contributions should stick to existing project conventions and used technologies as much as possible. In particular, do not introduce new libraries or new ways of doing things unless there's a very good reason for it. And even if there is, you'll probably want to discuss it with the maintainers first, to prevent disappoints when submitting your merge request.

The default option for new user interfaces should be plain old Flask with Jinja-rendered HTML and WTForms-generated forms. If more interactivity would really help the user experience a lot, consider if [HTMX](https://htmx.org/docs/) (which is already a dependency) may do the job. If you really need some custom JavaScript, try to keep it to a minimum and strive for graceful degradation (meaning things also work somewhat without JavaScript enabled). This also helps testability, as our testing framework does not support JavaScript.

#### Testing

As the testing framework has only recently been set up, much existing functionality is lacking tests. For any new and updated functionality, we *do* require automated tests *where reasonable*. If in doubt, ask the maintainers. Please refer to the [test documentation](tests/README.md).

#### Database migrations

A single merge request should generally contain at most one database migration file. Refer to the [Database section in README.md](README.md#database) for more information.

#### Add your name for posterity

If this is your first contribution, no matter how small, please feel free to add your name to the bottom of the `AUTHORS` file.

#### Offering your code for review

Once you have written the code that resolves the issue you're working on:

1. Git commit all your changes.
2. Merge the latest changes from the remote *master* branch (if any) into your local feature branch (`git pull origin/master`), and resolve merge conflicts, if any.
3. If merging causes a 'multiple heads' error from the Alembic migrations manager, you can view a list of heads using `bin/sd42 run alembic heads`. One of these should be your migration file. Change the `down_revision` in this file to point at the other head revision to resolve the problem.
4. Verify that the Ruff linter (a programs that checks for code consistency problems) is happy with the new code that you've created. The Ruff configuration for this project is provided in `pyproject.toml`, and should be used by Ruff automatically. You can either use a Ruff-plugin for your IDE (make sure to put it in `--preview` mode), or you can run Ruff manually on a file like this: `ruff check --preview lms42/email.py`.
5. Verify that the automated tests (`bin/sd42 test`) all pass.
6. If you needed to make any changes in the steps above, go back to step 1.
7. Verify that the difference between your local version and the master branch (`git diff origin/master..HEAD`) *only* contains changes relevant to your issue. Make sure that unrelated code is not changed. Also make sure that you've not accidentally committed debug statements or other such messiness. If you need to make changes to fix this, go back to 1.
8. Push your changes to your remote feature branch on GitLab.
9. Make sure your merge request description is accurate and comprehensive. In case you're proposing visual changes, including a screenshot (and perhaps a 'before' screenshot) can be very helpful for reviewers!
10. Request feedback.
- If you want peer feedback from a specific developer (let's say you're a student doing the Open Source module), assign her/him as a reviewer.
- If you think the merge request is ready to be merged, mark it as 'Ready' (instead of 'Draft'), through the three dots overflow menu. (If you're a student doing the Open Source module you will also want to start and submit the 'Maintainer review' assignment as this point.) A maintainer will review your work, and possibly merge it.
11. In case you get feedback, put your *Merge Request* back in 'Draft' mode (if the maintainer hasn't done so aready), try to resolve the remaining problems, and go back to step 1.
