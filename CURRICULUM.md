## Curriculum file format documentation

This is work-in-progress is still very much incomplete. Help is more than welcome!

### Directory structure

TODO.

### overview/periods.yaml

Contains the periods into which the curriculum is organized, with for each a list of module names. (See the next section for where we get further module info.)

A module name string may also consist of multiple parts, in this form:

```
current-module-name some-legacy-key=old-module-name another-legacy-key=even-older-module-name
```

This means that we'd normally see the `current-module-name` module in this place, but if the user has one of the legacy plans enabled (on their profile page) a different module can be shown instead.

### modules/<name>.yaml

Contains meta information about a module. TODO.

An incomplete list of properties:
- `name`: TODO.
- `short`: TODO.
- `description`: TODO.
- `index`: TODO.
- `owner`: TODO.

### node.yaml

Contains meta information for a node (a lesson or an exam). It may also include the assignment(s), unless the node is an exam.

An incomplete list of properties:

- `name`: Name as visible on `/curriculum`.
- `description`: Description as visible on `/curriculum`. This may contain additional details on the topic or a hint on what the assignment will be about.
- `days`: How many days a students should aim to spend on this assignment. For exams that don't have `allow_longer` set to 0, this is also the hard deadline. Defaults to 1.
- `ects`: Number of credits a student receives on successful completion of this lesson. Settings this marks the lesson as an exam (project). 
- `miller`: Miller taxonomy level for exams. `kh` is Knows How. `sh` is Shows How`. Etc. This appears currently unused in the code base.
- `goals`: An object, where the keys are learning goal identifiers, and the values are either numbers (the relative weight of the learning goal in this assignment), or objects containing:
    - An optional `title` key with a string value that is the one-line description of this goal. This should be short but specific, end in a dot, and in your head you should be able to prefix it with 'The student can...'. For example: 'Use backtracking to find optimal solutions to a class of problems.'. When a goal is featured in multiple assignments (which is usually the case as goals should generally be both in a regular assignment and in an exam), only one of them (usually the exam) should have the `title` key, to stick to one source of truth.
    - Optional keys for each of the Bloom taxonomy levels: 'know', 'comprehend', 'apply', 'analyze', 'evaluate' and 'create'. The value is a number that determines the relative weight of this Bloom level for this learning goal. When using the shortcut mentioned above (the learning goal key having just a number as a value), 'apply' is assumed.
- `assignment`: A shortcut for `assignment1`.
- `assignment[n]`: The actual assignment text and rubrics for the `n`th (1-based) assignment variant. Alternatively (and mandatory for exams) this can be in a file called `assignment[n].yaml`. TODO: The recursive YAML data structure for describing assignment is yet to be documented, sorry!
- `resources`: Similar to the above, but shared between assignment variants and placed under a 'Learning Resource' section header.
- `intro`: Similar to the above, but shared between assignment variants and without any section header.
- `TODO`: A string or a list of strings with teacher notes on what still needs to be done for this assignment.
- `public_for_students`: Set to `true` if logged-in students should be able to see the assignment without starting it.
- `public_for_externals`: Set to `true` when anonymous viewers should be able to see the assignment.
- `type`: TODO.
- `start_when_ready`: When set to `true` students are encouraged to start this assignment not when they start working on it, but when they're ready to submit it. This is useful for assignments that require a lot of waiting around on third parties (such as applying for an internship). Perhaps we can transition to a better solution, allowing students to pause this assignment (overriding the limit of 1 paused assignment).
- `startable`: When `false`, a node is always marked as 'passed'.
- `avg_attempts`: The number of times we expect students to have to redo the assignment on average, each time taking `days` to submit. This is used to calculate progress points (wich are nominal days) for an assignment. Defaults to 1.5 for regular assignment and to 1 for exams.
- `allow_longer`: Exams usually have a hard deadline set to `days` after the start. When this is true, there is no hard deadline.
- `ignore`: True when an assignment is (temporarily) disabled.
- `hidden`: True when an assignment is not visible in the overview. Use to phase out assignments that still have some grading left.
- `grading`: Normally assignments are required to have rubrics. Setting this to `false` overrides that check and hides the grading form.
- `upload`: When set to `false` students are not asked/unabled to upload any work for this assignment.
- `feedback`: When `False`, don't show the feedback form. Defaults to `True`.
- `wip`: When `True`, show a *Not ready* indicator, disallow starting the module, and make fatal errors for this node non-fatal. Defaults to `False`.
- `autopass`: When `True`, an attempt will be immediately marked as *passed* when the students submits it with finished==yes. This flag is only allowed when the assignment has no objectives. In fact, `autopass` defaults to `True` when there are no objectives and `upload` is `False`. In other cases, the default is `False`.
- `grading_upload`: When `True`, allow teachers to upload files during grading, which will become part of the exam dossier. When `"required"` teachers are forced to upload at least one file.
- `grade_without_student`: When `True`, a grading teacher will be assigned for attempts even when the student is not present. This is enabled by default for exams.
- `variant_penalty_<n>`: For exams, this tells us that when choosing a variant, this particular variant should get a certain numeric penalty. The value of 1 would cause this to be chosen only when all other variant have already been done.
- `provided_by`: A list of node IDs, or a single String node ID, that if any one of them is passed, this assignment will also be marked as passed.

### assignment[n].yaml

TODO.

#### Objectives

TODO.

An incomplete list of properties:

- `title`: TODO.
- `text`: TODO.
- `0`, `1`, `2`, `3`, `4`: The rubric ratings. At least one of these (or `scale` or `grading_instructions`) needs to be specified. (Or, as happens frequently, included from `reference.yaml` using a `^merge`.)
- `weight`: TODO. Not allowed for exams - use map instead.
- `map`: TODO. Required for exam objectives.
- `code`: TODO.
- `bonus`, `malus`: TODO.
- `scale`: When set to `10`, grading happens with a number 1..10, instead of on a 5-point scale.
- `grading_instructions`: Markdown text shown only to teachers for grading.

#### Alternatives

`title` and `text` fields in objectives can contain *alternatives*, causing students to randomly get (small) variations on an assignment. This can be especially useful for exams. They can be used like this:

```yaml
-
    title: Implement [widget:text]{a Text}[widget:image]{an Image} widget
    text: |
        Use your awesome powers to implement a widget that allow the user to view [widget:text]{many lines of text}[widget:image]{a png or jpg image}.
    4: This is a rubric.
```

#### Flashcards
Flashcards can be added at any location in the assignment?.yaml or node.yaml files. Once the student answers a flashcard, it will be added to its deck for spaced repetition.
```yaml
-
    question: |
        Which prefix should be used before a string (in the place of the _ below) so a variable can be inserted using '{}'?
        ```python
        name = "Jantje"
        print(_"Hello {name}")
        ```
    answer: f
    id: fstring1
```
- The question should consist of markdown text.
- `answer` is either a string or a list of strings, containing correct answers. They are case insensitive.
- The `id` is optional en defaults to the first answer. It must be unique within the node, and is used to track a flashcard in the database.

