# poetry run python scripts/attempt_working_hours.py

from lms42.app import db
from lms42.models.attempt import Attempt

for attempt in Attempt.query.all():
    attempt.total_hours = None if attempt.submit_time is None else attempt.hours

db.session.commit()
