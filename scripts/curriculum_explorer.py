import pickle
import os
import json


with open(os.path.dirname(__file__) + "/../curriculum.pickle", "rb") as file:
    data = pickle.load(file)


def value_to_str(value):
    if isinstance(value, dict):
        return "{" + str(len(value)) + " pairs}"
    if isinstance(value, list):
        return "[" + str(len(value)) + " values]"
    return str(json.dumps(value))


def explore(data, path):
    while True:
        print("\n\n" + " > ".join(path) + "\n")
        lookup = {}
        if isinstance(data, dict):
            for i, (k, v) in enumerate(data.items(), 1):
                lookup[str(i)] = k
                print(f"{i}. {k} => {value_to_str(v)}")

        elif isinstance(data, list):
            for i, v in enumerate(data):
                lookup[str(i)] = i
                print(f"{i}. {value_to_str(v)}")

        else:
            print(repr(data))
            return

        print("<enter>. go up")
        choice = input("\n> ").strip()
        if not choice:
            return
        if choice in lookup:
            explore(data[lookup[choice]], path + [str(lookup[choice])])


explore(data, ["Curriculum"])
