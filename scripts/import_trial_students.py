# bin/sd42 run python scripts/import_trial_students.py anja-file.xlsx
import sys
import datetime
from openpyxl import load_workbook
from lms42.app import db
from lms42.models.trial import TrialStudent

wb = load_workbook(sys.argv[1])
ws = wb.active

while True:
    match (
        input("Send trial day invitation email to new applicants? [y/n] ")
        .strip()
        .lower()
    ):
        case "y":
            send_email = True
            break
        case "n":
            send_email = True
            break

MAPPING = {
    "Roepnaam": "first_name",
    "Achternaam": "last_name",
    "Voorvoegsel": "last_name_prefix",
    "Studentnummer": "student_id",
    "Ontvangstdatum aanmelding": "submit_date",
    "Mutatiedatum aanmelding": "modify_date",
    "Locatie": "city",
    "Instroomdatum": "enrollment_date",
    "Status van aanmelding (omschrijving)": "enrollment_state",
    "Toelatingscategorie (omschrijving)": "admission_state",
    "Nationaliteit (Omschrijving)": "nationality",
    "Plaats (Correspondentie)": "correspondence_city",
    "Land (Correspondentie)": "correspondence_country",
    "Email privé": "email",
    "Omschrijving taal": "language",
    "Toelatende vooropleiding omschrijving": "prior_education",
    "Mob. telefoon": "phone",
}

db.session.execute(
    "update trial_student set admission_state='retracted', enrollment_state='retracted' where admission_state is not null or enrollment_state is not null"
)
new_students = []
headers = None

for row in ws.values:
    if not row[1]:  # Skip empty lines
        continue

    if not headers:
        headers = row
        continue

    row = {
        name.strip().lower().replace("  ", " "): row[col]
        if type(row[col]) != str
        else row[col].strip()
        for col, name in enumerate(headers)
        if name
    }
    row = {
        nice_name: row[ugly_name.lower()] for ugly_name, nice_name in MAPPING.items()
    }

    if row["last_name_prefix"]:
        row["last_name"] = row["last_name_prefix"] + " " + row["last_name"]
    del row["last_name_prefix"]
    row["email"] = row["email"].lower()

    for name in (
        "language",
        "correspondence_country",
        "correspondence_city",
        "nationality",
    ):
        row[name] = row[name].capitalize()

    for name in row:
        if name.endswith("_date"):
            row[name] = datetime.datetime.strptime(row[name], "%d-%m-%Y").date()

    row["student_id"] = int(row["student_id"])

    student = existing = (
        TrialStudent.query.filter(TrialStudent.email.ilike(row["email"])).first()
        or TrialStudent.query.filter_by(student_id=row["student_id"]).first()
        or TrialStudent.query.filter_by(
            first_name=row["first_name"], last_name=row["last_name"], student_id=None
        ).first()
    )

    if not student:
        student = TrialStudent()
        new_students.append(student)
        db.session.add(student)
    else:
        # Reset manual probability when a user retracts
        if (
            student.enrollment_state != "retracted"
            and row.get("enrollment_state") == "retracted"
        ):
            student.manual_probability = None

    for field, value in row.items():
        if getattr(student, field) != value:
            setattr(student, field, value)

    db.session.commit()

    if not existing:
        print(student.email)
        if send_email:
            student.send_invite_email()
