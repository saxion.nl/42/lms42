#!/bin/env python3

import sys
import yaml
import os
import subprocess

LMS_DIR = os.path.join(os.path.dirname(__file__), "..")
sys.path.append(LMS_DIR)

from lms42.app import db, app
from lms42.models import keyvalue
from lms42.models.attempt import Attempt

if len(sys.argv) == 2 and sys.argv[1] == "--update-db":
    processed = keyvalue.get("renames_processed") or 0

    with open("assignments/renames.yaml") as file:
        renames = yaml.load(file, Loader=yaml.FullLoader) or []

    for index, item in enumerate(renames):
        assert len(item) == 1
        if index < processed:
            continue
        old, new = next(iter(item.items()))
        print(f"Renaming {old} --> {new}")

        db.session.execute(
            """
            update attempt a1
            set node_id=:new
            where node_id=:old and not exists (
                select 1
                from attempt a2
                where a2.node_id=:new and a2.number=a1.number and a2.student_id=a1.student_id
            )
        """,
            {"new": new, "old": old},
        )

        db.session.execute(
            """
            update node_feedback f1
            set node_id=:new
            where node_id=:old and not exists (
                select 1
                from node_feedback f2
                where f2.node_id=:new and f2.student_id=f1.student_id
            )
        """,
            {"new": new, "old": old},
        )

        db.session.execute(
            """
            update user_flashcard uf1
            set node_id=:new
            where node_id=:old and not exists (
                select 1
                from user_flashcard uf2
                where uf2.node_id=:new and uf2.flashcard_id=uf1.flashcard_id and uf2.user_id=uf1.user_id
            )
        """,
            {"new": new, "old": old},
        )

        keyvalue.set("renames_processed", index + 1)
        db.session.commit()

        for attempt in Attempt.query.filter_by(node_id=new, data_deleted=False):
            old_dir = os.path.join(
                app.config["DATA_DIR"],
                "attempts",
                f"{old}-{attempt.student_id}-{attempt.number}",
            )
            if os.path.exists(old_dir) and not os.path.exists(attempt.directory):
                os.rename(old_dir, attempt.directory)


elif len(sys.argv) == 3 and sys.argv[1][0] != "-":
    old = sys.argv[1]
    new = sys.argv[2]
    # Move the directory
    old_path = f"{LMS_DIR}/assignments/{old}"
    new_path = f"{LMS_DIR}/assignments/{new}"
    if not os.path.isdir(old_path):
        raise FileNotFoundError(f"{old_path} does not exist")
    if os.path.isdir(new_path):
        raise FileExistsError(f"{new_path} already exists")
    os.rename(old_path, new_path)

    # Add an entry to the renames file
    with open(LMS_DIR + "/assignments/renames.yaml", "a") as file:
        file.write(f"- {old}: {new}\n")

    # Update the assignment reference in modules/
    subprocess.run(
        f"find '{LMS_DIR}/modules' -type f -exec sed -i 's/^\\(\\s*\\)- {old}/\\1- {new}/g' {{}} +",
        shell=True, check=False,
    )

else:
    print(f"Usage: {sys.argv[0]} OLD_NAME NEW_NAME | --update-db")
