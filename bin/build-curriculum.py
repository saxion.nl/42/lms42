import copy
import glob
import os
import pickle
import sys
import yaml

VERSION = [1, 1]

# Parse command line args
ignore_fatal = False  # fatal errors will not cause the build to abort
ignore_restricted = (
    False  # errors due to having no access to restricted files are not fatal
)
for arg in sys.argv[1:]:
    if arg == "--dev":
        ignore_fatal = True
    elif arg == "--ignore-restricted":
        ignore_restricted = True
    else:
        print("Invalid argument: " + arg)
        sys.exit(1)


# The dictionary that is to contain our overall result that will be written to the data files
result = {}


# Error handling function
errors = {
    "fatal": [],
    "error": [],
    "warning": [],
}


def error(msg, node=None, severity=None):
    if not severity:
        severity = "fatal"
        if node and node.get("wip"):
            severity = "error"

    errors[severity].append(f"{node['id']}: {msg}" if node else msg)

    (node or result).setdefault(
        "warnings" if severity == "warning" else "errors", []
    ).append(msg)


# See if we have encrypted access to the restricted files
with open(".restricted-check", "rb") as file:
    RESTRICTED_AVAILABLE = file.read() == b"OK"
if not RESTRICTED_AVAILABLE and not ignore_restricted:
    error("Restricted files are not available", None, "fatal")


# YAML parsing helpers
def load_yaml(filename):
    with open(filename) as file:
        return yaml.load(file, Loader=yaml.FullLoader)


def ref_constructor(loader, node):
    value = loader.construct_scalar(node)
    return references[value]


yaml.add_constructor("!ref", ref_constructor)


def dict_constructor(loader, node):
    d = loader.construct_mapping(node)
    if "^merge" in d:
        merge = d.pop("^merge")
        if isinstance(merge, str):
            merge = references[merge]
        merge = merge.copy()
        merge.update(d)
        d = merge
    return d


yaml.add_constructor("tag:yaml.org,2002:map", dict_constructor)


# Load referenced fragments from YAML using our custom '!ref' tag
references = load_yaml("curriculum/references.yaml")


def assure_sections(document, top_name):
    """Put all top-level items in a section if they aren't already. The created sections will have
    no title. Except when the section is at index 0, then it will be titled `top_name`.
    """
    result = []
    inserting_section: dict | None = None
    for index, item in enumerate(document):
        if isinstance(item, dict) and item.get("type") == "section":
            inserting_section = None
            result.append(item)
        else:
            if not inserting_section:
                inserting_section = {"type": "section", "children": []}
                if index == 0:
                    inserting_section["title"] = top_name
                result.append(inserting_section)
            inserting_section["children"].append(item)
    return result


def normalize_assignment(data, node, assignment):
    all_rubrics = []

    context = f"assignment{assignment}"

    # Recursively transform the assignment tree, also adding all rubric
    # objects to the rubrics list.
    document = assure_sections(
        normalize_assignment_recurse(data, context, all_rubrics, node), "Assignment"
    )

    # Prepend learning materials
    resources = node.get("resources")
    if resources:
        document = [
            {
                "type": "section",
                "title": "Learning materials",
                "children": normalize_assignment_recurse(
                    resources, context, all_rubrics, node
                ),
            }
        ] + document

    # Prepend the intro
    intro = node.get("intro")
    if intro:
        document = (
            assure_sections(
                normalize_assignment_recurse(intro, context, all_rubrics, node),
                "Learning materials",
            )
            + document
        )

    # Make sure rubrics have at least a title or a text
    for num, rubric in enumerate(all_rubrics, 1):
        if not rubric.get("title") and not rubric.get("text"):
            error(f"rubric {num} has no title nor a text", node, "warning")

    # Filter out 'must' rubrics
    rubrics = [rubric for rubric in all_rubrics if not rubric.get("must")]

    # Make sure 'map' is set for each rubric
    for rubric in rubrics:
        location = f"{context}: {rubric.get('title') or rubric.get('text', '')[0:30]}"
        if "map" in rubric:
            if "weight" in rubric:
                error(
                    f"{location}: Rubric cannot have both a weight and a map",
                    node,
                    "fatal" if "ects" in node else "warning",
                )
            if isinstance(rubric["map"], str):
                rubric["map"] = {rubric["map"]: 1}
        else:
            if "ects" in node:
                error(
                    f"{location}: Test assignment rubric without a mapping to goals",
                    node,
                )
            weight = rubric.pop("weight", 1)
            rubric["map"] = {
                goal_id: weight for goal_id, _goal in node["goals"].items()
            }

    # Create the code quality rubric, if applicable
    code_rubric = copy.deepcopy(references["codequality"])
    code_rubric["map"] = {}
    for rubric in rubrics:
        if rubric.get("code"):
            if rubric["code"] is True:
                rubric["code"] = 0.35
            for goal, goal_weight in rubric["map"].items():
                code_rubric["map"].setdefault(goal, 0)
                code_rubric["map"][goal] += goal_weight * rubric["code"]

    if code_rubric["map"]:
        document.append(
            {
                "type": "section",
                "title": "Code quality",
                "children": [{"type": "rubric", "rubric": len(all_rubrics)}],
            }
        )
        rubrics.append(code_rubric)
        all_rubrics.append(code_rubric)

    # Calculate total weights per learning goal
    goal_totals = {}
    for rubric in rubrics:
        bonus = rubric.get("bonus", 0)
        malus = rubric.get("malus", 0)
        if bonus + malus > 1:
            error(f"{context}: Bonus ({bonus}) + malus ({malus}) > 1 for rubric", node)
            continue
        for goal, weight in rubric["map"].items():
            goal_totals.setdefault(goal, 0)
            goal_totals[goal] += weight * (1 - bonus - malus)

    # Calculate map_fractions and fractions
    floor_total = 1
    goal_ranges = {}
    for rubric in rubrics:
        rubric["range"] = [0, 0]
        rubric["goal_ranges"] = {}
        malus = rubric.get("malus", 0)
        for goal, weight in rubric["map"].items():
            if goal not in node["goals"]:
                error(
                    f"{context}: Assignment map includes a goal '{goal}' that is not part of the node at",
                    node,
                )
                continue

            if goal_totals[goal] <= 0:
                error(f"{context}: Goal '{goal}' has no positive weight", node)
                continue

            fraction = weight / goal_totals[goal] * node["goals"][goal]["fraction"]

            min_grade = -malus * fraction * 9
            max_grade = min_grade + fraction * 9

            floor_total += min_grade
            rubric["range"][0] += min_grade
            rubric["range"][1] += max_grade
            rubric["goal_ranges"][goal] = [min_grade, max_grade]

            goal_ranges.setdefault(goal, [0, 0])
            goal_ranges[goal][0] += min_grade
            goal_ranges[goal][1] += max_grade

    # Check that all goals have been tested
    if rubrics:
        if node.get("grading") is False:
            error(f"{context}: Node has rubrics but grading==false", node)
        for goal_id, goal_obj in node["goals"].items():
            if goal_obj.get("weight") > 0 and (
                goal_id not in goal_totals or not goal_totals[goal_id]
            ):
                error(f"{context}: Goal '{goal_id}' has not been tested", node)
    elif node.get("grading") is not False:
        error(f"{context}: No rubrics", node)

    node.setdefault("autopass", not all_rubrics and not node.get("upload", True))
    if node.get("autopass") and all_rubrics:
        error(f"{context}: autopass is not possible when there are rubrics", node)
        node["autopass"] = False

    node.setdefault("grade_without_student", "ects" in node)

    return {
        "rubrics": all_rubrics,
        "document": document,
        "floor": floor_total,
        "goal_ranges": goal_ranges,
    }


def is_rubric(v):
    return isinstance(v, dict) and (
        v.get("type") == "rubric"
        or "must" in v
        or 0 in v
        or 1 in v
        or 2 in v
        or 3 in v
        or 4 in v
        or "grading_instructions" in v
        or "scale" in v
    )


def normalize_assignment_recurse(data, context, rubrics, node):
    if not isinstance(data, list):
        data = [data]

    result = []
    for child_pos, child in enumerate(data):
        if isinstance(child, dict):
            if "question" in child:
                answers = child.get("answer", child.get("answers", ""))
                if not isinstance(answers, list):
                    answers = [answers]
                answers = [answer.strip() for answer in answers]
                if not answers or not answers[0]:
                    error(
                        f"{context}>{child_pos}: Flashcard has no answer {child}", node
                    )
                else:
                    id = child.get("id") or answers[0]
                    flashcard = child.copy()
                    flashcard["answers"] = answers
                    flashcard.pop("answer", None)
                    flashcard.pop("id", None)

                    if id in node["flashcards"] and flashcard != node["flashcards"][id]:
                        error(
                            f"{context}>{child_pos}: Duplicate flashcard id '{id}' in assignment with different q&a",
                            node,
                        )
                    else:
                        node["flashcards"][id] = flashcard
                        result.append({"flashcard": id})

            elif is_rubric(child):
                # a rubric that has the title embedded
                result.append({"rubric": len(rubrics)})
                rubrics.append(child)
            elif "link" in child:
                if child.get("text"):
                    error("use 'info' instead of 'text' to describe links", node)
                result.append(child)
            else:
                for k, v in child.items():
                    if k[0] == "_":
                        result += normalize_assignment_recurse(
                            v, f"{context}>{child_pos}>{k}", rubrics, node
                        )
                    else:
                        result.append(
                            {
                                "type": "section",
                                "title": k,
                                "children": normalize_assignment_recurse(
                                    v, f"{context}>{child_pos}>{k}", rubrics, node
                                ),
                            }
                        )
        elif isinstance(child, str):
            result.append(child)
        else:
            error(f"{context}>{child_pos}: Unexpected assignment item '{child}'", node)

    return result


def get_recursive_providers(node_id: str, visited=None) -> list[str]:
    """
    Recursively gathers all providers for a given node.
    """
    if visited is None:
        visited = set()

    providers = set()

    # Check if the node_id has already been visited to prevent infinite recursion
    if node_id in visited:
        return list(providers)

    visited.add(node_id)

    node = nodes_by_id.get(node_id)
    if node:
        providers.add(node_id)
        if "provided_by" in node:
            for provider_id in node["provided_by"]:
                providers.update(get_recursive_providers(provider_id, visited))

    return list(providers)


endterms = load_yaml("curriculum/endterms.yaml")

periods = {}
nodes_by_id = {}
goals_with_outcomes = set()
tests = []
bloom_tax = ["know", "comprehend", "apply", "analyze", "evaluate", "create"]


for node_dir in sorted(glob.glob("assignments/*")):
    try:
        node_id = os.path.basename(node_dir)
        if node_id.startswith("_") or not os.path.isdir(node_dir):
            continue

        # Load the node.yaml and normalize it
        node = load_yaml(f"{node_dir}/node.yaml")
        if not isinstance(node, dict):
            error(f"Not a YAML dict: {node_dir}/node.yaml")
            continue

        if "ignore" in node:
            continue

        node["id"] = node_id
        node["format_version"] = VERSION

        if node_id.endswith("-exam") and not node.get("ects"):
            error(f"{node_id} has exam in the name but has no ECTS", node, "fatal")

        if (
            "ects" in node
            and not node_id.endswith(
                (
                    "-project",
                    "-exam",
                )
            )
            and not node.get("public_for_students")
        ):
            error(
                f"{node_id} has ECTS but has no -exam suffix, nor is it a project or publicly visible assignment, so it should probably be encrypted",
                node,
                "fatal",
            )

        if "ects" in node:
            tests.append(node)

        node.setdefault("type", "hand-in")

        node["days"] = int(node.get("days", 1))
        node["avg_attempts"] = float(
            node.get("avg_attempts", 1.0 if "ects" in node else 1.5)
        )
        node["directory"] = node_dir

        deps = node.get("depend", [])
        if isinstance(deps, str):
            deps = [dep.strip() for dep in deps.split(",")]
        node["depend"] = deps

        todo = node.get("TODO") or node.get("todo")
        if todo:
            if isinstance(todo, list):
                todo = "\n".join([f"- {item}" for item in todo])
            error(f"TODO:\n{todo.strip()}", node, "warning")

        restricted = "ects" in node and not node.get("public_for_students")

        # Normalize the goals
        node.setdefault("goals", {})
        if not node["goals"] and node.get("grading") is not False:
            error("No goals", node, "warning")

        total_weight = 0
        for goal_id, goal_obj in node["goals"].items():
            if not isinstance(goal_obj, dict):
                # For formative assignments no bloom levels are set and
                # the default we are assuming is 'apply'.
                node["goals"][goal_id] = goal_obj = {"apply": goal_obj}
            weight = 0
            unspecified = True
            for bloom, bloom_weight in goal_obj.items():
                if bloom in bloom_tax:
                    weight += bloom_weight
                    unspecified = False
                elif bloom not in {"title", "outcome_ids"}:
                    error(f"{goal_id}: Unknown key '{bloom}'", node, "warning")
            if unspecified:
                weight = 1
                goal_obj["apply"] = 1
            goal_obj["weight"] = weight
            total_weight += weight

        for goal_obj in node["goals"].values():
            goal_obj["fraction"] = goal_obj["weight"] / total_weight

        # Add all assignments to the node, after normalizing them
        if "assignment" in node:
            node["assignment1"] = node.pop("assignment")

        if restricted and "assignment1" in node:
            error("Exam assignment cannot be part of node.yaml", node, "fatal")

        cnt = 1
        while os.path.exists(f"{node_dir}/assignment{cnt}.yaml"):
            if not restricted or RESTRICTED_AVAILABLE:
                node[f"assignment{cnt}"] = load_yaml(f"{node_dir}/assignment{cnt}.yaml")
            cnt += 1

        # Make sure provided_by is always a list
        if "provided_by" not in node:
            node["provided_by"] = []
        elif isinstance(node["provided_by"], str):
            node["provided_by"] = [node["provided_by"]]

        node["flashcards"] = {}

        cnt = 1
        while f"assignment{cnt}" in node:
            node[f"assignment{cnt}"] = normalize_assignment(
                node[f"assignment{cnt}"], node, cnt
            )
            cnt += 1

        if cnt == 1:
            if not restricted or RESTRICTED_AVAILABLE or not ignore_restricted:
                error("No assignment", node)
            if node.get("intro") or node.get("resources"):
                node["assignment1"] = normalize_assignment([], node, 1)

        if nodes_by_id.get(node_id) is not None:
            error(
                f"Duplicate node id: {nodes_by_id[node_id]['directory']} and {node['directory']}"
            )

        nodes_by_id[node_id] = node

    except Exception:
        print(f"Error while processing assignment {node_dir}")
        raise


# Load modules
claimed_node_ids = set()
modules_by_id = {}

for module_file in sorted(glob.glob("modules/*.yaml")):
    try:
        module_id = os.path.basename(module_file)[:-5]
        if module_id.startswith("_"):
            continue

        module = load_yaml(module_file)
        if not isinstance(module, dict):
            error(f"Not a YAML dict: {module_file}")
            continue

        module["id"] = module_id
        module["avg_days"] = 0
        module["ects"] = 0

        module["nodes"] = []
        for node_id in module["assignments"]:
            node = nodes_by_id.get(node_id)
            claimed_node_ids.add(node_id)
            if not node:
                error(f"Unknown assignment '{node_id}' in module '{module_id}'")
                continue

            # Add the node to all the appropriate data structures
            module["nodes"].append(node)
            module["avg_days"] += node["days"] * node["avg_attempts"]

            if "ects" in node:
                module["ects"] += node["ects"]

        modules_by_id[module_id] = module

    except Exception:
        print(f"Error while processing module {module_file}")
        raise

# Check if all nodes are claimed by modules
for node_id in nodes_by_id:
    if node_id not in claimed_node_ids:
        error(f"Assignment '{node_id}' is not in any module (nor ignored)")


# Load curriculum periods
claimed_module_ids = set()
period_data = load_yaml("curriculum/periods.yaml")
periods = {}
if isinstance(period_data, dict):
    for period, module_names in period_data.items():
        periods[period] = []
        for module_name in module_names:
            module_options = module_name.split(" ")
            module_id = module_options.pop(0)
            if module_id not in modules_by_id:
                error(
                    f"periods.yaml expects module '{module_id}' but it does not exist"
                )
                continue
            module = modules_by_id[module_id]
            claimed_module_ids.add(module_id)
            periods[period].append(module)

            # Create legacy dictionary
            for legacy_key, legacy_module_id in (
                module_option.split("=") for module_option in module_options
            ):
                if legacy_module_id not in modules_by_id:
                    error(
                        f"periods.yaml expects legacy module '{legacy_module_id}' but it does not exist"
                    )
                module.setdefault("legacy", {})
                module["legacy"][legacy_key] = legacy_module_id
else:
    error("curriculum/periods.yaml is not a YAML dict")


# Check if all modules are claimed by periods
for module_id in modules_by_id:
    if module_id not in claimed_module_ids:
        error(f"Module '{module_id}' is not in any period")


# Calculate phase stats
phase_stats = {1: {}, 2: {}}
for period, modules in periods.items():
    if not period[0].isdigit():
        continue
    phase_num = int(period[0])
    if phase_num == 0:
        phase_num = 1
    if phase_num not in phase_stats:
        continue
    for module in modules:
        for node in module["nodes"]:
            type_name = node["type"]
            if "ects" in node:
                type_name += "-exam"
            if type_name not in phase_stats[phase_num]:
                phase_stats[phase_num][type_name] = {
                    "count": 0,
                    "avg_days": 0,
                    "ects": 0,
                }
            if "ects" in node:
                phase_stats[phase_num][type_name]["ects"] += node["ects"]
            phase_stats[phase_num][type_name]["count"] += 1
            phase_stats[phase_num][type_name]["avg_days"] += (
                node["days"] * node["avg_attempts"]
            )


# Calculate phase stats
for phase_num, stats in phase_stats.items():
    sums = {}
    for what in ["count", "avg_days", "ects"]:
        sums[what] = sum(s[what] for s in stats.values())
    stats["*"] = sums
    if sums["avg_days"] > 200 or sums["avg_days"] < 185:
        error(f"Total days for phase {phase_num} is {sums['avg_days']}")
    if sums["ects"] != 60:
        error(f"ECTS for phase {phase_num} is {sums['ects']}", None, "fatal")


nodes_by_goal = {}
current_goals = set()
current_exam_goals = set()
for period_name, modules in periods.items():
    for module in modules:
        for node in module["nodes"]:
            for goal_id, goal in node["goals"].items():
                nodes_by_goal.setdefault(goal_id, [])
                nodes_by_goal[goal_id].append(node)
                if period_name != "x":
                    current_goals.add(goal_id)
                    if node.get("ects"):
                        current_exam_goals.add(goal_id)


# Resolve learning goals in outcomes
outcomes_by_id = {}
for endterm_number, endterm in enumerate(endterms, 1):
    outcomes = []
    for outcome in endterm["outcomes"]:
        outcome["endterm"] = endterm
        goals = []
        for goal_id in outcome["goals"]:
            goal = {"node_ids": []}
            if goal_id not in current_goals:
                error(
                    f"Learning goal '{goal_id}' for outcome '{outcome['short']}' does not exist in current curriculum"
                )
                if goal_id not in nodes_by_goal[goal_id]:
                    continue
            elif goal_id not in current_exam_goals:
                error(
                    f"Learning goal '{goal_id}' is not part of any exam",
                    nodes_by_goal[goal_id][0],
                )
            for node in nodes_by_goal[goal_id]:
                node["goals"][goal_id].setdefault("outcome_ids", [])
                node["goals"][goal_id]["outcome_ids"].append(outcome["short"])
                goal["id"] = goal_id
                goal["description"] = node["goals"][goal_id]
                goal["node_ids"].append(node["id"])
            goals.append(goal)
            goals_with_outcomes.add(goal_id)
        outcome["goals"] = goals
        outcome["endterm_number"] = endterm_number
        outcomes.append(outcome)
        outcomes_by_id[outcome["short"]] = outcome

    endterm["outcomes"] = outcomes
    endterm["number"] = endterm_number

    endterm_goal = "endterm_" + endterm["short"]
    goals_with_outcomes.add(endterm_goal)
    for node in nodes_by_goal[endterm_goal]:
        node["goals"][endterm_goal]["outcome_ids"] = [
            outcome["short"] for outcome in outcomes
        ]


# Check if all goals in the current (non-'x') curriculum are part of learning outcomes.
for goal_id in current_goals:
    if goal_id not in goals_with_outcomes:
        for node in nodes_by_goal[goal_id]:
            error(f"Test goal '{goal_id}' is not attached to a learning outcome", node)


# Check if all tests actually test something
for test in tests:
    if len(test["goals"]) == 0 and test.get("goal_checks", True):
        error("Exam has no learning goals", test)


# Check if all goals have description and are tested formatively
for goal_id, nodes in nodes_by_goal.items():
    title = None
    if goal_id.startswith("endterm_"):
        endterm_short = goal_id.split("_", 1)[1]
        title = [endterm for endterm in endterms if endterm["short"] == endterm_short][
            0
        ]["title"]
    for node in nodes:
        goal_obj = node["goals"][goal_id]
        if "title" in goal_obj:
            if title and title != goal_obj["title"]:
                error(
                    f"Title for goal '{goal_id}' has two conflicting definitions",
                    node,
                    "error",
                )
            title = goal_obj["title"]

    if not title:
        title = f"No title for '{goal_id}'"
        error_node = nodes[0]
        for node in nodes:
            if "ects" in node:
                error_node = node
        error(f"Goal '{goal_id}' has no title", error_node)

    formative = summative = False
    for node in nodes:
        goal_obj = node["goals"][goal_id]
        goal_obj["title"] = title
        if not node.get("goal_checks", True):
            continue
        if node.get("ects"):
            summative = node
        else:
            formative = True
    if summative and not formative and not goal_id.startswith("endterm_"):
        error(f"Goal '{goal_id}' is only tested summatively", summative, "warning")

# Set any missing descriptions for nodes
for node in nodes_by_id.values():
    if "description" not in node:
        node["description"] = " ".join(
            [goal["title"] for goal in node["goals"].values()]
        )

# Check if all depends exist
for node in nodes_by_id.values():
    for dep in node["depend"]:
        if dep not in nodes_by_id:
            error(f"Dependency '{dep}' does not exist", node)

# Give all nodes it's module's exam as a provided_by property
for module in modules_by_id.values():
    exam_or_project_nodes = []

    # First pass to identify all nodes that are exams or projects
    for node_id in module["assignments"]:
        node = nodes_by_id.get(node_id)
        if node and "ects" in node:
            exam_or_project_nodes.append(node_id)

    # Update 'provided_by' for all nodes in the module
    for node_id in module["assignments"]:
        node = nodes_by_id[node_id]
        node["provided_by"].extend(exam_or_project_nodes)

# Second pass: Perform recursive provider gathering for all nodes
for module in modules_by_id.values():
    for node_id in module["assignments"]:
        node = nodes_by_id.get(node_id)
        if node and "provided_by" in node:
            providers_set = set(node["provided_by"])  # Use a set to avoid duplicates

            # Gather recursive providers for each direct provider in 'provided_by'
            for provider_id in node["provided_by"]:
                recursive_providers = get_recursive_providers(provider_id)
                providers_set.update(recursive_providers)

            node["provided_by"] = list(providers_set)


# Sort periods
periods = dict(sorted(periods.items()))

# Set module goals based on node goals
for module in modules_by_id.values():
    module["goals"] = {
        goal_id: goal["title"]
        for xnode in module["nodes"]
        if "ects" in xnode
        for (goal_id, goal) in xnode["goals"].items()
    }

# Retrieve legacy keys
legacy_keys = set()
for module in modules_by_id.values():
    for key in module.get("legacy", {}):
        legacy_keys.add(key)
legacy_keys = sorted(legacy_keys)

hbo_i = load_yaml("curriculum/hbo-i.yaml")

# Check renames
renamed_node_ids = set(nodes_by_id.keys())
with open("assignments/renames.yaml") as file:
    renames = yaml.load(file, Loader=yaml.FullLoader) or []
for item in reversed(renames):
    assert len(item) == 1
    old, new = next(iter(item.items()))
    if new not in renamed_node_ids:
        raise AssertionError(f"Assignment '{new}' does not exist")
    if old in renamed_node_ids:
        raise AssertionError(f"Assignment '{old}' already exists exist")
    renamed_node_ids.remove(new)
    renamed_node_ids.add(old)

result.update(
    {
        "format_version": VERSION,
        "periods": periods,
        "nodes_by_id": nodes_by_id,
        "modules_by_id": modules_by_id,
        "stats": phase_stats,
        "tests": tests,
        "bloom_tax": bloom_tax,
        "endterms": endterms,
        "hbo_i": hbo_i,
        "outcomes_by_id": outcomes_by_id,
        "legacy_keys": legacy_keys,
    }
)


if __name__ == "__main__":
    print("\n---------- build-curriculum.py ----------")
    for severity in ["fatal", "error"]:
        for msg in sorted(errors[severity]):
            print(severity.upper() + ": " + msg, file=sys.stderr)

    if len(errors["fatal"]):
        print(
            f"\n********** {len(errors['fatal'])} FATAL ERROR(S) **********\n",
            file=sys.stderr,
        )
        if not ignore_fatal:
            sys.exit(1)
    print()

    with open("curriculum.pickle", "wb") as file:
        file.write(pickle.dumps(result))
