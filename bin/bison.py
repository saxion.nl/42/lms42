#!/bin/env python3

import sys, os

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from lms42.models import curriculum

for year in [1, 2]:
    print(f"\n\n## Modules year {year}")
    for period, modules in curriculum.get("periods").items():
        if period[0] == str(year):
            for module in modules:
                if module["ects"]:
                    print(f"\n### {module['name']}")
                    print(f"  - ECTS: {module['ects']}")
                    print(f"  - Owner: {module['owner']}")
                    if "description" in module:
                        print(f"  - Module description: {module['description']}")
                    for node in module["nodes"]:
                        if "ects" in node:
                            print(f"  - Exam description: {node['description']}")
                    if node["id"] != "graduation":
                        print("  - Learning goals:")
                        for goal in module["goals"].values():
                            print(f"    - {goal}")
                    print("  - Competences:")
                    for endterm in curriculum.get("endterms"):
                        hits = 1 if node["id"] == "graduation" else 0
                        for outcome in endterm["outcomes"]:
                            if "goals" in outcome:
                                for goal in outcome["goals"]:
                                    if goal["id"] in module["goals"]:
                                        hits += 1
                        if hits > 0:
                            print(f"    - {endterm['title']}")
