#!/bin/env python
import sys
import csv
import os

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from lms42.routes.discord import bot_command


def create_channels(guild_id, csv_file_path, category):
    user_name_ids = {}  # {name => user_id}

    def add_name(name, user_id):
        if name:
            name = name.lower()
            if name in user_name_ids and user_name_ids[name] != user_id:
                user_name_ids[name] = False
            else:
                user_name_ids[name] = user_id

    with open(csv_file_path, newline="") as csvfile:
        if category:
            category_resp = bot_command(
                f"guilds/{guild_id}/channels", "POST", {"name": category, "type": 4}
            )
            category_id = category_resp.json()["id"]
        else:
            category_id = None

        members_resp = bot_command(f"guilds/{guild_id}/members?limit=500")
        for member in members_resp.json():
            user_id = member["user"]["id"]
            add_name(member["nick"], user_id)
            add_name(member["user"]["global_name"], user_id)
            add_name(member["user"]["username"], user_id)

        reader = csv.DictReader(csvfile)
        channel_permissions = {}
        for row in reader:
            user = row["user"].strip().lower()
            user_id = user_name_ids.get(user) or user_name_ids.get(user.split(" ")[0])
            if not user_id:
                print(
                    f'User "{user}" not found but should be in channel "{row["channel"]}"'
                )
                continue

            if row["channel"] not in channel_permissions:
                channel_permissions[row["channel"]] = [
                    {
                        "id": guild_id,  # @everyone role
                        "type": 0,  # 0 for a role, 1 for a user
                        "deny": 1024,  # Deny permission to READ_MESSAGES
                    }
                ]

            channel_permissions[row["channel"]].append(
                {
                    "id": user_id,
                    "type": 1,  # 1 is for a member
                    "allow": 68608,  # Permissions bit for viewing and sending messages
                }
            )

        for channel_name, permissions in channel_permissions.items():
            channel_resp = bot_command(
                f"guilds/{guild_id}/channels",
                "POST",
                {
                    "name": channel_name,
                    "type": 0,
                    "parent_id": category_id,
                    "permission_overwrites": permissions,
                },
            )
            print(channel_resp.json())


# Example usage
if len(sys.argv) in {3, 4}:
    create_channels(
        sys.argv[1], sys.argv[2], sys.argv[3] if len(sys.argv) > 3 else None
    )
else:
    print(f"""Usage:
  {sys.argv[0]} GUILD_ID CSV_FILENAME [GROUP_NAME]

Authorized guilds:""")
    for guild in bot_command("users/@me/guilds").json():
        print("  " + guild["id"] + " " + guild["name"])
    print(f"""
Authorization URL:
  https://discord.com/api/oauth2/authorize?client_id={os.environ.get("DISCORD_CLIENT_ID")}&permissions=1099780063248&scope=bot
""")
