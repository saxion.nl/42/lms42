#!/bin/env python3

import openai
import sys, os

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from lms42.models import curriculum


def ai(system, prompt=None):
    msgs = [{"role": "user", "content": system}]
    if prompt:
        msgs.append({"role": "user", "content": prompt})
    msgs.append(
        {
            "role": "user",
            "content": "Geef buiten het gevraagde GEEN enkele verdere output.",
        }
    )

    client = openai.OpenAI(base_url=os.environ.get("OPENAI_PROXY"))
    completion = client.chat.completions.create(model="o1-mini", messages=msgs)
    choice = completion.choices[0]
    return choice.message.content.replace("```html", "").replace("```", "")


def translate(text):
    return ai("Please translate the following academic text to Dutch:", text)


for period, modules in curriculum.get("periods").items():
    if period == "x":
        continue
    for module in modules:
        if not module["ects"]:
            continue

        if module["id"] == "new-graduation":
            goals = [endterm["title"] for endterm in curriculum.get("endterms")]
        else:
            goals = list(module["goals"].values())

        result = ai(
            "Schrijf de onderstaande leerdoelen om naar één enkele high-level, SMART leeruitkomst, in het Nederlands. Dit moet in de je-vorm zijn en vertellen wat de student *doet* en NIET wat hij *kan*. Een voorbeeld uit een heel ander vakgebied: 'Je herkent de factoren die belangrijk zijn bij gezondheidsinstanthouding gedurende de levensloop en kan deze verantwoord toepassen bij het analyseren van het risico op gezondheidsproblemen, gebruikmakende van actuele literatuur.'",
            "\n".join([f"- {g}" for g in goals]),
        )

        indicatoren = ai(
            "Schrijf de onderstaande module-leerdoelen om naar ongeveer 5 module-indicatoren (beschrijving van handelingen die een student verricht, in de je-vorm), die samen alle leerdoelen goed dekken. Probeer de leerdoelen handig samen te trekken zodat er minimaal 4 tot MAXIMAAL 6 indicatoren overblijven. Geef het resultaat als Nederlandstalig HTML lijstje, beginnende met `<ul>`.",
            "\n".join([f"- {g}" for g in goals]),
        )

        print("<table>")
        print(f"<tr><td>Modulenaam</td><td>{module['name']}</td></tr>")
        print(f"<tr><td>Leeruitkomst</td><td>{result}</td></tr>")
        print(f"<tr><td>Aantal EC</td><td>{module['ects']}</td></tr>")
        if "description" in module:
            print(f"<tr><td>Introductie (EN)</td><td>{module['description']}</tr>")
            print(
                f"<dr><td>Introductie</td><td>{translate(module['description'])}</tr>"
            )
        print(
            f"<tr><td>Input tbv de module-indicatoren</td><td><ul><li>{'</li>\n<li>'.join(goals)}</ul></td></tr>"
        )
        print(f"<tr><td>Aanzetje indicatoren</td><td>{indicatoren}</td></tr>")
        print(
            "<tr><td>Afronding van de module / toetsing / beroepsproduct(en)  </td><td></td></tr>"
        )
        print(
            f"<tr><td>Wensen en aandachtspunten </td><td>Probeer zo veel mogelijk uit te lijnen met en gebruik te maken van de voltijd-module: https://sd42.nl/curriculum#{period}</td></tr>"
        )

        print("</table>\n\n")
