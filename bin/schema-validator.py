#!/usr/bin/env python3
import sys, yaml, os
from schema import Schema, Optional, SchemaError

MODULE_SCHEMA = Schema(
    {
        "name": str,
        "short": str,
        "bison_exam": str,
        "description": str,
        "owner": str,
        Optional("co-owners"): list,
        Optional("wip"): bool,
        Optional("hidden"): bool,
        Optional("train"): str,
        Optional("count_as_in_progress"): bool,
        "assignments": list,
    }
)

NODE_SCHEMA = Schema(
    {
        "name": str,
        "description": str,
        "goals": dict,
        "days": int,
        Optional("pair"): bool,
        Optional("ignore"): bool,
        Optional("depends"): str,
        Optional("ai"): str,
        Optional("public_for_externals"): bool,
        Optional("todo"): list,
        Optional("resources"): list,
        Optional("assignment"): dict,
    }
)


def validate(path_yaml, schema):
    with open(path_yaml) as file:
        data = yaml.safe_load(file)
        try:
            schema.validate(data)
        except SchemaError as e:
            print(f"     >> {e}")
            return False, data
    return True, data


# Usage:
#  no args: all modules are validated
#  --module <module_name.yaml>: validates a specific module
#  --node <node_directory_name>: validates a specific node (node.yaml from the node directory in the assignment directory)
if __name__ == "__main__":
    MODULES_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), "../modules")
    NODES_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), "../assignments")
    if len(sys.argv) == 3:
        if sys.argv[1] == '--module':
            print(f"Module validation: {sys.argv[2]}")
            is_valid, data = validate(os.path.join(MODULES_PATH, sys.argv[2]), MODULE_SCHEMA)
            print(f"Valid: {is_valid}")
        elif sys.argv[1] == '--node':
            print(f"Node validation: {sys.argv[2]}")
            is_valid, data = validate(os.path.join(NODES_PATH, sys.argv[2], "node.yaml"), NODE_SCHEMA)
            print(f"Valid: {is_valid}")
        else:
            print("Usage: invalid args")
    else:
        num_invalid_modules = 0
        print("Module validation: ALL")
        module_yamls = sorted([each for each in os.listdir(MODULES_PATH) if each.endswith('.yaml')])
        teachers = {}
        errors = []
        for index, module in enumerate(module_yamls):
            print(f"{index+1}: Validating {module}")
            is_valid, module_data = validate(os.path.join(MODULES_PATH, module), MODULE_SCHEMA)
            if not is_valid:
                num_invalid_modules += 1
                print(f"    ERROR: Module {module} not valid")
            owner = module_data.get('owner')
            if owner:
                teachers.setdefault(owner, {"owns": [], "co-owns": []})
                teachers[owner]["owns"].append(module_data.get("name"))
                co_owners = module_data.get("co-owners")
                if co_owners:
                    for co in co_owners:
                        teachers.setdefault(co, {"owns": [], "co-owns": []})
                        teachers[co]["co-owns"].append(module_data.get("name"))
        
        print("----------------------")
        print(f"# Modules: {len(module_yamls)}")
        print(f"# Invalid modules: {num_invalid_modules}")
        print("----------------------")
        # print("Teachers: (name) - (#owner) - (#co-owner)")
        # for index, (key, value) in enumerate(teachers.items()):
        #     print(f"{index+1}: {key} - {len(value['owns'])} - {len(value['co-owns'])}")
