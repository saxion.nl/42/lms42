
## Create a database

### 1: Import into temporary table

```sql
TODO
```

### 2: ERD

```plantuml
!include https://sd42.nl/static/style-light.plantuml

entity incident {
    ---
    * time
}

entity location {
    ---
    * street
    * city
    * postal_code
}

location |o--o{ incident

entity type {
    ---
    * code
    * description
}

incident }o--|| type

entity agent {
    ---
    * badge_number
    * name
}

incident }o--|{ agent

entity suspect {
    ---
    * name
    * gender
    * date_of_birth
}

suspect }o--o{ incident
```

### 3: Create schema

```sql
TODO
```

### 4: Populate schema

```sql
TODO
```


## Insert more data

### 5.1

Insert 3 new agents into the database. You are free to choose their names and badge numbers.

```sql
TODO
```

### 5.2

Insert suspect 'Dr. Jekyll' into the database and add him to all incidents associated with 'Mr. Hyde'. (Two queries.)

```sql
TODO
```


### 5.3

Insert the following incident into the database: Just now, an incident of type 'Riot' took place at not registered before location: '425 Phillips Pine' in 'Susanmouth' with postal code 'MI 20522'. The incident was reported by agent 'Agent Smith'. (Multiple queries)

*Hint:* `NOW()` returns the current date and time.

```sql
TODO
```


## Query the data

### 6.1

Display the list of unique type codes and type descriptions for all incidents that happened in the city of 'Barbarahaven'.

```sql
TODO
```


### 6.2

For planning fire fighting capacity, the fire department would like to know at what hour the most smoke report incidents are being created. Display a table that shows the hour (sorted from 0 up to 23) and the number of incidents with type code '10-73' that have been reported within that hour (for any date). 

*Hint:* `EXTRACT(HOUR FROM time)`

```sql
TODO
```

Expected row count: 24
Expected column names: hour smoke_reports


### 6.3

For the city with the most incidents, display the name of the city and the number of incidents that occurred there.

```sql
TODO
```

Expected column names: city number_of_incidents


### 6.4

Show the descriptions of all incident types that agent 'Agent Smith' has handled at least 4 times, ordered alphabetically.

```sql
TODO
```

Expected column names: description


### 6.5

Display all suspects that are involved in more than the average number of incidents (per suspect). Show the suspect's name and the number of incidents he or she is involved in.

*Hint:* Start by writing a query to show the average number of incidents per suspect, and then use that query as a subquery.

```sql
TODO
```

Expected column names: name number_of_incidents


## Change the data

### 7.1

Suspect 'John Doe' turned out to be an accomplice in a different incident. Update the database so that 'John Doe' is no longer a suspect in incident 1458 but on incident 1421.

```sql
TODO
```


### 7.2

The chief wants to clean old incident types from the database. Change all incidents that have the type with description 'Blockade' to the (already existing) type with description 'Road Blocked at ___'.

```sql
TODO
```


### 7.3

It appears that suspect 'Dr. Jekyll' is the same person as 'Mr. Hyde'. They have been associated in the same incidents. Update the database to combine these two suspect into one:  'Dr. Jekyll-Hyde', keeping the gender and date of birth from 'Mr. Hyde'. You can (and should) use multiple queries.

```sql
TODO
```


## Create and use a view

### 8.1

Create a `VIEW` called `daily_vehicle_incidents` that, for each day that vehicle-related incidents (those incidents that contain the text *Vehicle* in their types) have been registered, contains the date and the number of vehicle-related incidents for that date, ordered by date.

```sql
TODO ;

SELECT *
FROM daily_vehicle_incidents;
```

Expected column names: date number_of_incidents


### 8.2

Show the total number of incidents (not only vehicle-related) that were registered on the day with the most vehicle-related incidents. Make good use of your `daily_vehicle_incidents` view.

```sql
TODO
```

Expected column names: count





## Make it fast

For this objective, you should assume that all of your database tables contain many millions of rows (as may well be the case when the database has been in use for a decade or two).

### 9.1

Provide the SQL for creating one or multiple indexes that may help speed up your query in answer to *6.1*. 

```sql
TODO
```

### 9.2

Provide the SQL for creating one or multiple indexes that may help speed up your query in answer to *6.2*. 

```sql
TODO
```



## Extend the design

### 10

TODO: copy the plantuml from objective #2 and extend it based on the instruction on the LMS.
