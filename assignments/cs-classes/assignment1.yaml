Introduction:
    - |
        The previous assignment provided a brief introduction to the C# language, but scripting in this manner isn't the recommended approach for C# programming. 
        This is primarily due to the difficulty in maintaining and adding new features to such scripts. Therefore, we will now transform the game using Object-Oriented Programming (OOP), focusing primarily on classes.

        OOP will help structure the game more effectively, making it easier to manage and extend as we add new features.

    -
        link: https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/tutorials/classes
        title: Classes in C#
        info: This written document gives a solid introduction into classes in C#

    -
        link: https://www.youtube.com/watch?v=-ETZ0nOCU14
        title: Classes & Objects  | C# Tutorial
        info: A video that answers the questions, What are classes? What are objects? How are they different?

    -
        link: https://www.youtube.com/watch?v=Reeefq-Nxkk
        title: Constructors  | C# Tutorial
        info: A video showing how to nicely create objects out of your classes using constructors.

    -
        link: https://www.youtube.com/watch?v=wZ5zl4i68Hg
        title: Object Methods  | C# Tutorial
        info: How to use methods in combination with classes

    -
        link: https://www.youtube.com/watch?v=EbW-1fPhXQE
        title: Getters & Setters  | C# Tutorial
        info: A video about properties and reading/modifying them. For now, ignore the private keyword and just make everything public, this will be introduced in the next module.

Class Diagrams:
    - |
        Throughout this module we'll gradually introduce you to class diagrams to direct you to a working solutions. You might notice similarities between class diagrams and Entity-Relationship Diagrams (ERDs).
        However, while ERDs are used to model relational databases, class diagrams serve to represent object-oriented programs. We'll explore some key components of class diagrams below, accompanied by their corresponding C# code implementations.
        As new concepts of OOP are introduced, we'll also introduce the class diagram representations of those concepts.
    -
        link: https://www.visual-paradigm.com/guide/uml-unified-modeling-language/what-is-class-diagram/
        title: Visual Paradigm - What is a class diagram?
        info: A good introduction to class diagrams. Refer to this if something in the above doesn't make sense. Note that this also explains several constructs that we have not introduced so far, only read up to the part where we give explanations here as well.

    - |
        ## Classes

        The following class diagram illustrates a single class named Shape, featuring two properties and two methods (including the constructor):
        
        ```plantuml
        class Shape {
            X: int
            Y: int
            Shape(x: int, y: int)
            Draw()
        }
        ```

        Here's the corresponding C# code for the Shape class:

        ```csharp
        public class Shape
        {
            public Shape(int x, int y)
            {
                X = x;
                Y = y;
            }

            public int X { get; set; }

            public int Y { get; set; }

            public void Draw()
            {
                return; //Implementation goes here
            }
        }
        ```

        ## Level of Detail
        The above example includes comprehensive details about the classes being modelled. However, class diagrams are versatile and can also provide a high-level overview of your system. This is achieved by omitting certain details.

        For instance, a simplified version of our previous diagram might look like this:
        
        ```plantuml
        @startuml
        class Shape {
            +Shape()
            +Draw()
        }
        ```

        Or for an even more abstract representation:

        ```plantuml
        @startuml
        class Shape {
        }
        ```

        ## Associations
        One way that a class can relate to other classes (you'll learn about the other in a later assignment) is to have instances hold references to instances of other classes via a field or property. We call these associations. Associations are modelled using a line between two classes, and may have a label that clarifies the meaning of the relation.
        
        ```plantuml
        Man - Woman: spouse
        ```

        Classes can also have associations with themselves:
        ```plantuml
        Human - Human: spouse
        ```

        In many cases, the label describes the association from the perspective of one of the objects. In that case it can have an arrow (to clarify that the `Employee` does not *employ* the `Boss`):
        ```plantuml
        class Boss
        Boss - Employee: employs >
        ```

        All of these relations are bidirectional, meaning that both objects hold a references to the other object as an instance variable. One way, but far from the only way, to do this in C# is:

        ```csharp
        public class Human
        {
            public Human()
            {
                Spouse = null;
            }

            public Human? Spouse { get; set; }

            public void Marry(Human human)
            {
                Spouse = human;
                human.Spouse = this;
            }	
        }
        ```

        These simplified diagrams are useful for conveying the overall structure of a system without getting bogged down in implementation details. The level of detail you choose depends on the purpose of the diagram and the audience it's intended for.

        ## Directional associations
        When only one object needs to reference another (without reciprocation), we can indicate this unidirectional association using an arrowhead. The arrowhead points towards the class being referenced:
        ```plantuml
        class User
        User -> Color: favorite
        ```

        In this diagram, the label *favorite* doesn't require an additional arrow because the arrowhead already clarifies the direction of the association.

        Here's a C# implementation of this unidirectional association:

        ```csharp
        public class Color
        {
        }

        public class User
        {
            public User()
            {
                FavoriteColor = null;
            }

            public Color? FavoriteColor { get; set; }

            public void SetFavoriteColor(Color color)
            {
                FavoriteColor = color;
            }
        }
        ```

        This code demonstrates how a `User` can have a reference to a `Color` object as their favorite color, while the `Color` class doesn't need to know about the `User`.

        ## Cardinality
        Associations in class diagrams also have a cardinality (or multiplicity), which indicates the number of instances involved in the relationship. By default, we assume a one-to-one relationship, but we can specify different cardinalities by adding numbers or ranges near the class being referenced.

        In the following example, a `Person` can belong to zero or one `Team`, while each `Team` must have one or more members:

        ```plantuml
        Team -> "1..*" Person: members
        class Team {
            +AddMember()
        }
        ```

        This diagram shows that:
        - A `Team` has one or more (1..*) `Person` instances as members.
        - The `Team` class has an `AddMember()` method to manage this relationship.

        Here's a possible C# implementation of this relationship:

        ```csharp
        public class Person
        {
        }

        public class Team
        {
            public List<Person> Members;

            public Team()
            {
                Members = new List<Person>();
            }

            public void AddMember(Person member)
            {
                Members.Add(member);
            }
        }
        ```

        In this code:
        - The `Team` class maintains a list of `Person` objects to represent its members.
        - The `AddMember` method allows new `Person` instances to be added to the team, fulfilling the "one or more" requirement from the diagram.
        - The `Person` class doesn't need to reference the `Team`, reflecting the unidirectional nature of the association in the diagram.

        This implementation demonstrates how cardinality in class diagrams translates to actual code structure, influencing how objects relate to and interact with each other.

        ## Notes
        Class diagrams can include notes to provide additional clarification or context about elements in the diagram. These notes help explain intent, implementation details, or any other relevant information.

        Here's an example of how notes can be used in a class diagram:

        ```plantuml
        class MyThing {
            +MyMethod()
        }

        note right {
            This is a note for the //MyThing// class.
            It can provide additional context or explanations.
        }
        ```


Tips & Tricks: 
    Code conventions:
        - |
            Code conventions in C# differ from what you're used to in Python. Programmers from different languages will likely never agree on a single style, so it's important to adapt. Below are some key points to keep in mind:

            - Every class should be in a separate file. This helps keep your code organized. The class and the file should have the exact same name.
                - In C#, classes are typically organized within namespaces, which is a major structural difference from Python. It’s a core part of the class declaration and file organization.
                - As long as you keep the convention where the namespace is equal to the folder (starting from the root directory), you likely won't get problems with this.
                - Classes have access to files in the same namespace, otherwise you will need a using statement (which you can add pretty easily)
            - C# uses PascalCase (capitalizing the first letter of each word) for naming classes, methods, and properties.
                - As a general guideline PascalCase is generally used for all public identifiers (classes, methods, properties) and camelCase is more for private/internal members and local variables. You will get introduced to public/private in the next assignment.
            - For local variables, fields, and method parameters (elements not visible outside of a class or method), C# typically uses camelCase (lowercase first letter, capitalizing subsequent words).
            - Fields often have an underscore (_) prefix, which allows you to omit the this keyword in constructors when referring to instance fields.
            
            These conventions are not enforced by the compiler, so your code will still run even if you don't follow them. However, adhering to them consistently will make your code more readable and maintainable. Your code will be evaluated based on the four conventions mentioned above.
    
    Getting more out of the solution explorer:
        - |
            Using the Solution Explorer in Visual Studio Code unlocks additional features because it recognizes that you’re working in a C# project. Here are a few ways this can be useful:

            - To create a new class, hover over the project name and select "New File". You’ll get a popup where you can choose Class, and it will automatically set up the class for you.
            - When you hover over sections of code, you might see a small lightbulb icon. This opens a context-dependent menu, offering options to make automatic changes or perform useful actions, such as:
                - When hovering over a class name in a file containing multiple classes, you can select "Move to separate file" to automatically create a new file for that class.
                - You can select multiple lines of code and quickly convert them into a method.
                - After defining a field, you can automatically generate or update the constructor to initialize that field.
                - Adding using statements if classes you use are in another namespace

            You can open the context menu with the shortcut `Ctrl + .`

            Be cautious, though. These suggestions may include changes you’re not familiar with. Feel free to experiment, but avoid using something if you don’t understand what it does. Just because it’s suggested doesn’t mean it’s always better.


Assignment:
    - |
        We are going to transform the solution from the previous assignment into an Object Oriented solution so it gets easier to expand later. We take this step by step. 

        The final class diagram in this assignment looks like this

        ```plantuml
        class Player
        {
            + Name: string
            + Health: int
            + Player(string name)
            + TakeDamage(int amount): void
            + IsDead(): bool
            + PlayTurn(Monster monster): void
        }

        class Monster
        {
            + Number: int
            + Health: int
            + Monster(int number)
            + TakeDamage(int amount): void
            + IsDead(): bool
            + PlayTurn(Player player): void
        }

        class Game
        {
            + Game(Player player, int monsters)
            + Play()
            + RunMonsterBattle(int number)
        }

        Game -> "1..*" Monster
        Game -> Player
        ```

    - Players: 
        -   text: |
                Create a class named Player with the following methods:

                - Constructor: Accepts the player's name and initializes the `Name` property. Sets the `Health` property to a constant `INITIAL_HEALTH` (set to 50).
                - `TakeDamage()`: Reduces the player's `Health` by a specified amount.
                - `IsDead()`: Returns true if the player's `Health` is zero or less, otherwise returns false.
                - `PlayTurn()`: Receives a reference to a `Monster` class (to be defined) and manages the logic of prompting the player for actions and executing them accordingly.

                This transformation will facilitate easier expansion and maintenance of the game by utilizing Object-Oriented Programming principles, starting with encapsulating player-related functionalities into a dedicated class.
            ^merge: feature
            map:
                classes: 1

    - Monsters:
        -   text: |
                Create a class named `Monster` with methods similar to those in the `Player` class, but tailored to the monster's behavior as described in the original implementation. Instead of a name, each monster will have a number.

                Focus on implementing the specific behaviors of the monster, such as taking damage, determining if it's dead, and handling its turn in combat. Don't concern yourself with code duplication for now; we'll address this in the Inheritance assignment.
            ^merge: feature
            map:
                classes: 1

    - Game:
        -   text: |
                To implement the game logic in an Object-Oriented manner, create a class named Game with the following methods:

                - Constructor: Accepts a `Player` object and the number of monsters (an integer), storing them in corresponding `Player` and `Monsters` properties.
                - `Play()`: Calls the `RunMonsterBattle()` method repeatedly for the specified number of monsters.
                - `RunMonsterBattle()`: Implements the logic from the original code that alternates between the player and monster turns until either of them is defeated.
               
                In your `Program.cs` file, the logic should be minimal. It should prompt the player for their name and the number of monsters to battle, then instantiate a `Game` object and invoke its `Play()` method. 
                Optionally, you can separate interaction with the console into a helper class like `InputHandler`, although it's not required for this assignment.

                This approach adheres to Object-Oriented Programming principles by encapsulating the game's logic into dedicated classes (`Player`, `Monster`, and `Game`), promoting better organization and maintainability of the codebase.
            ^merge: feature
            map:
                classes: 1

Background Material:
    - |
        As a reference, we provide you with materials on how to achieve the same concepts in Python.
    -   
        link: https://www.youtube.com/watch?v=apACNr7DC_s
        title: Socratica - Python Classes and Objects
        info: "Python makes it easy to make a class and use it to create objects."
    -
        link: https://www.youtube.com/watch?v=ZDa-Z5JzLYM
        title: "Python OOP Tutorial 1: Classes and Instances"
        info: "This covers much of the same information as the previous video. You can speed up the video if it bores you."
    -   
        link: https://realpython.com/python3-object-oriented-programming/
        title: Object-Oriented Programming (OOP) in Python 3
        info: Similar topics as above, but in text form. You can stop reading before "Inherit From Other Classes in Python".
            