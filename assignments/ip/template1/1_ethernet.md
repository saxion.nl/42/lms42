# Ethernet

In this objective you will be presented with an overview of a network and asked to answer some questions. Provide your answers in this file.

## Questions

Suppose Bob wants to send a picture from his phone to his laptop (given the current LAN)

1. Complete the frame below (replace the '...' with your answer):
    
    | Destination address | Source address | Ether type | Payload |
    | -- | -- | -- | -- |
    | ... | ... | 08 00 | [image data] |
    
2. Assume the size of the picture is 12 Mb. Can it be sent in one frame? Explain why or why not.

...


3. Explain how the receiver checks whether the data has been received correctly? (Give a detailed description including the relevant part of sent packet)

...



```plantuml
@startuml 
!define osaPuml https://raw.githubusercontent.com/Crashedmind/PlantUML-opensecurityarchitecture2-icons/master

!include osaPuml/Common.puml
!include osaPuml/User/all.puml
!include osaPuml/Hardware/all.puml
!include osaPuml/Misc/all.puml
!include osaPuml/Server/all.puml
!include osaPuml/Site/all.puml

package "Local Area Network (LAN)" {
  

' Users
together {
osa_user_green(Bob, "Bob", "User", "")
}

' Devices
together {
osa_iPhone(phone, "10.0.1.12", "MAC: C8-69-CD-4C-0A-0D", "Bob's Phone")
osa_laptop(laptop, "10.0.1.5", "MAC: C8-00-84-E7-36-28", "Bob's Laptop")
}

' Network
osa_device_wireless_router(wifiAP, "10.0.1.3", "MAC: 8E:9C:1f:8E:5E:16", "WiFi Router")
}


Bob ..> laptop: works on
Bob ..> phone: messages on

laptop--> wifiAP
phone --> wifiAP


@enduml
```
