# IP protocol

In this objective you will be presented with an overview of a network and asked to answer some questions. Provide your answers in this file.

## Questions

Suppose Bob uses his laptop to request a HTML page from a web server on the internet.

1. Give an example of a private IP range and explain why it is called "private".

...

2. In the diagram below some information is missing. Assume an ip range of 192.168.0.0/24 for Bob's LAN with a default gateway with IP 192.168.0.1. Complete the frame below (replace the '...' with your answer). 
    1. You are allowed to choose your own (valid) IP addresses for the devices within the LAN.
    2. Don't forget to fill in the ARP table of Bob's laptop.

3. Bob wants to check whether the server is reachable. He opens a terminal on his laptop and types the following command (replace the '...' with your answer):

...

4. Given the highly simplified overview below, how many hops does a request from Bob's laptop to the web server?

...



```plantuml
@startuml 
!define osaPuml https://raw.githubusercontent.com/Crashedmind/PlantUML-opensecurityarchitecture2-icons/master

!include osaPuml/Common.puml
!include osaPuml/User/all.puml
!include osaPuml/Hardware/all.puml
!include osaPuml/Misc/all.puml
!include osaPuml/Server/all.puml
!include osaPuml/Site/all.puml

package "Local Area Network (LAN)" {
  

' Users
together {
osa_user_green(Bob, "Bob", "User", "")
}

' Devices
together {
osa_iPhone(phone, "IP: ...", "MAC: C8:69:CD:4C:0A:0D", "Bob's Phone")
osa_laptop(laptop, "IP: ...", "MAC: C8:00:84:E7:36:28", "Bob's Laptop")
note right 
    ARP table
    | **Internet address**   | **Physical address** |
    | ... | ... |
    | ... | ... |
end note
}

' Network
osa_device_wireless_router(wifiAP, "IP: ...", "MAC: 8E:9C:1f:8E:5E:16", "WiFi Router")
}


package "Internet (WAN)" {
    together {
        osa_database(cloud, "ISP Router", "ISP")
        osa_database(router1, "Router", "Network")
        ' osa_database(router2, "Router 2", "Network")
        ' osa_cloud(cloud, "Internet", "Network")
        osa_server(server, "178.21.117.25", "Ubuntu Server 20.04 LTS", "Hosting Provider")
    }
}

Bob ..> laptop: works on
Bob ..> phone: messages on

laptop--> wifiAP
phone --> wifiAP

wifiAP <--> cloud
router1 <-r-> cloud

server <-r-> router1


@enduml
```
