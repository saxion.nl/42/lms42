const udp = require("dgram");
const BufferBuilder = require('buffer-builder');
const BufferReader = require('buffer-reader');
const { ArgumentParser } = require("argparse");

// Initialize the argument parser to process command line arguments
const parser = new ArgumentParser({
    prog: "saxtp-client",
    description: "Download a file using the SaxTP protocol.",
});

// Define the expected command line arguments
parser.add_argument("-s", "--server", {
    help: "The name or IP address of the server to download from.",
    default: "sd42.nl"
});
parser.add_argument("-p", "--port", {
    help: "The UDP port to connect to.",
    default: 29588, type: Number
});

// Get an object containing our command line arguments
const args = parser.parse_args();

// Create a UDP socket (client), UDP is connectionless.
const socket = udp.createSocket("udp4");

// Prepare data to send to the server, for this you should use the buffer builder
// to built a message according to the protocol
const dataToSend = [];

// Send a message to the server
socket.send(dataToSend, args.port, args.server, (error) => {
    if (error) {
        console.error(`Failed to send data to server: ${error}`);
        process.exit();
    }
});

// Subscribe to the message event, which will be called whenever the server sends us a message
socket.on('message', (receivedData, info) => {
    console.log(`Received ${receivedData.length}B from the server`);
    console.log(receivedData);
    // The message will be sent as bytes, you'll want to use the buffer reader to
    // interpret the data according to the protocol
});


// TODO: The above provides an example of how to use the argument parser, and dgram packages