# Internship report


----------------------------------------


## 1. The basics

- Your name: 
- Internship: first or second
- Company name: 
- Company supervisor name: 
- Saxion study coach name: 
- Start date: 
- End date: 

- Company description: 
- Company head count: 
- Team description: 
- Team head count: 


----------------------------------------


## 2. Journal (reflectio)

At the end of every week reflect on how your internship is going. Look at your journal entries and summarize what you have accomplished that week and which challenges you faced.

- Week 1:
    - Accomplishments:
        1. **TODO**: write what you are proud of (e.g. complex piece of code you have build, new technology you have learned)
        2. ...
    - Challenges:
        1. **TODO**: write about your struggles (e.g. issues that took more time than expected). Also reflect on how you handled the problem and if you could do better in the future.
        2. ...
- Week 2:
    - Accomplishments:
        1. ...
    - Challenges:
        1. ...


----------------------------------------


## 3. Tech stack

...

Below are a template and an example. **TODO**: Please remove once you don't need them anymore.

### SOME_TECH
- Description: 
- Company usage: 
- My usage: 
- Curriculum: 
- Opinion: 

### Python
- Description: A high-level interpreted programming language.
- Company usage: Example.com uses Python for its API backend.
- My usage: I created a new REST endpoint and fixed a couple of backend bugs.
- Curriculum: Python usage at Example.com uses type hints and annotations much more heavily than we learned in the curriculum.
- Opinion: I would prefer to use a statically typed language for the backend, to prevent many run-time bugs.

### Comparison

**TODO**: Second internship only. Remove this section if this is your first internship. (Though it may be smart to think ahead!)

| | COMPANY_NAME_A | COMPANY_NAME_B |
| -- | -- | -- |
| Backend language | PHP | Go |
| Backend framework | Laravel | Home grown |
| Development environment | Docker compose | Custom shell script |
| Backend tests | Extended Cypress test suite | Non-existent |
| Production hosting | Dedicated servers | AWS Lambda |
| Database | MySQL | AWS Amplify |
| Linting | Etc... | Etc... |
| Etc...

----------------------------------------


## 4. Contributions

...

Below is a template. **TODO**: Please remove once you don't need it anymore.

### EXAMPLE_FEATURE

- What: 
- Learning: 
- Duration: 
- Team work: 
- Demo: ![](media/example-feature.webp) or [video](./media/example-feature-after.webm)
- Source code: [patch](./sources/example-feature.patch) or directory `./sources/example-repo/`
- Status: 


----------------------------------------


## 5. Roles

...

Below is a template. **TODO**: Please remove once you don't need it anymore.

### SOME_ROLE_TITLE

- Description: 
- Count: 
- Profile: 
- Typical collaboration: 
- Your collaboration: 


----------------------------------------


## 6. Development process

### Description

..._

### Comparison

**TODO**: Second internship only. Remove this section if this is your first internship. (Though it may be smart to think ahead!)

| | COMPANY_NAME_A | COMPANY_NAME_B |
| -- | -- | -- |
| Sprints | 2 weeks | 4 weeks + 2 cooldown |
| Daily standups | Long discussions | Brief status updates |
| Weekly meetings | None | Plan adjustment |
| Bug fixing | Direct if urgent, or planned for sprint | During cooldown |
| Retrospective | Etc... | Etc... |
| Etc...

### Recommendations

- ...
- ...
- ...
