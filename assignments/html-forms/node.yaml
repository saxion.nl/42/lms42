name: Web forms, fonts & icons
ai: The student should already know HTML and CSS, including concepts like flexbox. His knowledge is limited to static HTML, no programming yet. He is currently learning about web forms, fronts & icons.
goals:
    htmlforms: 1
    icons-fonts: 1
    design: 1

intro:
  "Intermezzo: using the keyboard":
  - |
      When programming, many software developers try to prevent using the mouse/touchpad as much as possible, and use keyboard shortcuts instead. Why?
      - Once you're used to working with keyboard shortcuts, you'll be a lot faster at most tasks than someone who is using the mouse.
      - Using a mouse or touchpad for extended periods of time may be bad for your wrist, shoulders and other joints.
      - All the cool kids are using the keyboard.
  -
      link: https://docs.kde.org/stable5/en/khelpcenter/fundamentals/kbd.html
      title: KDE - Common Keyboard Shortcuts
      info: Hopefully you already know the most important ones here. But it sure doesn't hurt to go over the list and pick up a few new ones.

  -
      link: https://www.youtube.com/watch?v=Xa5EU-qAv-I
      title: VSCode Keyboard Shortcuts For Productivity
      info: This video may help you on your way becoming a keyboard warrior in VSCode. Printed out VSCode shortcut sheet cheats should also be floating around our class rooms somewhere. Put one on your desk for a couple of weeks, to remind you to use the shortcuts.

  - |
    In case you feel like geeking out and becoming a true keyboard warrior, the awesomeness scale goes up quite a bit further...
    - Many developers use a *modal* editor like [VIM](https://www.loginradius.com/blog/engineering/vim-getting-started/) or ([Kakoune](https://kakoune.org/) or [Helix](https://helix-editor.com/)), that allows you to perform complex text editing operations using just sequences of single key presses. (Instead of shortcuts that require simultaneously pressed keys, which are relatively slow to perform.) The keys required by common operations are those close to the 'home' positions of your hands (left index finger on `f`, right index finger on `j`), reducing hand movement as much as possible. If you'd like to experiment with this, without leaving the familiar environment of Visual Studio Code, there's the *Vim emulation for Visual Studio Code* extension.
    - Keyboards in this part of the world usually have a QWERTY layout. It turns out QWERTY is not optimal for typing speed. To fix that, one Mr. August Dvorak created the [**Dvorak** alternative keyboard layout](https://en.wikipedia.org/wiki/Dvorak_keyboard_layout), in order to squeeze out a couple of additional words per minute. KDE's keyboard settings allow you to easily switch to Dvorak. But if you do this, you're probably going to need to paste some empty white stickers on your keys, and write the Dvorak-meaning for the keys on them. Until you've (re)learned to type blind, that is. (After that, you can take of the stickers, but you'll be cursing whenever you need to type on somebody else's keyboard.)

resources:
  Web forms:
    - In this series of lessons, we're creating "static" HTML files. That means that what's on the pages never changes, unless of course the web admin (you!) changes the files. In a future series of lessons (1.3 Dynamic web) we'll learn how to create dynamic web pages, retrieving data from a database using Python. 
    - "For now, we'll just the HTML-side of this: HTML user input elements. This set of HTML elements allow users to input text, check a checkbox, press a button and select a date, among other things. Normally, data a user inputs in this way would be submitted to a dynamic web server, but we're not doing that part for now. We'll just be showing the input elements, ignoring the user's input."
    - "We recommend that you scan through the following pages quickly, and then use them as a reference:"
    - link: https://www.w3schools.com/html/html_form_elements.asp
      title: "w3schools: HTML Form Elements"
      info: A list of all user input elements.
    - link: https://www.w3schools.com/html/html_form_input_types.asp
      title: "w3schools: HTML Input Types"
      info: The &lt;input&gt; element can handle all types of user input. This article provides a list with examples for each.
    - link: https://www.w3schools.com/html/html_form_attributes.asp
      title: "w3schools: HTML Input Attributes"
      info: Additional attributes that can be used to change the behavior of form elements.
  Web fonts:
    - link: https://internetingishard.netlify.app/html-and-css/web-typography/index.html
      title: "HTML & CSS is hard: Web Typography"
      info: A pretty thorough introduction on applying typography using HTML & CSS.
  Web icons:
    - link: https://www.youtube.com/watch?v=gdL7XWlFJ9c
      title: "Optimise page load time: icon font or SVG?"
      info: "A quick introduction to the ways in which you can add icons to a modern web site. Main points: 1) you want to use vector icons. 2) inline SVGs are often more suitable then icon fonts."
    - link: https://www.youtube.com/watch?v=or7amkb0Pk8
      title: Web Icons with No External Libraries
      info: Hands-on demo on how to find SVGs on Iconify, and use them inline within your site.
    - link: https://iconify.design/icon-sets/
      title: Iconify icon sets
      info: A website containing allowing you to search icons within many free icon collections. The site makes it easy to obtain the SVG for an icon, which we can just paste into our HTML code.
