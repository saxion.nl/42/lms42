const net = require('net');

const PORT = 8080;

// At the end of this assignment you should have a data structure that looks something like this,
// Each user is a key in a dictionary with information for each user.
const userData = {
    "Mario": {
        "numberOfVisits": 3,
        "userAgent": "Mozzarella/5.0 (Bowser; K00-p4) Fireflower/64.0",
        "ip": "1.2.3.64"
    }
}
