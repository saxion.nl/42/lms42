### Hello and welcome!

Hi there, great that you have found this file! 
As the name might indicate, the readme.md is a file you might want to read each time you encounter it, since it might contain important information.
Don't worry to much about the .md extension (markdown) of the file as you will learn more about file extensions throughout the program.


For this primer you just have to re-write the examples given in the assignment on the SD42 website line-by-line. 
We recommend you to write this by hand, and not use a copy-past functionality since it's here to help you get familiar with how to write code yourself.
Also, the use of AI for this assignment is highly discouraged as you will just need to re-write the code that was given by us. 


### For the experienced....

As mentioned in the assignment, if you already have some coding experience you can skip this assignment if you feel confident enough. 
