# Data modeling

## Case 1: StackOverflow

### Entities

- entity 1
  - attribute 1
  - attribute 2
- entity 2
  - attribute 1
  - attribute 2
- entity 3
  - attribute 1
  - attribute 2

### Logical ERD

We've made a start for you:

```plantuml
!include https://sd42.nl/static/style-light.plantuml

' Note that the asterisk (*) is used set the attribute as mandatory (NOT NULL)

entity User {
    *name
}

entity Question {
    *title
    *timestamp
    description
}

User ||--|{ Question
```

### Physical ERD

We've made a start for you:

```plantuml
!include https://sd42.nl/static/style-light.plantuml

entity User {
    *id: integer
    --
    *name: text
}

entity Question {
    *id: integer
    --
    *title: text
    *timestamp: timestamp
    *user_id: integer <<FK>>
    description: text
}

User ||--|{ Question
```

Before you continue...
In the objectives below you'll be asked to design more databases. It is probably a good idea to ask a teacher or a smart-looking fellow student for some feedback on your StackOverflow design, in order not to keep making the same mistakes.

## Case 2: Twitter

### Physical ERD

```plantuml
!include https://sd42.nl/static/style-light.plantuml

entity TODO {

}

```


## Case 3: *up to you*

```plantuml
!include https://sd42.nl/static/style-light.plantuml

entity TODO {
    
}

```