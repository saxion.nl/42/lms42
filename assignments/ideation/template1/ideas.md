# Idea 1
## Title

## Job stories


-----

# Idea 2
## Title

## Job stories


-----

# Idea 3
## Title

## Job stories


-----

# Evaluate your ideas

## Idea 1
Feedback:
 - Is it clear what problem the idea solves? (ask the person to describe the problem in own words)

 - Is it clear how the idea solves the problem? (ask the person to describe the problem in own words)

 - Do you think the idea is a solution to the problem? (why / why not)


## Idea 2
Feedback:
 - Is it clear what problem the idea solves? (ask the person to describe the problem in own words)

 - Is it clear how the idea solves the problem? (ask the person to describe the problem in own words)

 - Do you think the idea is a solution to the problem? (why / why not)


## Idea 3
Feedback:
 - Is it clear what problem the idea solves? (ask the person to describe the problem in own words)

 - Is it clear how the idea solves the problem? (ask the person to describe the problem in own words)

 - Do you think the idea is a solution to the problem? (why / why not)
