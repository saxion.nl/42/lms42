import sys
from dataclasses import dataclass
from abc import ABC, abstractmethod
import traceback
from typing import NoReturn


def error(message) -> NoReturn:
    """Display an error message and exit the application with an error code."""
    traceback.print_stack(file=sys.stdout)
    print(f"\nERROR: {message}.", file=sys.stderr)
    sys.exit(1)


def print_tree(data, tab='', depth=32):
    """Display an indented view of the AST."""
    next_tab = tab+'    '
    if depth < 1:
        print("<<DEPTH-LIMIT>>")
    elif isinstance(data, str | int | float | bool) or data is None:
        print(repr(data))
    elif isinstance(data, list):
        print('[')
        for item in data:
            print(next_tab, end='')
            print_tree(item, next_tab, depth-1)
        print(tab + ']')
    else:
        print(data.__class__.__name__ + ' {')
        attrs = {attr: getattr(data, attr) for attr in dir(data) if not callable(getattr(data, attr)) and not attr.startswith("_")}
        for key, value in attrs.items():
            print(f'{next_tab}{key}: ', end='')
            print_tree(value, next_tab, depth-1)
        print(tab+'}')


class RunData:
    """Class used for maintaining run-time data (variable values) when running (interpreting) the program."""
    def __init__(self):
        self.variables: dict[str, bool | int | str | None] = {}


class TreeNode(ABC):
    """Any node in the Abstract Syntax Tree should inherit from this abstract base class."""
    @abstractmethod
    def run(self, data: RunData) -> int | str | bool | None:
        pass


@dataclass
class Block(TreeNode):
    """A list of statements to be executed sequentially."""
    statements: list[TreeNode]
    def run(self, data: RunData):
        for statement in self.statements:
            statement.run(data)


@dataclass
class Program(TreeNode):
    """The top-level node for any program."""
    block: Block
    def run(self, data: RunData):
        # Clear variables before we start (again).
        data.variables.clear()
        self.block.run(data)


@dataclass
class Declaration(TreeNode):
    """Create a new variable, optionally initializing it."""
    identifier: str
    expression: TreeNode | None
    def run(self, data: RunData):
        # If there's an expression, run it and use its value to initialize the variable.
        data.variables[self.identifier] = self.expression.run(data) if self.expression else None


@dataclass
class IfStatement(TreeNode):
    """Depending on `condition` run either `yes` or `no` (if the latter exists)."""
    condition: TreeNode
    yes: TreeNode
    no: TreeNode | None
    def run(self, data: RunData):
        cond = self.condition.run(data)
        if cond:
            self.yes.run(data)
        # TODO: Else.


@dataclass
class WhileStatement(TreeNode):
    """Run `statement` while `condition` is `True`."""
    condition: TreeNode
    statement: TreeNode
    def run(self, data: RunData):
        pass
        # TODO


OPERATORS = {
    '+': lambda a, b: a + b,
    '-': lambda a, b: a - b,
    '*': lambda a, b: a * b,
    '/': lambda a, b: a / b,
    '%': lambda a, b: a % b,
    '==': lambda a, b: a == b,
    '!=': lambda a, b: a != b,
    '<': lambda a, b: a < b,
    '>': lambda a, b: a > b,
    '<=': lambda a, b: a <= b,
    '>=': lambda a, b: a >= b,
    '||': lambda a, b: a or b,
    '&&': lambda a, b: a and b,
}

@dataclass
class BinaryOperator(TreeNode):
    """Evaluate `left` and `right` combine them using `operator` and return the result.
    The '=' operator is a special case. It checks if `left` is a `VariableReference` and
    assigns the `right` value to it."""
    left: TreeNode
    operator: str
    right: TreeNode
    def run(self, data: RunData):
        right = self.right.run(data)

        if self.operator == '=':
            if not isinstance(self.left, VariableReference):
                error("Left part of assignment must be a variable")
            # TODO: Retrieve the value from `data.variables`.
            return

        left = self.left.run(data)

        # TODO: Execute the operator and return the result. (Example: `OPERATORS['+'](1,2)`)


@dataclass
class Literal(TreeNode):
    """A literal string ("test") or number (42)."""
    value: int | str
    def run(self, data: RunData):  # noqa: ARG002
        return self.value


@dataclass
class VariableReference(TreeNode):
    """Reading a variable's value."""
    name: str
    def run(self, data: RunData):
        pass
        # TODO: Retrieve the value from `data.variables`.


FUNCTIONS: dict = {
    'print': print,
    'input': input,
    'parseInt': int,
}

@dataclass
class FunctionCall(TreeNode):
    """Call one of the built-in functions (print, input, parseInt)."""
    name: str
    arguments: list[TreeNode]
    def run(self, data: RunData):
        pass
        # TODO
        # Hint: you can use `func(*my_list)` to pass list items as individual arguments to a function.
