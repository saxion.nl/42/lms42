# This function computes the fibonacci sequence.
#
# Also see: https://en.wikipedia.org/wiki/Fibonacci_number
def fib(n):
    fib_seq = [0, 1]
    if n > 2:
        for i in range(1, n):
            fib_seq.append(fib_seq[i-1] + fib_seq[n-2])
    return fib_seq

# This function computes the triangular numbers sequence.
# A triangular number correspond to the number of dots that would appear in an equilateral triangle
# when using a basic triangular pattern to build the triangle.
#
# Also see: https://www.101computing.net/triangular-numbers/#:~:text=The%20first%2010%20numbers%20of,36%2C%2045%2C%2055%2C%20%E2%80%A6&text=So%20using%20an%20iterative%20approach,View%20on%20trinket.io
def tri(n):
    number = 0
    tri_seq = []
    for i in range(1, n):
        number = number + i
        tri_seq.append(i)
    return tri_seq

# Converts a sequence of strings into a sequence of integers.
# Spaces around the numbers should be ignored.
# List items that cannot be converted to integers should be skipped.
def convert_to_int_list(items):
    result = []
    for item in items:
        result.append(int(item))
    return items

# This function validates whether the sequence is a sorted list.
def check_sequential(seq):
    for i in range(len(seq)):
        if seq[i] > seq[i-1]:
            raise ValueError("Invalid sequence (order not ascending)!")

# This function takes two lists and raises an error in case the lists are
# of unequal length or contain items that are not the same.
def check_lists_equal(a, b):
    for i in range(len(a)):
        if a[i] != b[0]:
            raise ValueError(f"List are unequal at position {i}")




################################################
# TEST CASES
#
# DO NOT MODIFY BELOW THIS POINT!
################################################


# Test check_lists_equal

check_lists_equal([1, 2, 3, 4], [1, 2, 3, 4])
check_lists_equal(["a"], ["a"])
check_lists_equal([], [])

try:
    check_lists_equal([1, 2, 3, 4, 5], [1, 2, 3, 4, 4])
    raise RuntimeError("check_lists_equal should have raised an RuntimeError")
except ValueError:
    pass

try:
    check_lists_equal(["a"], ["b"])
    raise RuntimeError("check_lists_equal should have raised an RuntimeError")
except ValueError:
    pass

try:
    check_lists_equal([1, 2, 3], [1, 2])
    raise RuntimeError("check_lists_equal should have raised an RuntimeError")
except ValueError:
    pass

try:
    check_lists_equal([1, 2], [1, 2, 3])
    raise RuntimeError("check_lists_equal should have raised an RuntimeError")
except ValueError:
    pass

print('PASSED: check_lists_equal tests')



# Test fib

fib_10 = fib(10)
check_lists_equal(fib_10, [0, 1, 1, 2, 3, 5, 8, 13, 21, 34])

fib_2 = fib(2)
check_lists_equal(fib_2, [0, 1])

fib_1 = fib(1)
check_lists_equal(fib_1, [0])

fib_0 = fib(0)
check_lists_equal(fib_0, [])

print('PASSED: fib tests')


# Test tri

tri_10 = tri(10)
check_lists_equal(tri_10, [1, 3, 6, 10, 15, 21, 28, 36, 45, 55])

tri_1 = tri(1)
check_lists_equal(tri_1, [1])

tri_0 = tri(0)
check_lists_equal(tri_0, [])

tri_minus_1 = tri(-1)
check_lists_equal(tri_minus_1, [])

print('PASSED: tri tests')



# Test check_sequential

check_sequential([1, 2, 3, 4])
check_sequential([1, 2, 30, 400])
check_sequential([])
check_sequential([5])

fib_list = fib(50)
check_sequential(fib_list)

tri_list = tri(100)
check_sequential(tri_list)

try:
    check_sequential([4, 10, 50, 20])
    raise RuntimeError("check_sequential should have raised an RuntimeError")
except ValueError:
    pass

try:
    check_sequential([500, 10, 50, 70])
    raise RuntimeError("check_sequential should have raised an RuntimeError")
except ValueError:
    pass

print('PASSED: check_sequential tests')


# Test convert_to_int_list
int_list_3 = convert_to_int_list(["123", "456", "789"])
check_lists_equal(int_list_3, [123, 456, 789])

int_list_3 = convert_to_int_list(["123", "456 ", " 789"])
check_lists_equal(int_list_3, [123, 456, 789])

# This one is tricky!
rand_str = "00958 01093 01545 02260 02577 03374 03987 04330 05325 05659 05883 06407 07336 07635 07767 07937 08204 08956 09264 09998 10007 10383 10385 11015 11167 11225 12049 12313 12347 13252 13513 14166 15059 15319 15845 16219 16747 17021 17851 18548 18795 19189 19941 20415 21075 21744 22742 22923 23134 24082 24302 24736 25104 25273 26024 26356 27214 27307 28O88 28553 28835 29500 29635 30021 30480 31290 32177 32317 32571 33269 33330 33553 33875 33947 34412 35070 35713 36123 36329 36835 37336 38016 38756 39221 39757 39941 40029 40487 41316 41488 42313 42804 43646 43970 44222 45202 45509 46108 46850"
rand_strs = rand_str.split(' ')
rand_ints = convert_to_int_list(rand_strs)
check_sequential(rand_ints)
assert len(rand_ints) == len(rand_strs)-1
# Notice the -1 here! Check the convert_to_int_list comments for a hint about what should cause this.

print('PASSED: convert_to_int_list tests')
print('--------[ END ]----------')
