MENU = """
Choose one of these options:
1. Simple exception
2. Sometimes an exception
3. Exception function
4. Different exceptions (or no exception)
0. Exit"""


def throw_exception():
    print("Throwing a ZeroDivisionError exception")
    # TODO: Implement this function so that it always throws a ZeroDivisionError (which you get by dividing a number by 0)


def main():
    while True:
        print(MENU)
        selection = int(input("Choose an option: "))
        if selection == 0:
            break

        if selection == 1:
            # TODO: Catch this (specific!) exception and let the program continue normally.
            number = int("a")
            print(f"Number is {number}")
        elif selection == 2:
            # TODO: Catch the (specific) exception and print "That is not a number" in case of an error.
            number = int(input("Type in a number: "))
            print(f"Number is {number}")
        elif selection == 3:
            # TODO: Call the method throw_exception()
            # TODO: Implement the method so that it `throws` an exception.
            # TODO: Catch the exception here with a try except and print "Oopsie" in case of an error.
            throw_exception()
        elif selection == 4:
            # TODO: Within a single try-block, catch specific exceptions, multiple excepts allowed. Print "That is not a number"
            # in case of an  invalid input and "Error fetching item at {number}" when item can not be found.
            my_list = ["a", "b", "c"]
            number = int(input("Type in a number: "))
            print(f"Number is {number}")
            item = my_list[number]
            print(f"Item is {item}")


if __name__ == "__main__":
    main()
