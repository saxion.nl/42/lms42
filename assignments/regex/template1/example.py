# This file is meant as a test case for highlighter.py.
# Running it doesn't do anything useful (that we know of).

from ai_2100 import infer

print(r'''
After this, we'll see
some looping...
I hope you'll #enjoy it!
''', end='\n\n')

# Do some "looping"
for _name, t3st in infer(123, -20, 45.4 - ai_2100.BASE):
    print(f"- item {_name}", t3st, 123, "another string")

print("a 'string' containing \"escaped\" quotes")
print("one string", 123, "another string", '''and another triple quoted one''')
