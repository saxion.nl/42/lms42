name: Backtracking
goals:
    backtracking: 1
resources:
    -
        link: https://www.youtube.com/watch?v=gBC_Fd8EE8A
        title: Think like a programmer - Backtracking
        info: An introduction video about backtracking algorithms.
    -
        link: https://medium.com/@lavanya.ac/introduction-to-backtracking-8ada04f0255
        title: Introduction to Backtracking
        info: Maze solving using backtracking. The code examples are in JavaScript, but don't let that scare you. 

assignment1:
    Sudoku solver:
        -
            ^merge: feature
            text: |
                Implement a solver for 9x9 Sudoku's, using backtracking! After each step, print the game board and `sleep(0.5)`, to show what your algorithm is doing.

                If you want, you can get (a lot of!) help from the videos below. We recommend you think about the problem yourself for 10 minutes first. If you're not making any progress, watch the first 10 minutes of the first video, and see if you can implement the solver based on that. If really needed, you can view parts of the rest of the videos for hints.
        -
            link: https://www.youtube.com/watch?v=eqUwSA0xI-s
            title: Python Sudoku Solver Tutorial with Backtracking 1
            info: Explanation of how to implement a Sudoku solver using backtracking. Please stop at around 10:00m when he start to look at Python code. Try to implement the solver yourself - even if you don't succeed, this is a really useful exercise! If you need a hint, view (a part of) the rest of this video and the next.
        -
            link: https://www.youtube.com/watch?v=lK4N8E6uNr4&t=842s
            title: Python Sudoku Solver Tutorial with Backtracking 1
            info: The rest of the solver implementation. Only watch this if you need to.
    Travel planner:
        - |
            Let's imagine a simplified version of this world, that has only nine countries: Australia, Belgium, China, Denmark, Egypt, France, Germany, Haiti and Kenya. Each of these countries has nine cities, named just 1 through 9. So the biggest city in Germany would just be called `Germany 1` or `G1`.

            In this restricted world, your travel options are rather limited as well:

            - Between some cities within a country, it is possible to take a train. Train connections are indicated in the diagram below using the small black arrows. For example, one could travel by train from `A4` via `A1` to `A2` (all of which are in Australia).
            - Airplanes allow you to travel between cities in different countries. However, airline connections are only available when:
              - The two countries have signed a travel agreement, indicated by the large purple arrows. So there can be flights from `A` to `B` but not from `A` to `E`.
              - The cities have the same number (1 through 9) within their country. So we may travel from `A1` to `B1` but not from `A1` to `B2`.
              - Airlines only fly between cities that share the same main economic activity, indicated in the diagram below by color. (Red being *finance*, blue being *IT*, yellow being *entertainment* and green being *agriculture*.) So travel between `D2` and `F2` is possible, while travel between `D3` and `F3` is *not*.

            <img src="travel.svg" class="no-border" style="max-width: 100%; width: 100%; max-height: none;">

            Some examples:

            - A traveler who wants to travel from `D7` to `D8`, can fly from `D7` to `A7`, take a train from there to `A8` and then fly back to `D8`.
            - A traveler who wants to go from `K2` to `D3`, can fly from `K2` to `B2`, take a train to `B1`, fly to `A1`, train to `A2`, fly to `D2` and finally take a train to `D3`.

        -
            ^merge: feature
            text: |
                In the `travel.py` file, the data from the image is already provided as a set of classes. We recommend that you study the code and play around with it in the *repl*, in order to understand the structure.

                Your job is to implement the `_find_route_recursive` function, that searches for a route between two cities. Check your results by hand. Good luck!

                **Hint:** At any time the state of the traveler can be described by the city where the traveler is situated at that moment. From such a state a number of follow-up trips are possible, leading to new states to explore. However, if a trip would lead to a city where the traveler has already been, we don't need to explore that path any further, as any solution starting from this point could be reached more directly without going back and forth between cities. Without this restriction, your algorithm will run forever (or end with a *stack overflow*, due to infinite recursion).
                **Hint**: Take in consideration that traveling by train is cheaper than by plane. So first discover by train then by plane.