"use strict";

const express = require('express');
const data = require('./data');

// Create an express app
const app = express();

// Configure express to automatically decode JSON bodies
app.use(express.json());

// Make sure tables and initial data exist in the database
data.applySchema();

// This route causes the database to reset to what's in `schema.sql`.
// This should *not* be enabled in production, it's for testing only.
if (process.env.ALLOW_RESET_DATABASE) {
	app.put('/reset_database', function(req,rsp) {
		data.dropAllTables();
		data.applySchema();
		rsp.json({});
	});
}

// Leave a route for '/' returning status code 200 in place, as it is used by the 'npm test' script
// to detect that the server is up and running.
app.get('/', function(request, response) {
	response.json({success: true})
});

// TODO!


// Return a 404 if none of the above routes matched.
app.use(function(request, response, next) {
	response.status(404).json({error: "Invalid resource"})
});

// Set the default error handler
app.use(function (err, req, res, next) {
	console.error(err.stack)
	res.status(500).send('Internal server error')
});
  
// Start accepting requests
const listener = app.listen(process.env.PORT || 3000, "0.0.0.0", function () {
	console.log(`Server running at http://${listener.address().address}:${listener.address().port}`);
});
