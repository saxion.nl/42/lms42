Assignment:
    - |
        Create Javascript to enhance a web form. The basic web forms has been provided in the template. It's your job to implement the objectives below within `form.js`. You shouldn't need to modify the CSS or HTML files.

        You can view the page by right clicking the `index.html` in *VSCode* and selecting *Open with Live Server*.

    - link: https://video.saxion.nl/media/WebTech+assignment+1A+Javascript/1_fhcxige2
      title: Requirements video
      info: This video demos each of the objectives below. Please note that the video has been, uh, borrowed from HBO-ICT. It contains a few useless references to week numbers, and to Glitch (a service we're not using). Also, tapping the *Submit* button will just display an error (instead of displaying the posted data), which is fine.

    - ^merge: feature
      weight: 1
      text: |
          User name box gets a border color when it's not empty. It becomes red (CSS class `bad`) when the user name contains less than 3 characters or when non-alphanumeric characters are used. Otherwise it becomes green (CSS class `good`).

          ```{hint}
          - You can *listen* for the `"input"` event, which fires on input elements whenever their value changes.
          - Always have the JavaScript console in the browser development tools open. Without error messages (and debug output), you're flying blind!
          ```

    - ^merge: feature
      weight: 2
      text: |
          Email address box is colored similarly, using an email-address checker that rejects address that:

          - contain obviously invalid characters,
          - contain less or more than exactly one '@' sign, or
          - have an empty user name (the part before the '@')
          - have a domain name (the part after the '@') without any '.' characters,
          - have empty components (the parts around the '.' characters) in the domain name

          This can be solved using a single regular expression (if you dare!), or by custom logic (probably involving `String.split`).

    - ^merge: feature
      weight: 1
      text: |
          Birthdate, similar. Using provided `checkDate()` function.

          ```{hint}
          You may notice these checks becoming a little repetitive. Try to come up with a way to keep your code small.
          ```

    - ^merge: feature
      weight: 2
      text: |
          Password field, similar coloring. Only it should show a reason when the box is red. Reasons are:

          - At least 6 characters. 
          - Must include a lower letter.
          - Must include an upper case letter.
          - Must include a non-letter.

    - ^merge: feature
      weight: 1
      text: |
          Password again field should be colored based on whether it matches the first password field.

          ```{hint}
          This field should also update its class when the *first* password field is changed. However, you may want to come back to this later, as this will work automatically if you take the approach *hinted* in objective #7.
          ```

    - ^merge: feature
      weight: 3
      text: |
          Clicking 'Add custom field' should add a row with key and value fields. They should be submitted in the form as `customKey1` / `customValue1`, `customKey2` / `customValue2`, etc.

          You may *not* use `innerHTML` for this. Use `document.createElement` instead.

          *Hint:* create a `<label>`, add two `<input>` children to it, and append the label to `.customFields`.

    - ^merge: feature
      weight: 3
      text: |
          The submit button should become green when all fields have been filled in according to the above requirements and the i-agree-checkbox has been checked.

          ```{hint}
          One easy way to do this, that will probably also reduce and simplify your code, is to check *all form fields* whenever there's an `input` event on the `<form>`. (Remember that events *bubble up* the HTML tree, so you'll only need to set a single event listener.) Yes, this will cause you computer to do some needless work on every keystroke, but computers are pretty fast (understatement!) and *correctness* is more important than speed.
          ```

    - ^merge: feature
      weight: 1
      text: |
          The submit button should ignore clicks when it is not green.

    - must: true
      text: |
          You *must not* add/edit any HTML or CSS for this. You *should* of course *study* the HTML and CSS, to know what you're working with.
