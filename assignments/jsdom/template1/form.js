"use strict";

/**
* Determines if a given year/month/day combination exists.
* @param {string} dateStr For example: '31-12-1999'
* @returns {boolean}
*/
function checkDate(dateStr) {
    // split in 3 parts:
    let dmy = dateStr.split('-');
    if (dmy.length != 3) return false;
    if (dmy[2]<1000 || dmy[2]>9999) return false; // unreasonable year
    
    let date = new Date();
    date.setYear(dmy[2]);
    date.setMonth(dmy[1]-1); // first month is 0
    date.setDate(dmy[0]); // first date is, of course, 1
    // if the date hasn't been altered, it's valid:
    return date.getDate()==dmy[0] && date.getMonth()==dmy[1]-1 && date.getFullYear()==dmy[2];
}

/**
 * Called on page load. Implement your logic here.
 * @param {HTMLFormElement} The form element to work with.
 */
function initFormChecking(form) {
    
    // .....
    
}

