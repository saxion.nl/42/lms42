let router = module.exports = require('express').Router();

// Attach the router from the test.js file to the /test base url
router.use('/test', require('./test'));

// localhost:3000/hello
router.get("/hello", (req, res) => {
    res.send("Hello World!");
});
