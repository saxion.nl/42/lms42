const express = require('express');
const data = require('./data');
const cors = require('cors');

// Create an express app
const app = express();

// Configure express to automatically decode JSON bodies
app.use(express.json());

// This lets browsers know that any website is allowed to make requests to our API.
// In practice we'd rarely want this to be open, instead you'd define which websites
// you allow to make requests to your API.
app.use(cors());

// Make sure tables and initial data exist in the database
data.applySchema();

// This route causes the database to reset to what's in `schema.sql`.
// This should *not* be enabled in production, it's for testing only.
if (process.env.ALLOW_RESET_DATABASE) {
	app.put('/reset_database', function(req,rsp) {
		data.dropAllTables();
		data.applySchema();
		rsp.json({});
	});
}

// Include the routers
app.use(require('./routers'));

// Start accepting requests
const listener = app.listen(process.env.PORT || 3000, "0.0.0.0", function () {
	console.log(`Server running at http://${listener.address().address}:${listener.address().port}`);
});
