<Program> ::= <Block> EOF

<Block> ::= <Expression> (EOL+ <Expression>)* EOL*

<Expression> ::= <BinaryOperator> | <Assignment> | <InputOutput> | <IfExpression> | <LoopExpression>

<BinaryOperator> ::= <Atom> ( ("&&" | "||" | "==" | "!=" | "+" | "-" | "*" | "/" | "%") <BinaryOperator> )?

<Atom> ::= STRING | NUMBER | IDENTIFIER | "(" <Block> ")"

<Assignment> ::= TODO: The 'set' expression.

<InputOutput> ::= TODO: print! str? int? expressions.

<IfExpression> ::= TODO

<LoopExpression> ::= TODO
