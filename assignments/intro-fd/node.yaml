name: Domain modeling and wireframing
description: Create a domain model and wireframes for a case description that has been worked out using Example Mapping.
goals:
    datamodeling: 1
    wireframing: 1
days: 1
depend: datamodeling
resources:
    - 
        link: https://openpracticelibrary.com/practice/example-mapping/
        title: Example Mapping
        info: This blog gives a short introduction on Example Mapping.

assignment:
    - Introduction: |
        Writing applications can be hard. It gets more complicated when you need to write an application for someone else, let's say a **customer**. How do you know that the application you are creating is what the customer wants? This is where **functional design** comes into play. 

        Before you start writing code you need to know what the desired application should **do** (what functionality it has). Now you can immediately start writing your code the moment you know what the customer wants but odds are that his or her wishes are inconsistent or not specific enough. As a result you will probably have to rewrite large parts of your application after the initial version! Either because it is not what the customer envisioned or that new features pop in the customers head upon seeing the application you have created. 
        
        In order to minimize the need for revisions you could create a functional design of the application. The functional design should give the customer an impression of what the application will look like and how it will work. You can show this, and even let the customer play with this design before you even write a single line of code. Changes to your functional design are easier to make (and [cheaper](https://rnjn.in/glossary/boehms-law/)) because you do not have to rewrite any code. When the customer is satisfied with the design it should lead to fewer changes in your application (changes will still happen of course, always).

        During this module you are asked to create wireframes in order to give an impression of what the application will look like. You can either choose to do this in **WireText** or in **Quant-UX**.

    - Tasks:
      -   
        I want to use Wiretext!: |
          You should install *WireText* extension for Visual Studio Code, which allows you to quickly create and modify wireframes as text. You should be able to find it in your VSCode extension marketplace, or you can [download it here](https://open-vsx.org/extension/SaxionSD/wiretext).

          Install the plugin, study its documentation, and play around with it in order to understand how it works. When you have the basics down, use it to create your wireframes for this assignment.
      -
        If you want to use Quant-UX!: |
          Quant-UX is an online prototyping tool to design, test and analyze your visual designs. You can find the documentation here [https://app.quant-ux.com/#/help.html](https://app.quant-ux.com/#/help.html)  

    - Assignment: |
        In this assignment we take a first step in creating a functional design - create a domain model and the wireframes for a *chores* case. The technique will we use is called *Example Mapping*. Watch the video provided in the resources to get a better understanding of what this is. 
        
        In the template folder you will find an worked out example of the a beer tracking system (called *beertab*). In the `example-beertab.md` and `example-beertab.wt` you will find the case description (including the domain model) and the wireframes respectively. Study these files before you start working on the objectives.

    - Domain model:
      -
        link: https://www.markdownguide.org/basic-syntax
        title: Basic Markdown Syntax
        info: Nearly all Markdown applications support the basic syntax outlined in this document. There are minor variations between Markdown viewers.
      -
        link: https://www.youtube.com/watch?v=jtWj-rreoxs
        title: Map out a Domain Model
        info: Introduction into how map entities from use cases to a domain model.
      -
        link: https://plantuml.com/ie-diagram
        title: PlantUML - Entity Relationship Diagram
        info: Documentation on ERDs in PlantUML
      -
        text: |
          The domain model is a representation of meaningful real-world concepts (relevant to the software that needs to be built). Your task is to create a **logical domain model** for the application you are going to build. If you don't recall the difference between the logical and physical data model, please look back at your data modeling assignment from SQL. We will use an ERD to describe our domain model and in contrast to the example in the video we ask you should also write down the attribute types.

          - Create the domain model using PlantUML (see the file `case-chores.md` in the template folder).
          - Your domain model should be fully consistent with the case description and your wireframes (next objective).

        0: Domain model does not give a clear impression of the application to be built
        4: Domain contains all entities with proper relationship (including correct cardinality).
        map:
          datamodeling: 1

    - Wireframes:
      - 
        link: https://www.justinmind.com/blog/low-fidelity-vs-high-fidelity-wireframing-is-paper-dead/
        title: "Low fidelity vs high fidelity wireframes: what's the difference?"
        info: A blog explaining the difference between low - and high fidelity wireframes and what the benefits of each is.
      -
        text: |
          Create **low fidelity** wireframes for your application, demonstrating how the end product might work (but not what it will look like). Your design should show which visual elements are found in your application screens and how the user can interact with these elements.

          Your tasks:
          - Create a wireframe for every screen in your **mobile** application (in `case-chores.wt` or in quant-ux). Do *not* use standard filler text (like *Lorem ipsum*), but try to come up with some realistic example data. That allows you to verify that all data fields actually make sense, and helps viewers understand how the application will be used in practice.
          - For every screen you should explain (*in a note next to the screen*) the following:
            1. Which elements are interactive (though this should be obvious in your wireframe).
            2. For each interactive element the effect it has in the application. For example a "Save" button in your application will save information in an applications database. You should describe these actions. Although this "Save" example is straightforward some actions a not (for example a push notification that is sent to a different user when a message is created in the application). Buttons/links that refer to another WireText screen generally don't need any further description.
          - Your wireframes should be fully consistent with the case description and your domain model.

        0: Wireframes do not give a clear impression of the application to be built
        4: Clear wireframes including intuitive navigation between pages.
        map:
          wireframing: 1
