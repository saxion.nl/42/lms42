# General purpose registers
a: int32 666
b: int32 0

macro test INSTRUCTION EXPECTED {
    set32 *a 72846
    set32 *b 15870
    INSTRUCTION *a *b
    if=32 *a EXPECTED
    write8 '.'
    if>32 *a EXPECTED
    write8 'x'
    if<32 *a EXPECTED
    write8 'x'
}

main:
    test and32 7310
    
    test or32 81406

    test xor32 74096

    test add32 88716

    test sub32 56976

    test mul32 1156066020

    test div32 4

exit:
    write8 '\n'
    set16 *ip 0
