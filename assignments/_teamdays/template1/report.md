# Individual Project Reports

This report should take you about half an hour. You should write it alone, without your teammates looking over your shoulder.


## Who did what?

For each of your team mates (yourself included), describe in one or two sentences what he/she contributed to the project. You may also add a note if he/she collaborated especially good or bad.

**Name 1:** Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
**Name 2:** Etc...


## Collaboration failures?

Mention the most important things that went wrong collaborating. For each, propose how you would collaborate differently on your next (different!) project, to prevent this from happening again.

**Problem:** Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
**How to prevent:** Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

**Problem:** Etc..
**How to prevent:** Etc..


## Collaboration successes?

Especially in case you didn't have much to say in the previous section, please mention the most important things that went right during the project collaboration.

**Success:** Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

**Success:** Etc..


## Restrospective / Daily standup notes

If you participated in either one of these sessions, please include your notes or a general summation about this session in your own words. You can best do this by answering the questions you tackled during the session. 
So, for the retrospective: 

- What went well?
- What did not go so well?
- How can this be improved? 

and / or for the daily standup: 

- What did you do?
- What are you going to do today?
- Are there any impediments that you face, and how can you resolve them? 