name: JavaScript & the DOM
description: Enhance a web page using client-side-only JavaScript.
goals:
    javascript: 1
    dom: 1
depend: ux-exam, webapps-exam
resources:
    JavaScript:
        -
            link: https://www.destroyallsoftware.com/talks/wat
            title: Destroy all software - Wat
            info: A 4 minute video to get in the mood for learning JavaScript!
        -
            link: https://www.youtube.com/watch?v=Sh6lK57Cuk4
            title: The Weird History of JavaScript
            info: Some fun (and tangentially useful) background on JavaScript. It also foreshadows some of the topics we will encounter in this module.
        -
            link: https://www.youtube.com/watch?v=W6NZfCO5SIk
            title: "JavaScript Tutorial for Beginners: Learn JavaScript in 1 Hour"
            info: This video walks you through the very basics of JavaScript. You may be able to watch it at double speed.
        -
            link: https://runestone.academy/runestone/books/published/JS4Python/TheBasics/toctree.html
            title: "JavaScript for Python Programmers - Part 1: the Basics"
            info: The first part of a free book, that can be used as a reference or as an alternative to the video above.

    The DOM:
        -
            link: https://www.youtube.com/watch?v=H63dVFDuJDM
            title: "JavaScript Tutorial For Beginners #32 - What is the DOM in JavaScript?"
            info: Just a short introduction video.
        - The below videos are pretty complete, but maybe a bit longer than you need. You may want to increase the pace, or use the section index to skip to the parts you want to know about.
        -
            link: https://www.youtube.com/watch?v=v7rSSy8CaYE&list=PLZlA0Gpn_vH9k5ju1yq9qCDqvtuTVgTr6&index=17
            title: Learn JavaScript DOM Traversal In 15 Minutes
        -
            link: https://www.youtube.com/watch?v=y17RuWkWdn8&list=PLZlA0Gpn_vH9k5ju1yq9qCDqvtuTVgTr6&index=18
            title: Learn DOM Manipulation In 18 Minutes
        -
            link: https://www.youtube.com/watch?v=XF1_MlZ5l6M&list=PLZlA0Gpn_vH9k5ju1yq9qCDqvtuTVgTr6&index=19
            title: Learn JavaScript Event Listeners In 18 Minutes

assignment:
    - |
        Create Javascript to enhance a web form. The basic web forms has been provided in the template. It's your job to implement the objectives below within `form.js`. You shouldn't need to modify the CSS or HTML files.

        You can view the page by right clicking the `index.html` in *VSCode* and selecting *Open with Live Server*.
        
    -
        link: https://video.saxion.nl/media/WebTech+assignment+1A+Javascript/1_fhcxige2
        title: Requirements video
        info: This video demos each of the objectives below. Please note that the video has been, uh, borrowed from HBO-ICT. It contains a few useless references to week numbers, and to Glitch (a service we're not using). Also, tapping the *Submit* button will just display an error (instead of displaying the posted data), which is fine.
    
    -
        ^merge: feature
        weight: 1
        text: |
            User name box gets a border color when it's not empty. It becomes red (CSS class `bad`) when the user name contains less than 3 characters or when non-alphanumeric characters are used. Otherwise it becomes green (CSS class `good`).
        
    -
        ^merge: feature
        weight: 2
        text: |
            Email address box is colored similarly, using an email-address checker that rejects address that:

            - contain obviously invalid characters,
            - contain less or more than exactly one '@' sign, or
            - have an empty user name (the part before the '@')
            - have a domain name (the part after the '@') without any '.' characters,
            - have empty components (the parts around the '.' characters) in the domain name

            This can be solved using a single regular expression (if you dare!), or by custom logic (probably involving `String.split`).

    -
        ^merge: feature
        weight: 1
        text: |
            Birthdate, similar. Using provided `checkDate()` function.

    -
        ^merge: feature
        weight: 2
        text: |
            Password field, similar coloring. Only it should show a reason when the box is red. Reasons are:

            - At least 6 characters. 
            - Must include a lower letter.
            - Must include an upper case letter.
            - Must include a non-letter.

    -
        ^merge: feature
        weight: 1
        text: |
            Password again field should be colored based on whether it matches the first password field.

    -
        ^merge: feature
        weight: 3
        text: |
            Clicking 'Add custom field' should add a row with key and value fields. They should be submitted in the form as `customKey1` / `customValue1`, `customKey2` / `customValue2`, etc.

            You may *not* use `innerHTML` for this. Use `document.createElement` instead.

            *Hint:* create a `<label>`, add two `<input>` children to it, and append the label to `.customFields`.

    -
        ^merge: feature
        weight: 3
        text: |
            The submit button should become green when all fields have been filled in according to the above requirements and the i-agree-checkbox has been checked.

    -
        ^merge: feature
        weight: 1
        text: |
            The submit button should ignore clicks when it is not green.

    -
        must: true
        text: |
            You *must not* add/edit any HTML or CSS for this. You *should* of course *study* the HTML and CSS, to know what you're working with.
