using Avalonia.Controls;
using Avalonia.Interactivity;

namespace AvaloniaApp;

public partial class MainWindow : Window
{
    // This file is just for demo purposes
    // During the assignment, create your own Avalonia project
    // You can put it in this existing solution or in your own, whatever you prefer.
    public MainWindow()
    {
        InitializeComponent();

        // Subscribe to the Click event
        MyButton.Click += Button_Click;
    }

    //Event Handler
    private void Button_Click(object sender, RoutedEventArgs e)
    {
        DisplayText.Text = "Button clicked!";
    }
}
