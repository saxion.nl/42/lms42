MegaSoft X-L:
- |
    <video class="side" src="x-l.mp4" autoplay muted loop controls></video>
    Everybody loves to hate MicroSoft Excel. How hard can it be to make something better, right? Let me introduce you to MegaSoft X-L (alpha 0.0.1)! Let's build it!

    <div style="clear: both;"></div>

-
    must: true
    title: Technical requirements
    text: |
        For this assignment you are *not* to use any client-side JavaScript **libraries**, and you must *not* edit any `.html` files. Instead, you should directly use the DOM API to create the user interface, using methods/properties such a [document.createElement](https://developer.mozilla.org/en-US/docs/Web/API/Document/createElement), [innerText](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/innerText), [className](https://developer.mozilla.org/en-US/docs/Web/API/Element/className), [setAttribute](https://developer.mozilla.org/en-US/docs/Web/API/Element/setAttribute), [append](https://developer.mozilla.org/en-US/docs/Web/API/Element/append), [addEventListener](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener), etc. `innerHTML` (or similar approaches) may *not* be used.

-
    title: Create the layout and empty table
    ^merge: feature
    text: |
        The initial screen from the video above, using JavaScript in `x-l.js`. It doesn't need to handle any user input yet. So when opening `index.html` in the browser (using the VSCode Live Server for instance), it should display the app title header, the two buttons, and the table containing just the *Delete* column, the *Name* column and one row with an empty name cell.

        You can use `static-example.html` to peek at what the desired DOM structure should look like. (Don't forget the `<tbody>`!)

        ```{hint}
        Remember to always have the JavaScript console in the browser development tools open. Without error messages (and debug output), you're flying blind!
        ```

-
    link: https://www.crockford.com/domjs.html
    title: Plain Old Javascript and the DOM
    info: Shows an example of a simple helper function to add nodes to the DOM.


-
    title: DOM creation abstraction
    ^merge: feature
    text: |
        As you may have noticed in the previous objective, directly modifying DOM nodes takes quite a lot of JavaScript code. Create an *abstraction* function to make working with the DOM more convenient for you. It should at least handle:

        - Creating DOM element with a given tag name.
        - Optionally setting their class names.
        - Optionally setting their text content.
        - Adding elements to their parents.

        Modify your earlier code, and make sure that it indeed becomes shorter and more readable.

        You can draw inspiration from the article linked above, but other approaches are also fine!

-
    title: Add rows
    ^merge: feature
    text: |
        When clicking the *Add row* button, an empty row (with a delete cross and the right number of columns) should be added to the table.

        Try not to duplicate any pieces of code from the first objective (but create helper functions instead).

-
    title: Modify cells
    ^merge: feature
    text: |
        When clicking a cell, its text content should be replaced by an `<input>` that has the text content as `value`. The `<input`> should get keyboard `focus()` immediately.

        When the element looses keyboard focus (indicated by the `blur` event) or when enter is pressed (search the web for how to detect this), the input event should be replaced by just plain text content again.

-
    title: Delete rows
    ^merge: feature
    text: |
        When clicking the delete ✗ for a row, that row should (unsurprisingly) be deleted.

-
    title: Add columns
    ^merge: feature
    text: |
        When clicking the *Add column* button, we should ask the user for a column name (using `prompt()`), and if the name was not left empty, a column should be added. This involves creating additional `<td>` elements for each of the existing rows.

        Again, try not to duplicate any pieces of code from the earlier objectives (but create helper functions instead).

Testing:

- |
    We've prepared some automated *end-to-end* tests (or *integration* tests, if you prefer) for this project. The tests use [Playwright](https://playwright.dev/), which provides us with a cool way to have an actual browser (in this case Chromium, the Open Source base for Google Chrome) automatically interact with our app!

-
    link: https://www.youtube.com/watch?v=q-xS25lsN3I
    title: What is Node.js and how it works (explained in 2 minutes)
    info: We're going to be (passively) using Node.js and NPM now. So it'd be good to know what these are! 😊
    
-
    link: https://www.youtube.com/watch?v=Xz6lhEzgI5I
    title: Getting Started with Playwright and VS Code
    info: Introduction video for using Playwright from within VS Code. Shows you how to install everything you need, what tests look like, and how to run them.

-
    must: true
    title: Make sure all tests pass
    text: |
        - Use *Add/Remove Software* to install `npm` (which will bring along `nodejs` automatically). If you're using Mac OSX you can `brew install npm`.
        - Install the Playwright VSCode extension.
        - Use the extension to install Playwright into your X-L project (*unselect* all options except Chromium), as shown in the video.
        - Move the provided `x-l.spec.js` file into the newly created `tests` directory (or the directory may have been named `e2e`).
        - You should now be able to execute the tests in that file from within VS Code. You'll probably want to enable the *Show browser* checkbox in the *Testing* VSCode pane (the lab flask icon on the left).

        Make sure that your app passes all the tests! You may have to change your DOM structure a little bit to match the expectations of the tests.
