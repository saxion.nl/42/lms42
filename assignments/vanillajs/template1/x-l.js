"use strict";
// The line above causes JavaScript to crash on some errors that would otherwise remain hidden. Leave it there!


function start() {
    let test = document.createElement('h1');
    test.innerText = "Hello world!";
    document.body.appendChild(test);
}

window.addEventListener('load', start);
