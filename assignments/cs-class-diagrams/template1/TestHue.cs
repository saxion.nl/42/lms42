﻿// using Xunit;

// namespace ClassDiagrams;

// public class TestHue
// {
//     private readonly Hub _hub;

//     public TestHue()
//     {
//         _hub = new Hub();
//     }

//     [Fact]
//     public void Test_AddRoom()
//     {
//         //Arrange        
//         Room kitchen = new("kitchen");

//         //Act
//         _hub.AddRoom(kitchen);

//         //Assert
//         Assert.Equal("kitchen", kitchen.Name);
//         Assert.Equal("kitchen {\n}", kitchen.ToString());
//         Assert.Equal(kitchen, _hub.GetRoom("kitchen"));
//         Assert.Null(_hub.GetRoom("no such room"));
//     }

//     [Fact]
//     public void Test_AddLight()
//     {
//         //Arrange
//         Room kitchen = new("kitchen");
//         Light kitchenCeiling = new("ceiling");

//         //Act
//         kitchen.AddLight(kitchenCeiling);

//         //Assert
//         Assert.Equal("ceiling", kitchenCeiling.Name);
//         Assert.Equal("ceiling → l0", kitchenCeiling.ToString());
//         Assert.Equal(kitchenCeiling, kitchen.GetLight("ceiling"));
//         Assert.Null(kitchen.GetLight("No Such Light"));
//         Assert.Equal("kitchen {\n    ceiling → l0\n}", kitchen.ToString());
//     }

//     [Fact]
//     public void Test_Brighness()
//     {
//         //Arrange
//         Room kitchen = new("kitchen");
//         Light kitchenCeiling = new("ceiling");
//         kitchen.AddLight(kitchenCeiling);

//         //Act
//         kitchenCeiling.SetState(new LightState(42));

//         //Assert
//         Assert.Equal("kitchen {\n    ceiling → l42\n}", kitchen.ToString());
//     }

//     [Fact]
//     public void Test_ColorLights()
//     {
//         //Arrange
//         Room bedroom = new("bedroom");

//         //Act
//         bedroom.AddLight(new Light("bedside left"));
//         bedroom.AddLight(new Light("bedside right"));
//         bedroom.AddLight(new ColorLight("closet"));

//         //Assert
//         Assert.Equal("bedroom {\n    bedside left → l0\n    bedside right → l0\n    closet → l0 h0 s0\n}", bedroom.ToString());

//     }

//     [Fact]
//     public void Test_Atmosphere()
//     {
//         //Arrange
//         Room bedroom = new("bedroom");
//         bedroom.AddLight(new Light("bedside left"));
//         Light bedsideRight = new("bedside right");
//         bedroom.AddLight(bedsideRight);
//         LightState deepRed = new ColorLightState(30, 324, 100);
//         bedroom.SetState(deepRed);

//         //Assert
//         Assert.Equal("bedroom {\n    bedside left → l30\n    bedside right → l30\n}", bedroom.ToString());

//         //Act
//         bedsideRight.SetState(new LightState(50));

//         //Assert
//         Assert.Equal("bedroom {\n    bedside left → l30\n    bedside right → l50\n}", bedroom.ToString());
//     }

//     [Fact]
//     public void Test_Scenes()
//     {
//         Room bedroom = new ("bedroom");
//         bedroom.AddLight(new Light("bedside"));
//         bedroom.AddLight(new ColorLight("closet"));
//         LightState deepRed = new ColorLightState(30, 324, 100);
//         bedroom.SetState(deepRed);
//         bedroom.StoreScene("romantic reading");
//         bedroom.SetState(new LightState(100));

//         Assert.Equal("bedroom {\n    bedside → l100\n    closet → l100\n}", bedroom.ToString());
//         bedroom.StoreScene("bright");

//         bedroom.ActivateScene("romantic reading");
//         Assert.Equal("bedroom {\n    bedside → l30\n    closet → l30 h324 s100\n}", bedroom.ToString());

//         bedroom.ActivateScene("bright");
//         Assert.Equal("bedroom {\n    bedside → l100\n    closet → l100\n}", bedroom.ToString());

//     }

//     [Fact]
//     public void Test_PrintComplexHub()
//     {
//         Room kitchen = new("kitchen");
//         _hub.AddRoom(kitchen);

//         Light kitchenCeiling = new("ceiling");
//         kitchen.AddLight(kitchenCeiling);
//         kitchenCeiling.SetState(new LightState(42));

//         Room bedroom = new("bedroom");
//         _hub.AddRoom(bedroom);

//         Light bedside = new("bedside");
//         bedroom.AddLight(bedside);
//         bedroom.AddLight(new ColorLight("closet"));

//         bedroom.SetState(new LightState(100));

//         Assert.Equal("Hub>>>\nkitchen {\n    ceiling → l42\n}bedroom {\n    bedside → l100\n    closet → l100\n}<<<Hub", _hub.ToString());
//     }
// }