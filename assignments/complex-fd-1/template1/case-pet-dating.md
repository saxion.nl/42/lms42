# Goals

## Main goal
[describe the goal]


## Tasks
[list the tasks]
- ...

# Example Mapping
For the top 3 tasks work out the rules and examples

### Story: TODO

**Rule 1:**  TODO

- **Example 1.1:** The one where....
    1. ...

- **Example 1.2:** The one where...
    1. ...

**Rule 2:** TODO

- **Example 2.1:** The one where....
    1. ...

- **Example 2.2:** The one where...
    1. ...


## Questions
[list your questions here]


# Domain model

```plantuml

entity "Entity" as Entity {
  *attribute: text
}

note left of Entity
  This is an example. Please change it ;)
end note



```

# User flows
For the top 3 tasks you have defined above you should create a user flow (so one user flow per task)


```plantuml

start
:name of screen;
:action;
if (decision?) then
  :error screen;
  detach
endif
:success screen;
:do something (action);
stop
```
