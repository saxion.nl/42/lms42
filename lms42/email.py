from smtplib import SMTP
import datetime
import os
import flask

def send(to_addr, subject, message_text, fake=False):
    host = os.environ.get('SMTP_HOST', '').strip()
    user = os.environ.get('SMTP_USER', '').strip()
    password = os.environ.get('SMTP_PASSWORD', '').strip()
    from_addr = os.environ.get('SMTP_FROM', '').strip() or f"Saxion Software Development <{user}>"

    headers = {
        'Content-Type': 'text/plain; charset=utf-8',
        'Content-Disposition': 'inline',
        'Content-Transfer-Encoding': '8bit',
        'From': from_addr,
        'To': ", ".join(to_addr) if isinstance(to_addr, list) else to_addr,
        'Date': datetime.datetime.now().strftime('%a, %d %b %Y  %H:%M:%S %Z'),
        'X-Mailer': 'python',
        'Subject': subject
    }

    msg = ''
    for key, value in headers.items():
        msg += f"{key}: {value}\n"
    msg += f"\n{message_text}\n"

    if fake or not host:
        base = '/tmp/lms42-email'
        for i in range(9, 0, -1):
            try:
                os.replace(f"{base}.{i-1}", f"{base}.{i}")
            except:  # noqa: E722
                pass
        with open(f"{base}.0", 'w') as file:
            file.write(msg)
        if flask.has_request_context():
            if fake:
                flask.flash(f"Email written to {base}.0")
            else:
                flask.flash(f"Please configure an SMTP server in config.sh - email written to {base}.")
        return

    smtp = SMTP(host=host, port=587)
    smtp.connect(host, 587)
    smtp.starttls()
    if user:
        smtp.login(user, password)

    smtp.sendmail(from_addr, to_addr, msg.encode("utf8"))
    smtp.quit()
