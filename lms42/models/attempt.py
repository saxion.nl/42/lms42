from typing import Optional
from ..app import db, app
from datetime import datetime, timedelta, timezone
import json
import os
import shutil
import sqlalchemy as sa
from sqlalchemy.ext.mutable import MutableList
from flask_login import current_user
import random


FORMATIVE_ACTIONS = {
    "passed": "The student passes.",
    "repair": "The student needs to correct some work.",
    "failed": "The student needs to start anew.",
}


class Attempt(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)

    __table_args__ = (
        sa.Index('attempt_status_index', 'status', 'student_id'),
        sa.Index('attempt_student_submit', 'student_id', 'submit_time'),
        sa.UniqueConstraint('student_id', 'node_id', 'number'),
    )

    student_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False)
    student = sa.orm.relationship("User", foreign_keys="Attempt.student_id", back_populates="attempts")

    grade_teacher_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'))
    grade_teacher = sa.orm.relationship('User', foreign_keys='Attempt.grade_teacher_id', backref='grade_attempts')

    number = sa.Column(sa.Integer, nullable=False)

    node_id = sa.Column(sa.String, nullable=False)
    variant_id = sa.Column(sa.SmallInteger, nullable=False)

    start_time = sa.Column(sa.DateTime, default=datetime.now(timezone.utc), nullable=False)
    deadline_time = sa.Column(sa.DateTime)
    upload_time = sa.Column(sa.DateTime)
    submit_time = sa.Column(sa.DateTime)

    # Valid status:
    #   awaiting_approval
    #   in_progress
    #   needs_grading
    #   needs_consent
    #   passed
    #   repair
    #   failed
    #   paused
    #   awaiting_recording
    #   not_graded
    #   fraudulent
    status = sa.Column(sa.String, nullable=False)

    # TODO: consider renaming this property
    # Valid values:
    #   yes
    #   deadline
    #   forfeit
    #   accidental
    finished = sa.Column(sa.String)

    credits = sa.Column(sa.Integer, nullable=False, default=0)
    
    avg_days = sa.Column(sa.Float, nullable=False)

    alternatives = sa.Column(sa.JSON, default={}, nullable=False)

    done_objectives = sa.Column(MutableList.as_mutable(sa.ARRAY(sa.Integer)), default=[], nullable=False)

    total_hours = sa.Column(sa.Float, nullable=False, default=0)
    
    record_status = sa.Column(sa.String) # paused, in_progress, error, finished, removed
    max_screenshot_id = sa.Column(sa.Integer, default=0, server_default="0", nullable=False)
    last_screenshot_time = sa.Column(sa.DateTime)

    data_deleted = sa.Column(sa.Boolean, nullable=False, default=False)

    def assign_grade_teacher(self, teacher_id: Optional[int]) -> bool:  # noqa: UP007 (CI breaks if we change this)
        """Assigns a teacher for grading this attempt.

        Args:
            teacher_id (Optional[int]): The `id` of the teacher that needs to grade the assignment.
                If `None` is passed the teacher (if assigned) is removed for the assignment.

        Returns:
            bool: Returns `True` if the teacher is assigned (or cleared in the case where `None`
            is passed as an argument).
        """
        if self.status in {'needs_grading', 'needs_consent'}:
            self.grade_teacher_id = teacher_id
            return True
        return False
    
    @property
    def require_teacher_feedback(self) -> bool:
        """Most assignments require a teacher to provide feedback to a student, however some
        assignments can be graded without the teacher providing feedback to the student (this
        is the case for exams and assignments marked with the 'grade_without_student' flag).

        Returns:
            bool: Return `True` if a teacher needs to discuss the submission with the student.
        """
        return not self.node["grade_without_student"]

    @property
    def hours(self):
        """Returns the number of hours spent on the assignment, excluding structural absent days and regular
        off-time (such as evenings, weekends and vacations).
        We assume that when submit_time is set, the `total_hours` property will be set to our return value.
        As our return value shouldn't change from then on, we will just return `total_hours`.
        """
        if self.submit_time is not None:
            return self.total_hours
        leave_days = set() if self.credits else {ad.date for ad in AbsentDay.query.filter_by(user_id=self.student_id, exempt_progress=True)}
        return self.total_hours + working_days.get_working_hours_delta(self.start_time, datetime.now(timezone.utc), leave_days, bool(self.credits))

    @property
    def latest_grading(self):
        return self.gradings.order_by(Grading.id.desc()).first()

    @property
    def directory(self):
        return os.path.join(app.config['DATA_DIR'], 'attempts', f"{self.node_id}-{self.student_id}-{self.number}")

    @property
    def node(self):
        return curriculum.get('nodes_by_id').get(self.node_id)

    @property
    def node_name(self):
        node = self.node
        if not node:
            return self.node_id
        return node["name"]

    def write_json(self):
        d = utils.model_to_dict(self)
        d["student"] = {
            "full_name": self.student.full_name,
            "email": self.student.email,
        }
        d["gradings"] = [utils.model_to_dict(grading) for grading in self.gradings]
        with open(os.path.join(self.directory, "attempt.json"), "w") as file:
            file.write(json.dumps(d, indent=4))
        
    def delete_data(self):
        self.data_deleted = True
        db.session.commit()
        shutil.rmtree(self.directory, ignore_errors=True)

    def set_ungraded(self):
        self.status = "not_graded"
        self.finished = "finished"
        self.total_hours = self.hours
        self.submit_time = datetime.now(timezone.utc)
        db.session.commit()

    # All exams with 5, 6 and 10 grades need consent, as well as those with >= 15 ECTS and a random 10% of the rest.
    def is_consent_needed(self, grade, form):
        if 'mark_fraudulent' in form:
            return False
        if form.get('request_consent'):
            return True
        return "ects" in self.node and (grade in {5, 6, 10} or self.node["ects"] >= 15 or random.randrange(0, 10) == 0)
        

def get_notifications(attempt, grading):
    if attempt.status == "in_progress":
        msg = "In progress for"
    elif attempt.status == "paused":
        msg = "Paused after"
    elif attempt.finished == "forfeit":
        msg = "Given up after"
    elif attempt.finished == "deadline":
        msg = "Submitted due to deadline after"
    elif attempt.finished == "accidental":
        msg = "Cancelled accidental attempt after"
    elif attempt.status == "not_graded":
        msg = "Not finished after"
    else:
        msg = "Finished after"

    notifications = [
        f"{msg} {round(attempt.hours / working_days.DAY_HOURS, 1)} working days."
    ]

    notifications.append(f"Started at {utils.utc_to_display(attempt.start_time)}.")

    if attempt.upload_time:
        notifications.append(f"Work uploaded at {utils.utc_to_display(attempt.upload_time)}.")

    if attempt.submit_time:
        notifications.append(f"Submitted at {utils.utc_to_display(attempt.submit_time)}.")
        if current_user.is_teacher and not attempt.data_deleted:
            notifications.append(f'Grading command: <code class="click-to-copy">lms grade {attempt.student.short_name}@{attempt.node_id}~{attempt.number}</code>')

    if attempt.data_deleted:
        notifications.append("Attempt data no longer available.")

    if attempt.deadline_time:
        if attempt.status == "in_progress":
            notifications.append(f"Deadline: {utils.utc_to_display(attempt.deadline_time)}.")

        exceed = working_days.get_working_hours_delta(attempt.deadline_time, attempt.submit_time or datetime.now(timezone.utc))
        if exceed > 0:
            notifications.append(f"<strong>Deadline exceeded</strong> by {round(exceed, 1)} hours!")

    if grading:
        if current_user.is_teacher or not grading.needs_consent:
            notifications.append(f"Graded {grading.grade} by {grading.grader.full_name} on {utils.utc_to_display(grading.time)}.")
        if grading.needs_consent:
            notifications.append("Awaiting consent by a second examiner.")
        else:
            if grading.consent_user_id:
                notifications.append(f"Consent given by {grading.consenter.full_name} on {utils.utc_to_display(grading.consent_time)}.")
            if grading.grade_motivation:
                notifications.append(f"Teacher feedback:<div class='notification is-info'>{utils.unsafe_markdown_to_html(grading.grade_motivation)}</div>")
            if attempt.status in FORMATIVE_ACTIONS:
                notifications.append(f"<strong>{FORMATIVE_ACTIONS[attempt.status]}</strong>")

    return notifications


from . import curriculum
from .grading import Grading
from .. import utils, working_days
from .user import AbsentDay
from ..app import schedule_job


def delete_old_data():
    attempts = Attempt.query \
        .filter_by(data_deleted=False) \
        .filter(datetime.now() - Attempt.submit_time >= sa.sql.expression.case([
            (Attempt.credits > 0, timedelta(days=8*365)),
        ], else_=timedelta(days=365))) \
        .all()

    for attempt in attempts:
        attempt.delete_data()


schedule_job(func=delete_old_data, trigger="cron", hour=4, minute=15, second=0)
