from ..app import db
from .. import utils
from . import curriculum
import sqlalchemy as sa
import datetime
import flask


class Grading(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)

    attempt_id = sa.Column(sa.Integer, sa.ForeignKey('attempt.id'), nullable=False, index=True)
    attempt = sa.orm.relationship("Attempt", backref=sa.orm.backref("gradings", lazy="dynamic"))

    time = sa.Column(sa.DateTime, default=datetime.datetime.utcnow, nullable=False)

    grader_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False, index=True)
    grader = sa.orm.relationship("User", foreign_keys="Grading.grader_id")

    grade = sa.Column(sa.Integer, nullable=False)
    grade_motivation = sa.Column(sa.String)

    passed = sa.Column(sa.Boolean, nullable=False)

    objective_scores = sa.Column(sa.ARRAY(sa.Float), nullable=False)
    objective_motivations = sa.Column(sa.ARRAY(sa.String))

    needs_consent = sa.Column(sa.Boolean, nullable=False, default=False)
    consent_user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'))
    consenter = sa.orm.relationship("User", foreign_keys="Grading.consent_user_id")
    consent_time = sa.Column(sa.DateTime)

    def announce(self):
        if not self.attempt.credits:
            return

        extnode = curriculum.get_extnode(self.attempt.node_id, self.attempt.student)
        module_name = extnode["module"]["name"] if extnode else self.attempt.node_id
        student = self.attempt.student

        flask.flash(f"Remember to publish the grade in [Bison](https://saxion.educator.eu/judgment?identifier=beoordelingperstudent_saxion): a {self.grade} for {self.attempt.student.full_name} ({self.attempt.student.student_number}) regarding {module_name} submitted on {utils.utc_to_display(self.attempt.submit_time)}. Don't finalize it yet.")

        if student.discord_id and student.discord_notification_exam_reviewed:
            if self.attempt.status == "passed":
                content = f"You passed {module_name} with a {round(self.grade)}!"
            else:
                content = f"You failed {module_name} with a {round(self.grade)}."

            discord.send_dm(discord_id=student.discord_id, title=f"An exam has been reviewed by {self.grader.short_name}.", content=content)


from .user import User
from ..routes import discord
