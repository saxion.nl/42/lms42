from typing import Optional
from sqlalchemy import and_, func, distinct
from sqlalchemy.orm import Session
from lms42.models.attempt import Attempt
from lms42.models.user import Location, User
from ..app import schedule_job, db

MAX_QUEUE_LENGTH_PER_TEACHER = 5


def add_to_queue(student_id: int, session: Session) -> None:
    user = session.query(User).get(student_id)
    if user and user.is_student:
        student: User = user
        location: Location = student.current_location(session)
        if location == Location.DEVENTER or location == Location.ENSCHEDE:
            # only queue assignments that need to be graded with the student present
            attempts = student_attempts_that_require_teacher_feedback(student_id, session)
            
            # to coach if available on site, shortest que on site otherwise
            coach = student.coach

            if coach and coach.current_location(session) == location \
                and teacher_queue_size(coach.id, session) < MAX_QUEUE_LENGTH_PER_TEACHER:
                teacher = coach
            else:
                # it should not happen that there is no teacher on site, but it could
                teacher = teacher_with_shortest_queue_at_location(location, session)

            if teacher:
                for attempt in attempts[:2]:
                    attempt.assign_grade_teacher(teacher.id)


def student_attempts_that_require_teacher_feedback(student_id, session):
    # require_teacher_feedback is a python property derived from the curriculum pickle, cannot use in query
    attempts: list[Attempt] = session.query(Attempt) \
        .filter_by(student_id=student_id) \
        .filter_by(status="needs_grading") \
        .order_by(Attempt.submit_time) \
        .all()
    
    return [a for a in attempts if a.require_teacher_feedback]


def teacher_queue_size(teacher_id: int, session: Session) -> int:
    return session.query(func.count(distinct(Attempt.student_id)))\
            .filter_by(grade_teacher_id=teacher_id)\
            .filter_by(status="needs_grading") \
            .scalar()


def teacher_with_shortest_queue_at_location(location: Location, session: Session) -> Optional[User]:
    teachers_ordered_by_queue_length: list[User] = session.query(User)\
        .filter(User.is_teacher)\
        .outerjoin(Attempt, and_(Attempt.grade_teacher_id == User.id, Attempt.status == "needs_grading"))\
        .group_by(User.id)\
        .order_by(func.count(distinct(Attempt.student_id)))\
        .all()
    
    return next(filter(lambda t: t.current_location(session) == location, teachers_ordered_by_queue_length), None)


def remove_attempts_that_require_teacher_feedback_from_queues():
    session = db.session
    # only removes attempts from the queues that require teacher feedback,
    # because exams are assigned manually by teachers based on expertise and teacher coordination

    attempts: list[Attempt] = session.query(Attempt) \
        .filter_by(status="needs_grading") \
        .all()
    
    attempts = [a for a in attempts if a.require_teacher_feedback]

    for attempt in attempts:
        attempt.assign_grade_teacher(None)
    session.commit()
        

# job to clear inboxes at the end of the day
schedule_job(func=remove_attempts_that_require_teacher_feedback_from_queues, trigger="cron", day_of_week='mon-fri', hour=18)
