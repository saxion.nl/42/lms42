from ..app import db
import sqlalchemy as sa


class Safari(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)

    first_date = sa.Column(sa.Date, nullable=False)
    second_date = sa.Column(sa.Date, nullable=False)
    company_signup_end_date = sa.Column(sa.Date, nullable=False)
    student_signup_end_date = sa.Column(sa.Date, nullable=False)
    
    status = sa.Column(sa.Text, nullable=False, default='company_registration') # company_registration, student_registration, closed

    def get_offer(self, company_id):
        return SafariOffer.query.filter_by(safari_id=self.id, company_id=company_id).first()

    def get_registration(self, student_id):
        return SafariRegistration.query.filter_by(safari_id=self.id, student_id=student_id).first()


class SafariOffer(db.Model):
    safari_id = sa.Column(sa.Integer, sa.ForeignKey('safari.id'), nullable=False, primary_key=True)
    safari = sa.orm.relationship('Safari', backref='offers', lazy='joined')
    company_id = sa.Column(sa.Integer, sa.ForeignKey('company.id'), nullable=False, primary_key=True)
    company = sa.orm.relationship('Company', backref='safari_offers')

    spots = sa.Column(sa.Integer, nullable=False)
    use_alt_day = sa.Column(sa.Boolean, nullable=False, default=False)

    contact_person = sa.Column(sa.String, nullable=False)
    contact_person_info = sa.Column(sa.String, nullable=False)
    extra_information = sa.Column(sa.String, nullable=False)
    
    @property
    def available_spots(self):
        return max(0, self.spots - len(self.registrations))

    @property
    def registrations(self):
        return SafariRegistration.query.filter_by(safari_id=self.safari_id, company_id=self.company_id).all()


class SafariRegistration(db.Model):
    safari_id = sa.Column(sa.Integer, sa.ForeignKey('safari.id'), nullable=False, primary_key=True)
    student_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False, primary_key=True)
    student = sa.orm.relationship('User', backref='safari_registrations')

    company_id = sa.Column(sa.Integer, sa.ForeignKey('company.id'), nullable=False)
    company = sa.orm.relationship('Company')
