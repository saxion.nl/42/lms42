from ..app import db
import sqlalchemy as sa


class DashboardQuery(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)

    name = sa.Column(sa.String, nullable=False)
    sql = sa.Column(sa.String, nullable=False)
    explanation = sa.Column(sa.String, nullable=False)
    student_viewable = sa.Column(sa.Boolean, nullable=True)
