from ..app import db
import sqlalchemy as sa


class RegistrationTemplate(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)
    date = sa.Column(sa.Date, nullable=False)
    secret = sa.Column(sa.String, nullable=False)

    group_id = sa.Column(sa.Integer, sa.ForeignKey('group.id', ondelete='CASCADE'))
    group = sa.orm.relationship('Group')
