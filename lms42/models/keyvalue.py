import sqlalchemy as sa
import json
from ..app import db


class KeyValuePair(db.Model):
    key = sa.Column(sa.String, primary_key=True)
    value = sa.Column(sa.String, nullable=False)


def set(key: str, value):
    item = KeyValuePair.query.get(key) or KeyValuePair(key=key)
    item.value = json.dumps(value)
    db.session.add(item)
     

def get(key: str):
    keyvalue = KeyValuePair.query.get(key)
    return None if keyvalue is None else json.loads(keyvalue.value)
