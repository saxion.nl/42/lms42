import datetime
import math
import sqlalchemy as sa
import re
import glob
import os
import openai

from lms42.models.attempt import Attempt
from ..app import db
from ..utils import unsafe_markdown_to_html
from ..assignment import Assignment
import json


class AiQuery(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)
    time = sa.Column(sa.DateTime, default=datetime.datetime.utcnow, nullable=False)

    attempt_id = sa.Column(sa.Integer, sa.ForeignKey('attempt.id'))
    attempt = sa.orm.relationship("Attempt")

    request_user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False)
    request_user = sa.orm.relationship("User")

    question = sa.Column(sa.String, nullable=False)
    answer = sa.Column(sa.String, nullable=False)
    prompt = sa.Column(sa.String, nullable=False)

    milli_dollars = sa.Column(sa.Integer, nullable=False, default=lambda context: get_price(context.current_parameters['model'], context.current_parameters['prompt_tokens'], context.current_parameters['completion_tokens']))

    model = sa.Column(sa.String, nullable=False)
    prompt_tokens = sa.Column(sa.Integer, nullable=False)
    completion_tokens = sa.Column(sa.Integer, nullable=False)

    def to_html(self):
        return "<article><div class=question>" + unsafe_markdown_to_html(self.question) + "</div><div class=answer>" + unsafe_markdown_to_html(self.answer) + "</div></article>"


def get_price(model: str, prompt_tokens: int, completion_tokens: int): # milli-dollars per thousand tokens
    if model.startswith("gpt-3.5-turbo"):
        in_price, out_price = 0.5, 1.5
    elif model.startswith("gpt-4o"):
        in_price, out_price = 2.5, 10
    elif model.startswith('gpt-4-turbo') or model in {'gpt-4-1106-preview', 'gpt-4-0125-preview'}:
        in_price, out_price = 10, 30
    elif model.startswith("gpt-4-32k"):
        in_price, out_price = 60, 120
    elif model.startswith("gpt-4"):
        in_price, out_price = 30, 60
    elif model.startswith("o1-preview"):
        in_price, out_price = 15, 60
    elif model.startswith("o1-mini"):
        in_price, out_price = 3, 12
    elif model == 'claude-2.1':
        in_price, out_price = 8, 24
    else:
        raise ValueError(f"Unknown model price for {model}")

    return math.ceil(in_price * prompt_tokens / 1000 + out_price * completion_tokens / 1000)


class AiGrading(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)

    attempt_id = sa.Column(sa.Integer, sa.ForeignKey('attempt.id'), nullable=True)
    attempt = sa.orm.relationship("Attempt")

    input = sa.Column(sa.String, nullable=False)
    prompt = sa.Column(sa.String, nullable=False)
    output = sa.Column(sa.String, nullable=False)

    __table_args__ = (
        sa.Index('ai_grading_attempt_index', 'attempt_id'),
    )

    @property
    def output_object(self):
        try:
            return json.loads(self.output)
        except json.JSONDecodeError:
            return {'invalid_json': self.output}

    @property
    def prompt_object(self):
        try:
            return json.loads(self.prompt)
        except json.JSONDecodeError:
            return {'invalid_json': self.prompt}

    @property
    def input_object(self):
        try:
            return json.loads(self.input)
        except json.JSONDecodeError:
            return {'invalid_json': self.input}


def read_files(attempt, subdir):
    pre = f"{attempt.directory}/{subdir}/"
    for filename in glob.glob(pre+"**", recursive=True):
        if not os.path.isfile(filename):
            continue
        assert filename.startswith(pre)
        short = filename[len(pre):]
        if '/data/' in filename or '/.' in filename or '/_' in filename or filename.split('.')[-1].lower() not in {'py', 'java', 'md', 'rs', 'js', 'sql'}:
            continue

        with open(filename) as file:
            try:
                yield short, file.read()
            except UnicodeDecodeError:
                # The file is likely encrypted
                yield short, "This file cannot be read."


def make_prompt(attempt):
    ao = Assignment.load_from_directory(attempt.directory)

    FEEDBACK = """
Respond with only JSON, in the following form:

```json
{
    "Objective #1": {
        "feedback": [
            "The program would crash if the user inputs something other than digits.",
            "The program does not play user statistics yet."
        ],
        "grade": 4
    },
    "Objective #2": {
        "feedback": [
            "`board` and `marker` are not descriptive function names, as they don't describe an *action* only a *subject*.",
            "Use the imperative form for function name, so `read_file` instead of `reading_file`.",
            "The function `check_winner` does more than its name implies.",
            "Some variable names are `camelCase` while others are `snake_case`. Consistency is important.",
            "Single letter variables names are usually a bad idea, unless the letters have a conventional meaning (like x and y for coordinates) and they're only used in a short code fragment."
        ],
        "grade": 2
    }
}
```

Output one JSON object per objective. Each of those objectives should contain:
- `feedback` [list of strings]: It may be empty or contain up to 5 items of constructive feedback. Feedback should NOT mention things that were done well, but only problems and missing parts of the assi
gnment. Also, don't point out possible improvements (such as more user-friendly behavior) that are not explicitly required by the assignment. Within each feedback item, you may also provide a hint nudgi
ng in the right direction, without spoiling it. Prevent repeating similar feedback by combining into a single item. A perfectly completed objective should have an empty feedback array. Restrict feedback
 about code quality to the objective focussed on code quality, using the other objectives to point out (potential) bugs, misunderstandings and missing functionality.
- `grade` [number]: Indicates on a scale of 1 through 5 how well the student did on the objective:
  1. Nothing useful yet
  2. A good start has been made, but nothing functional yet
  3. Getting there, partly working
  4. Working, some slight rough edges but definitely passable
  5. Perfect

"""
    assignment = ao.to_ai_markdown().strip()
    assignment = re.subn(r'(?s)<script.*?</script>', '', assignment)[0]
    assignment = re.subn(r'(?s)<link.*?>', '', assignment)[0]

    submission = [{"role": "user", "content": f"{name}:\n```\n{content}\n```"} for name, content in read_files(attempt, 'submission')]

    messages = [
        {
            "role": "system",
            "content": "You are an AI tutor grading student work while providing helpful feedback."
        },
        {
            "role": "system",
            "content": f"This is the assignment the student received:\n\n```{assignment}\n```"
        },
        {
            "role": "system",
            "content": "What follows is the student's work."
        },
    ] + submission + [
        {
            "role": "system",
            "content": FEEDBACK
        }
    ]

    model = "o1-mini"
    if model.startswith("o1-"):
        # o1 unfortunately doesn't support system roles yet
        for message in messages:
            if message['role'] == 'system':
                message['role'] = 'user'

    return {
        "model": model,
        "messages": messages,
        "response_format": {"type": "json_object"},
    }


def examine_attempt(attempt_id):
    if not os.environ.get("OPENAI_API_KEY"):
        return False
    
    attempt = Attempt.query.get(attempt_id)

    prompt = make_prompt(attempt)
    if not prompt:
        return False

    try:
        client = openai.OpenAI(base_url=os.environ.get('OPENAI_PROXY'))
        completion = client.chat.completions.create(**prompt)
        choice = completion.choices[0]
        answer = choice.message.content
    except Exception as e:
        print(f"OpenAI error: {e}")
        answer = json.dumps({"error": str(e)})

    ai_grading = AiGrading(
        attempt_id=attempt.id,
        input="",
        prompt=json.dumps(prompt),
        output=answer,
    )
    db.session.add(ai_grading)
    db.session.commit()

    return ai_grading
