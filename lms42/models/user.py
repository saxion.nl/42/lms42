from typing import Optional
from ..app import app, db, schedule_job, retry_commit
from .. import working_days
import flask_login
import datetime
import enum
import os
import secrets
import base64
import sqlalchemy as sa
import flask
from sqlalchemy.ext.hybrid import hybrid_property
from backports.cached_property import cached_property
from sqlalchemy .orm import Session
from sqlalchemy.dialects.postgresql import ENUM

STUDENT_LEVEL = 10

AVATARS_DIR = os.path.join(app.config['DATA_DIR'], "avatars")
os.makedirs(AVATARS_DIR, exist_ok=True)


class Location(enum.Enum):
    DEVENTER = 'Deventer'
    ENSCHEDE = 'Enschede'
    OTHER = 'Other'
    LOCAL = 'Local'


SOME_DATE = datetime.date(2020, 1, 1)
MIN_START_TIME = datetime.time(8, 0)
MAX_END_TIME = datetime.time(18, 0)


class TimeLog(db.Model):
    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False, primary_key=True)
    user = sa.orm.relationship('User')

    date = sa.Column(sa.Date, nullable=False, primary_key=True, index=True)

    start_time = sa.Column(sa.Time, nullable=False)
    end_time = sa.Column(sa.Time, nullable=False)
    suspicious = sa.Column(sa.Boolean, nullable=False, default=False)
    location = sa.Column(ENUM(Location, name='location_enum'), nullable=True, default=Location.OTHER)

    @property
    def hours(self):
        start = datetime.datetime.combine(SOME_DATE, min(max(self.start_time, MIN_START_TIME), MAX_END_TIME))
        end = datetime.datetime.combine(SOME_DATE, min(max(self.end_time, MIN_START_TIME), MAX_END_TIME))
        result = (end - start).total_seconds() / 3600
        if self.suspicious:
            result /= 2
        return result


class User(db.Model):  # noqa: PLR0904
    id = sa.Column(sa.Integer, primary_key=True)

    first_name = sa.Column(sa.String, nullable=False)
    last_name = sa.Column(sa.String, nullable=False)
    
    email = sa.Column(sa.String, nullable=False, unique=True)
    
    @sa.orm.validates('email')
    def lower_case_email(self, key, value):
        return value.lower()

    level = sa.Column(sa.SmallInteger, nullable=False, default=STUDENT_LEVEL)
    # 10 student
    # 30 inspector
    # 50 teacher
    # 80 admin (allows impersonation)
    # 90 owner

    student_number = sa.Column(sa.Integer, unique=True)
    short_name = sa.Column(sa.String, nullable=False, unique=True)

    @sa.orm.validates('short_name')
    def lower_case_short_name(self, key, value):
        return value.lower()

    @property
    def coach_id(self):
        return self.group.study_coach_id if self.group else None

    @property
    def coach(self):
        return self.group.study_coach if self.group else None

    attempts = sa.orm.relationship("Attempt", lazy='dynamic', foreign_keys="Attempt.student_id", back_populates="student")

    current_attempt_id = sa.Column(sa.Integer, sa.ForeignKey('attempt.id', use_alter=True))
    current_attempt = sa.orm.relationship("Attempt", foreign_keys="User.current_attempt_id")

    default_schedule = sa.Column(sa.ARRAY(sa.String), nullable=False, default=['present', 'present', 'present', 'present', 'present'])

    avatar_name = sa.Column(sa.String)

    group_id = sa.Column(sa.Integer, sa.ForeignKey('group.id', use_alter=True, ondelete="SET NULL"))
    start_date = sa.Column(sa.String, nullable=False, default=datetime.date.today)

    is_hidden = sa.Column(sa.Boolean, nullable=False, default=False)
    english = sa.Column(sa.Boolean, nullable=False, default=False)

    discord_id = sa.Column(sa.String, nullable=True)

    discord_notification_same_exercise = sa.Column(sa.Boolean, nullable=False, default=True)
    discord_notification_exam_reviewed = sa.Column(sa.Boolean, nullable=False, default=True)

    check_in_teacher_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'))
    check_in_teacher = sa.orm.relationship('User', foreign_keys='User.check_in_teacher_id', backref='check_in_students', remote_side=id)
    last_check_in_time = sa.Column(sa.DateTime, nullable=False, server_default="2021-02-01 00:00:00")

    ai_credits = sa.Column(sa.Integer, nullable=False, default=0)
    intro_bux = sa.Column(sa.Integer, nullable=False, default=0)

    bke_certification = sa.Column(sa.Boolean, nullable=False, default=False)

    legacy_keys = sa.Column(sa.ARRAY(sa.String), nullable=False, server_default="{}", default=[])

    __table_args__ = (
        sa.Index('user_type_index', 'is_active', 'level', 'is_hidden'),
    )

    # The following properties and method are required by Flask Login:
    is_active = sa.Column(sa.Boolean, nullable=False, default=True)
    is_anonymous = False

    @property
    def todays_group_teacher_id(self):
        day = {0: 'monday', 1: 'tuesday', 2: 'wednesday', 3: 'thursday', 4: 'friday'}.get(datetime.datetime.now().weekday(), 'monday')
        return getattr(self.group, day+'_teacher_id', None)

    @property
    def paused_attempt(self):
        return Attempt.query.filter_by(student_id=self.id).filter_by(status='paused').first()

    @property
    def leniency_targets(self):
        lt = LeniencyTargets.query.filter_by(student_id=self.id).order_by(LeniencyTargets.time.desc()).limit(1).first()
        return lt.targets if lt else ''

    @property
    def next_flashcard(self):
        return UserFlashcard.query \
            .filter_by(user_id=self.id) \
            .filter(UserFlashcard.next_view <= datetime.datetime.today()) \
            .order_by(UserFlashcard.next_view.asc()) \
            .limit(1) \
            .first()

    @property
    def is_authenticated(self):
        return self.is_active

    def get_id(self):
        return str(self.id)

    def get_ects(self):
        passed_exams = Attempt.query \
                .filter_by(student_id=self.id, status='passed') \
                .filter(Attempt.credits > 0) \
                .distinct(Attempt.node_id) \
                .subquery()

        return db.session.query(sa.sql.func.sum(passed_exams.c.credits)).scalar() or 0

    @hybrid_property
    def is_fake(self):
        return "@" not in self.email

    @hybrid_property
    def is_student(self):
        return self.level == STUDENT_LEVEL
    
    @property
    def is_contributor(self):
        with open('AUTHORS') as file:
            contributors = file.readlines()
        contributors = [contributor.strip('- ').strip() for contributor in contributors]
        return self.full_name in contributors

    @hybrid_property
    def is_inspector(self):
        return self.level >= 30

    @hybrid_property
    def is_teacher(self):
        return self.level >= 50

    @hybrid_property
    def is_admin(self):
        return self.level >= 80
        
    @hybrid_property
    def is_owner(self):
        return self.level >= 90

    @property
    def avatar(self):
        if self.avatar_name:
            return f"/static/avatars/{self.avatar_name}"
        return "/static/placeholder.png"

    @property
    def avatar_path(self):
        return os.path.join(AVATARS_DIR, self.avatar_name)

    @avatar.setter
    def avatar(self, data_uri):
        # TODO: this is not transaction safe.
        if self.avatar_name:
            try:
                os.unlink(self.avatar_path)
            except Exception:
                print(f"Unlink {self.avatar_path} failed")
            self.avatar_name = None

        if not data_uri:
            return

        comma = data_uri.find(';base64,')
        if comma:
            header = data_uri[0:comma]
            image = base64.b64decode(data_uri[comma+8:])
            ext = ".jpg" if header.find("/jpeg") else (".png" if header.find("/png") else None)
            if ext:
                self.avatar_name = secrets.token_urlsafe(8) + ext
                with open(self.avatar_path, "wb") as file:
                    file.write(image)
                return
        raise Exception(f"Couldn't parse avatar data url starting with {data_uri[0:40]}")

    @property
    def full_name(self):
        return (self.first_name + " " + self.last_name).strip()

    @property
    def url_query(self):
        if flask_login.current_user.id != self.id:
            return f"?student={self.id}"
        return ''

    def last_log_of_today(self, session: Session) -> Optional[TimeLog]:
        return session.query(TimeLog) \
        .filter_by(date=datetime.date.today()) \
        .filter_by(user_id=self.id) \
        .order_by(sa.desc(TimeLog.end_time)) \
        .first()
    
    def is_present(self, session: Session, before_time=None):
        """True when the student has logged in from within a Saxion building today."""
        time_log = self.last_log_of_today(session)
        if time_log is None:
            return False
        return not (before_time and time_log.start_time > before_time)

    def current_location(self, session: Session) -> Location:
        time_log: TimeLog = self.last_log_of_today(session)
        if time_log is None:
            return Location.OTHER
        return Location(time_log.location)
    
    def get_presence_class(self, session: Session):
        time_log = self.last_log_of_today(session)
        if time_log is None:
            return "away"
        now = datetime.datetime.now() - datetime.timedelta(minutes=10)
        return "online" if time_log.end_time > now.time() else "today"

    @cached_property
    def performance(self):
        return Performance(self)

    def query_hours(self, start_date="1970-01-01", end_date=datetime.datetime.today(), period='YYYY-MM'):
        sql = sa.text("""
        select
            to_char(date, :period) as period,
            extract(epoch from sum(
                LEAST(end_time, CAST('18:00:00' AS TIME WITHOUT TIME ZONE))
                -
                GREATEST(LEAST(start_time, CAST('19:00:00' AS TIME WITHOUT TIME ZONE)), CAST('08:00:00' AS TIME WITHOUT TIME ZONE))
            ))+30*count(*) seconds
        from time_log
        where user_id = :user_id and date >= :start_date and date < :end_date
        group by period
        """)

        return db.session.execute(sql, {"user_id": self.id, "start_date": start_date, "end_date": end_date, "period": period}).fetchall()

    def get_attempts(self):
        if self.is_teacher: # user is teacher, so return attempt reviews
            return Attempt.query.join(Grading).filter_by(grader_id=self.id).order_by(Grading.time.desc()).limit(10).all()

        return Attempt.query.from_statement(sa.text("""
            WITH latest_attempts AS (
                SELECT * FROM attempt
                WHERE student_id = :student_id
                ORDER BY submit_time DESC
                LIMIT 10
            ),
            active_attempts AS (
                SELECT * FROM attempt
                WHERE student_id = :student_id
                AND status NOT IN ('passed', 'repair', 'failed')
            )
            SELECT * FROM (
                SELECT * FROM latest_attempts
                UNION ALL
                SELECT * FROM active_attempts
            ) x
            ORDER BY submit_time DESC
        """).params(student_id=self.id))

    # Just to make REPL work more pleasant:
    def __repr__(self):
        return f'<User {self.id}: {self.short_name}>'


class UserFlashcard(db.Model):
    user_id = sa.Column(sa.Integer, sa.ForeignKey("user.id"), primary_key=True)
    flashcard_id = sa.Column(sa.String, nullable=False, primary_key=True)
    node_id = sa.Column(sa.String, nullable=False, primary_key=True)
    repetitions = sa.Column(sa.Integer, nullable=False)
    interval = sa.Column(sa.Integer, nullable=False)
    next_view = sa.Column(sa.Date, nullable=True)
    easiness_factor = sa.Column(sa.Float, nullable=False)

    MAX_INTERVAL = 60
    
    __table_args__ = (
        sa.Index('flashcard_id_next_view', 'user_id', 'next_view'),
    )

    def update_interval(self, q):
        """SM-2 Algorithm implementation, calculates the best time for a student to view the card again (interval),
        and returns the easiness factor and repetition number that need to be constantly up to date.
        source: https://en.wikipedia.org/wiki/SuperMemo#Description_of_SM-2_algorithm
        """
        # correct answer
        if q >= 3:
            if self.repetitions == 0:
                self.interval = 1
            elif self.repetitions == 1:
                self.interval = 6
            else:
                self.interval = round(self.interval * self.easiness_factor)

            self.repetitions += 1
        # incorrect answer
        else:
            self.repetitions = 0
            self.interval = 1

        self.easiness_factor = max(self.easiness_factor + (0.1 - (5 - q) * (0.08 + (5 - q) * 0.02)), 1.3)
        # Consider the card learned when the next interval would be 60 days or more
        self.next_view = datetime.datetime.today() + datetime.timedelta(self.interval) if self.interval < self.MAX_INTERVAL else None

    def percentage(self):
        return min(100, round(100 * self.interval / self.MAX_INTERVAL))


class LoginLink(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)
    time = sa.Column(sa.DateTime, default=lambda: datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(hours=1), nullable=False)
    secret = sa.Column(sa.String, nullable=False)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'))
    user = sa.orm.relationship('User')

    company_id = sa.Column(sa.Integer, sa.ForeignKey('company.id'))
    company = sa.orm.relationship('Company')

    redirect_url = sa.Column(sa.String)

    def login(self):
        if self.user:
            if flask.session.get('company_id'):
                del flask.session['company_id']
            flask_login.login_user(self.user)
        else: # company
            flask_login.logout_user()
            flask.session['company_id'] = self.company.id
        return flask.redirect(self.redirect_url)


class LeniencyTargets(db.Model):
    student_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False, primary_key=True)
    time = sa.Column(sa.DateTime, default=datetime.datetime.now(datetime.timezone.utc), nullable=False, primary_key=True)

    teacher_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False)
    targets = sa.Column(sa.String, nullable=False)




def get_all_people(group_filter="%", presence_filter=False, show_hidden=False, show_inactive=False):
    
    query = sa.sql.text(f"""
select
    student.id,
    student.short_name,
    t.short_name as check_in_teacher_name,
    (case when student.avatar_name is null then '/static/placeholder.png' else '/static/avatars/' || student.avatar_name end) as avatar,
    g.name as group_name,
    array_remove(array_agg(tag || ':' || color || ':' || coalesce(url,'') order by prio desc, prio_time asc), null) as tags,
    (select end_time>(localtime at time zone 'Europe/Amsterdam')::time - interval '5 minutes' from time_log where time_log.user_id=student.id and date=current_date) as online
from "user" as student
left join "group" g on g.id=student.group_id
left join "user" t on t.id=student.check_in_teacher_id
left join (
    select
        student_id,
        'exam deadline' tag,
        '*important' color,
        21 prio,
        attempt.deadline_time as prio_time,
        null as url
    from attempt
    where attempt.status='in_progress' and attempt.credits>0 and now() > attempt.deadline_time

    union

    select
        student_id,
        'no recording' tag,
        '*warning' color,
        20 prio,
        attempt.deadline_time prio_time,
        null as url
    from attempt
    where attempt.status='in_progress' and attempt.credits>0 and extract(epoch from (now() - attempt.last_screenshot_time)) > 65

    union

    select
        student_id,
        attempt.node_id tag,
        (case when attempt.credits>0 then 'ok' else 'info' end) color,
        (case when attempt.credits>0 then 2 else 1 end) prio,
        attempt.deadline_time as prio_time,
        '/curriculum/' || attempt.node_id || '?student=' || student_id || '#attempt_' || number as url
    from attempt
    where attempt.status='in_progress'
                        
    union

    select
        student_id,
        (case when attempt.status='needs_consent' then '👀 '||node_id else node_id end) as tag,
        (case when credits>0 then 'warning' else 'default' end) as color,
        (case when attempt.status='needs_consent' then 2 when attempt.credits>0 then 1 else 0 end) as prio,
        attempt.submit_time as prio_time,
        '/curriculum/' || attempt.node_id || '?student=' || student_id || '#attempt_' || number as url
    from attempt
    where attempt.status='needs_grading' or attempt.status='needs_consent'

                        
) as tags on tags.student_id = student.id

where student.level>0{"" if show_hidden else " and student.is_hidden=false"}{"" if show_inactive else " and student.is_active=true"} and coalesce(g.name,'') LIKE :group_filter
group by student.id, g.name, t.short_name
{"having (select end_time>(localtime at time zone 'Europe/Amsterdam')::time - interval '5 minutes' from time_log where time_log.user_id=student.id and date=current_date)" if presence_filter else ""}
order by g.name nulls first, student.short_name
;
""")

    return db.session.execute(query, {
        "group_filter": group_filter,
    }).fetchall()


class AbsentDay(db.Model):
    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False, primary_key=True)
    user = sa.orm.relationship('User', foreign_keys="AbsentDay.user_id")

    date = sa.Column(sa.Date, nullable=False, primary_key=True, index=True)
    
    reason = sa.Column(sa.String, nullable=False)
    reason_history = sa.Column(sa.String, nullable=False, default='')
    exempt_time = sa.Column(sa.Boolean, nullable=False, default=False)
    exempt_progress = sa.Column(sa.Boolean, nullable=False, default=False)

    by_user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=True)
    by_user = sa.orm.relationship('User', foreign_keys="AbsentDay.by_user_id")

    @staticmethod
    def log(user_id, date, reason, exempt_time=None, exempt_progress=None):
        absent_day = AbsentDay.query.filter_by(user_id=user_id, date=date).first()
        if not absent_day:
            absent_day = AbsentDay(
                user_id=user_id,
                date=date,
                reason_history='',
            )
            db.session.add(absent_day)
        absent_day.reason = reason
        if absent_day.reason_history:
            absent_day.reason_history += "\n\n"
        absent_day.reason_history += datetime.datetime.now().strftime('%Y-%m-%d %H:%M')
        if flask_login.current_user and flask_login.current_user.is_authenticated:
            absent_day.reason_history += " by=" + flask_login.current_user.short_name
        if exempt_time is not None:
            absent_day.exempt_time = exempt_time
            absent_day.reason_history += f" exempt_time={exempt_time}"
        if exempt_progress is not None:
            absent_day.exempt_progress = exempt_progress
            absent_day.reason_history += f" exempt_progress={exempt_progress}"
        absent_day.reason_history += f": {reason}"


from .attempt import Attempt


def save_structural_days_as_absent_day(today=None):
    """Saves an absent_day in the database when the user has an absent day in his default schedule
    and today is a working day."""
    if today is None:
        today = datetime.date.today()
    if not working_days.is_working_day(today):
        # If today is not a working day, then there is no need to save an absent day.
        return
    day_number = today.weekday()
    for user in User.query.filter_by(is_active=True):
        reason = user.default_schedule[day_number]
        if not AbsentDay.query.filter_by(date=today, user_id=user.id).first() and reason != 'present':
            db.session.commit() # prevents postgresql serializability errors
            AbsentDay.log(user.id, today, 'other obligations' if reason == 'absent_structural' else 'work from home', exempt_time=True, exempt_progress=(reason == 'absent_structural'))
            db.session.commit()


# Every weekday (mon-fri) in the middle of the night the non-present days that are in the default schedule of the user,
# are stored as absent_day. (From: https://stackoverflow.com/a/38501429)
schedule_job(func=save_structural_days_as_absent_day, trigger="cron", day_of_week='mon-fri', hour=2, minute=15, second=0)
# schedule_job(func=save_structural_days_as_absent_day, trigger="interval", seconds=10)


from .company import Internship


@retry_commit
def check_in_missing():
    # See if the user needs a you-are-missing-check-in from the teacher
    today = datetime.date.today()
    two_days_ago = working_days.offset(today, -2)
    three_weeks_ago = working_days.offset(today, -21)
    day_number = today.weekday()
    for user in User.query.filter_by(is_active=True, is_student=True, check_in_teacher_id=None):
        reason = user.default_schedule[day_number]
        is_intern = Internship.query.filter(Internship.user_id == user.id, Internship.start_date <= today, Internship.end_date >= today).first()
        if reason == 'present' and not user.is_present(db.session) and user.last_check_in_time.date() < (three_weeks_ago if is_intern else two_days_ago):
            user.check_in_teacher_id = user.coach_id or user.todays_group_teacher_id


schedule_job(func=check_in_missing, trigger="cron", day_of_week='mon-fri', hour=10, minute=30, second=0)
# schedule_job(func=check_in_missing, trigger="interval", seconds=10)


from .performance import Performance
from .grading import Grading
