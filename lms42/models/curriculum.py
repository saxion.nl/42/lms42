import os
import pickle
from ..assignment import Assignment

cur_cache_stat = None
cur_cache_data = None


def get(key=None):
    global cur_cache_stat  # noqa: PLW0603
    global cur_cache_data  # noqa: PLW0603

    stat = os.stat('curriculum.pickle')
    if cur_cache_data is None or cur_cache_stat is None or stat.st_size != cur_cache_stat.st_size or stat.st_mtime != cur_cache_stat.st_mtime:
        cur_cache_stat = stat
        with open('curriculum.pickle', 'rb') as file:
            cur_cache_data = pickle.load(file)

    return cur_cache_data.get(key) if key is not None else cur_cache_data


def get_periods_copy():
    """Returns a semi-deep copy of `get('periods')`, copying up to
    and including the node meta info.
    """
    return {
        period: [
            (module.copy() | {
                "nodes": [node.copy() for node in module["nodes"]]
            })
            for module in modules
        ]
        for period, modules in get('periods').items()
    }


def get_periods_with_status(student) -> dict:
    if student is None:
        return get('periods')
    
    attempts_by_node = {}
    for attempt in Attempt.query.filter_by(student_id=student.id).order_by(Attempt.node_id, Attempt.number.asc()):
        if attempt.node_id not in attempts_by_node:
            attempts_by_node[attempt.node_id] = []
        attempts_by_node[attempt.node_id].append(attempt)

    periods = get_periods_copy()

    module_positions = {}
    for period, modules in periods.items():
        for module_index, module in enumerate(modules):
            module_positions[module["id"]] = (period, module_index)

    # Swap legacy modules into main periods if the student has the matching legacy keys.
    for modules in periods.values():
        for module_index, module in enumerate(modules):
            if module.get("legacy"):
                for legacy_key, legacy_module_id in module["legacy"].items():
                    if legacy_key in student.legacy_keys:
                        # Swap the two modules
                        legacy_period, legacy_module_index = module_positions[legacy_module_id]
                        modules[module_index] = periods[legacy_period][legacy_module_index]
                        periods[legacy_period][legacy_module_index] = module
                        break
                module.pop("legacy")

    for period, modules in periods.items():  # noqa: PLR1702
        for module in modules:
            if period == 'x': # Never count modules that we're not planning on doing
                module['count_as_in_progress'] = False
            module['status'] = 'wip' if module.get('wip') else 'future'
            next_node_status = 'wip' if module.get('wip') else 'startable'

            for node in module['nodes']:
                status = next_node_status
                attempts = attempts_by_node.get(node['id'], [])
                if len(attempts):
                    module['status'] = 'in_progress'

                attempt_statuses = [attempt.status for attempt in attempts]
                if 'in_progress' in attempt_statuses:
                    status = 'in_progress'
                elif 'paused' in attempt_statuses:
                    status = 'paused'
                elif 'passed' in attempt_statuses:
                    status = 'passed'
                    if node is module['nodes'][-1]:
                        # A topic is ready when its last node is passed
                        module['status'] = 'ready'
                elif 'needs_grading' in attempt_statuses or 'needs_consent' in attempt_statuses:
                    status = 'needs_grading'
                elif status == 'startable':
                    for dep_id in node['depend']:
                        dep_statuses = [attempt.status for attempt in attempts_by_node.get(dep_id, [])]
                        if 'passed' in dep_statuses:
                            pass
                        elif 'needs_grading' in dep_statuses or 'needs_consent' in dep_statuses:
                            status = 'almost_startable'
                        else:
                            status = 'future'
                            break

                if status in {'startable', 'future', 'almost_startable'} and 'provided_by' in node:
                    provided_by = node['provided_by']
                    for node_id in provided_by:
                        if any(attempt.status == 'passed' for attempt in attempts_by_node.get(node_id, [])):
                            status = 'provided'

                if status == 'needs_grading' and next_node_status == 'startable':
                    next_node_status = 'almost_startable'
                elif status not in {'passed', 'provided'}:
                    next_node_status = 'future'
                
                node['attempts'] = attempts
                node['status'] = status

    return periods


def _find_node_in_periods(periods, node_id):
    for period, modules in periods.items():
        for module_index, module in enumerate(modules):
            for node_index, node in enumerate(module["nodes"]):
                if node["id"] == node_id:
                    node['period'] = period
                    node['module_index'] = module_index
                    node["module"] = module
                    node['node_index'] = node_index
                    return node
    return None


def get_extnode(node_id: str, student):
    """Returns a node dict with some additional fields (when available) based on the student:
    - status: str
    - attempts: list[Attempt]
    - period: str (Based on the first module in this student's plan that has this node.)
    - module_index: int (Idem.)
    - module: dict (Idem.)
    - node_index: int (Idem.)
    - periods_with_status: dict[str, list] (This has little to do with node_id specifically but contains the full output of get_periods_with_status, which is convenient.)

    When the node no longer exist in the yaml files, a 'deprecated' placeholder is returned.
    """
    periods_with_status = get_periods_with_status(student)
    node = _find_node_in_periods(periods_with_status, node_id)

    if not node:
        if not student:
            return None
        attempts = Attempt.query.filter_by(student_id=student.id, node_id=node_id).order_by(Attempt.number.asc()).all()
        if not attempts:
            return None
        # This lesson no longer exists, but the student does have attempts... Fake it!
        try:
            node = Assignment.load_from_directory(attempts[-1].directory).node
            node["name"] += " (DEPRECATED)"
        except FileNotFoundError:
            node = {"name": "DEPRECATED and DELETED"}

        node["flashcards"] = {}
        node["attempts"] = attempts
        node["status"] = "DEPRECATED"
        node["module"] = {"name": "Unknown", "status": "unknown"}

    node["periods_with_status"] = periods_with_status

    return node


def find_module_for_node(node_id):
    """Finds module name for a node, HOWEVER it find the module based on the current
    curriculum, so this may be wrong for a student working on a legacy module that
    includes this assignment."""

    for module in get("modules_by_id").values():
        for node in module["nodes"]:
            if node["id"] == node_id:
                return module
    return None


from .attempt import Attempt
