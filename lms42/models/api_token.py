from ..app import db
from .user import User
import datetime
import sqlalchemy
import sqlalchemy as sa


class ApiToken(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)

    token = sa.Column(sa.String, nullable=False)

    creation_time = sa.Column(sa.DateTime, default=datetime.datetime.utcnow, nullable=False)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False)
    user = sa.orm.relationship(User, foreign_keys="ApiToken.user_id")

    host = sa.Column(sa.String, nullable=False)

    __table_args__ = (
        sqlalchemy.UniqueConstraint('user_id', 'host', name='api_token_user_host'),
    )
