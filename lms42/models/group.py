
from ..app import db
from .user import User
import sqlalchemy as sa


class Group(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String, nullable=False)
    study_coach_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'))
    monday_teacher_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'))
    tuesday_teacher_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'))
    wednesday_teacher_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'))
    thursday_teacher_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'))
    friday_teacher_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'))

    study_coach = sa.orm.relationship('User', foreign_keys=[study_coach_id])
    monday_teacher = sa.orm.relationship('User', foreign_keys=[monday_teacher_id])
    tuesday_teacher = sa.orm.relationship('User', foreign_keys=[tuesday_teacher_id])
    wednesday_teacher = sa.orm.relationship('User', foreign_keys=[wednesday_teacher_id])
    thursday_teacher = sa.orm.relationship('User', foreign_keys=[thursday_teacher_id])
    friday_teacher = sa.orm.relationship('User', foreign_keys=[friday_teacher_id])

    students = sa.orm.relationship(User, backref='group', passive_deletes=True, foreign_keys="User.group_id")
