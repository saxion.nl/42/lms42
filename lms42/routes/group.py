import datetime
from ..app import app, db, schedule_job
from ..models.user import User
from ..models.group import Group
from ..routes import discord
from flask import render_template, redirect
from flask_login import login_required, current_user
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, FieldList, FormField, SubmitField, HiddenField, BooleanField


def coerce_to_int_or_none(value):
    return None if value == "None" or value is None else int(value)


class GroupForm(FlaskForm):
    id = HiddenField()
    delete = BooleanField('Delete')
    name = StringField('Name')
    study_coach_id = SelectField('Study coach', coerce=coerce_to_int_or_none)
    monday_teacher_id = SelectField('Monday', coerce=coerce_to_int_or_none)
    tuesday_teacher_id = SelectField('Tuesday', coerce=coerce_to_int_or_none)
    wednesday_teacher_id = SelectField('Wednesday', coerce=coerce_to_int_or_none)
    thursday_teacher_id = SelectField('Thursday', coerce=coerce_to_int_or_none)
    friday_teacher_id = SelectField('Friday', coerce=coerce_to_int_or_none)
    
    def set_user_choices(self, user_choices):
        for field in self:
            if isinstance(field, SelectField):
                field.choices = user_choices


class GroupListForm(FlaskForm):
    groups = FieldList(FormField(GroupForm), min_entries=0)
    add_group = SubmitField('Add')
    save = SubmitField('Save')


@app.route('/groups', methods=['GET', 'POST'])
@login_required
def edit_groups():  # noqa: C901
    form = GroupListForm(data={"groups": Group.query.order_by(Group.name)})
    
    user_choices = [(None, '')] + [(user.id, user.short_name) for user in User.query.filter_by(is_teacher=True, is_active=True, is_hidden=False).order_by(User.short_name)]
    for item in form.groups:
        item.form.set_user_choices(user_choices)

    if not current_user.is_admin: # noqa: PLR1702
        for field in form:
            field.render_kw = {"disabled": "disabled"}
        for line in form.groups:
            for field in line.form:
                field.render_kw = {"disabled": "disabled"}
    elif form.validate_on_submit():
        if form.add_group.data:
            # Add a new empty group form
            form.groups.append_entry()
            # The new group should also have the choices set
            for item in form.groups:
                item.form.set_user_choices(user_choices)
        else:
            # Save the submitted data
            renamed_groups = {}
            for item in form.groups:
                group_form = item.form
                if group_form.delete.data: # Delete
                    if group_form.id.data:
                        group = Group.query.get(group_form.id.data)
                        if group:
                            db.session.delete(group)
                else: # Update or create
                    group_id = group_form.id.data
                    if group_id:
                        group = Group.query.get(group_id)
                        if group.name != group_form.name.data:
                            renamed_groups[group.name] = group_form.name.data
                    else:
                        group = Group()
                    group_form.populate_obj(group)
                    db.session.add(group)
            
            db.session.commit()
            if renamed_groups:
                schedule_job(discord.rename_groups, args=(renamed_groups,), next_run_time=datetime.datetime.now())

            return redirect("/people")

    return render_template('edit-groups.html', form=form)
