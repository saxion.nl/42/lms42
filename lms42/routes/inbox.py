from ..app import db, app
from ..assignment import Assignment
from ..models.attempt import Attempt, Grading
from ..utils import role_required
from ..routes import discord
from flask_login import current_user
from werkzeug.utils import secure_filename
import flask
import os
import sqlalchemy as sa
import datetime
import flask_wtf
import wtforms as wtf


class FilterForm(flask_wtf.FlaskForm):
    internship = wtf.BooleanField('Internship')
    exam = wtf.BooleanField('Exam')


@app.route('/inbox', methods=['GET'])
@role_required('teacher')
def get_inbox():
    hide_backlog = flask.request.args.get("hide_backlog", default=1, type=int)
    fragment: str = flask.request.args.get("fragment", "")

    # Only allow the "queue" fragment if this is an HTMX request.
    # Not an HTMX request: redirect to the full inbox.
    if fragment == "queue" and not flask.request.headers.get("HX-Request"):
        return flask.redirect(flask.url_for("get_inbox"))

    filter_form = FilterForm(flask.request.args)
    filter_by = []
    if filter_form.internship.data:
        filter_by.append("internship")
    if filter_form.exam.data:
        filter_by.append("exam")
    
    template = fragment if fragment == 'queue' else 'inbox'
    return flask.render_template(template + ".html",
        students=get_inbox_students(hide_backlog=bool(hide_backlog)),
        group_by="teacher_short_name",
        filter_form=filter_form,
        filter_by=filter_by,
        show_others=flask.request.args.get("others", default=1, type=int),
    )


def get_inbox_students(hide_backlog=True):
    
    query = sa.sql.text(f"""
select
    student.id,
    student.short_name,
    t.short_name as teacher_short_name,
    (case when student.avatar_name is null then '/static/placeholder.png' else '/static/avatars/' || student.avatar_name end) as avatar,
    array_remove(array_agg(tag || ':' || color || ':' || coalesce(url,'') order by prio desc, prio_time asc), null) as tags,
    (time_log.end_time > (localtime at time zone 'Europe/Amsterdam')::time - interval '5 minutes') as online
from (
    select
        student_id,
        'approval' tag,
        'important' as color,
        10 as prio,
        attempt.start_time as prio_time,
        null as teacher_id,
        '/curriculum/' || attempt.node_id || '?student=' || student_id || '#attempt_' || number as url
    from attempt
    where attempt.status='awaiting_approval'

    union

    select
        student_id,
        (case when attempt.status='needs_consent' then '👀 '||node_id else node_id end) as tag,
        (case when credits>0 then 'warning' else 'default' end) as color,
        (case when attempt.status='needs_consent' then 2 when attempt.credits>0 then 1 else 0 end) as prio,
        attempt.submit_time as prio_time,
        attempt.grade_teacher_id as teacher_id,
        '/curriculum/' || attempt.node_id || '?student=' || student_id || '#attempt_' || number as url
    from attempt
    where (attempt.status='needs_grading' {"and (not grade_teacher_id is null or attempt.credits>0)" if hide_backlog else ""}) or attempt.status='needs_consent'

    union

    select
        student_id,
        'exam deadline' tag,
        '*important' color,
        21 prio,
        attempt.deadline_time as prio_time,
        null as teacher_id,
        null as url
    from attempt
    where attempt.status='in_progress' and attempt.credits>0 and now() > attempt.deadline_time

    union

    select
        student_id,
        'no recording' tag,
        '*warning' color,
        20 prio,
        attempt.deadline_time prio_time,
        null as teacher_id,
        null as url
    from attempt
    where attempt.status='in_progress' and attempt.credits>0 and extract(epoch from (now() - attempt.last_screenshot_time)) > 65
 
) as tags
join "user" as student on tags.student_id = student.id
left join "user" as t on tags.teacher_id = t.id
left join time_log on time_log.user_id=student.id and date=current_date
where student.is_active=true
group by t.id, student.id, time_log.user_id, time_log.date
order by t.short_name nulls first, max(prio) desc, min(prio_time) asc
;
""")

    cutoff = datetime.datetime.now().replace(hour=10, minute=30)
    return db.session.execute(query, {
        "cutoff_time": cutoff,
    }).fetchall()


@app.route('/inbox/grade/<attempt_id>', methods=['POST'])
@role_required('teacher')
def grading_post(attempt_id):
    form = flask.request.form
    attempt = Attempt.query.get(attempt_id)

    # when attempt is graded, grade_teacher should be none
    attempt.grade_teacher_id = None
    
    ao = Assignment.load_from_directory(attempt.directory)

    # If the teacher has clicked not graded this sets the attempt to ungraded and returns to the student panel
    if 'not-graded' in form and "ects" in ao.node:
        flask.flash("You can not set the status of exams to 'not-graded'. How did you manage to do this?")
        return flask.redirect("/inbox")

    if 'not-graded' in form and "ects" not in ao.node:
        attempt.set_ungraded()
        return flask.redirect(f"/people/{attempt.student_id}#attempts")

    if attempt.status in {"passed", "repair", "failed", "needs_consent", "not_graded", "fraudulent"}:
        flask.flash("Attempt was already graded! Your grading has replaced the old grading, but the old grading is still available in the database.")
    elif attempt.status not in {"needs_grading", "not_graded"}:
        flask.flash(f"Attempt is still in {attempt.status}!?")
        return flask.redirect("/inbox")

    for file in flask.request.files.getlist('grading_upload'):
        os.makedirs(os.path.join(attempt.directory, 'grading'), exist_ok=True)
        file.save(os.path.join(attempt.directory, 'grading', secure_filename(file.filename)))

    scores = ao.form_to_scores(form)
    motivations = ao.form_to_motivations(form)

    grade, passed = ao.calculate_grade(scores)

    needs_consent = attempt.is_consent_needed(grade, form)
    
    if 'mark_fraudulent' in form:
        passed = False
    
    grading = Grading(
        attempt_id=attempt.id,
        grader_id=current_user.id,
        objective_scores=scores,
        objective_motivations=motivations,
        grade=grade,
        grade_motivation=form.get('motivation'),
        passed=passed,
        needs_consent=needs_consent,
    )

    if needs_consent:
        attempt.status = "needs_consent"
    elif "ects" in ao.node and 'mark_fraudulent' in form:
        attempt.status = "fraudulent"
    elif "ects" in ao.node:
        attempt.status = "passed" if passed else "failed"
    elif form.get('formative_action') in {"failed", "passed", "repair"}:
        attempt.status = form.get('formative_action')
    else:
        attempt.status = "passed" if passed else "repair"

    db.session.add(grading)
    db.session.commit()

    attempt.write_json()

    student = attempt.student
    if attempt.status == "passed":
        flask.flash(f"{student.full_name} passed with a {round(grade)}!")
    elif attempt.status == "needs_consent":
        flask.flash("Grading needs consent from another teacher.")
    elif attempt.status == "fraudulent":
        flask.flash("Submit a report to the examination board regarding this potential exam fraud")
    # elif grade is None:
    #     flask.flash(f"{student.full_name} has not received a grade.")
    else:
        flask.flash(f"{student.full_name} did NOT pass with a {round(grade)}.")
    if "ects" in ao.node and attempt.status != "needs_consent" and attempt.status != "fraudulent":
        grading.announce()
    elif attempt.node.get("discord_role_on_pass") and attempt.status == "passed":
        discord.manage_discord_user(student)
    
    return flask.redirect(f"/people/{attempt.student_id}#attempts")


@app.route('/inbox/<int:attempt_id>/grade_teacher', methods=['PUT'])
@role_required('teacher')
def set_grade_teacher(attempt_id):
    attempt = db.session.query(Attempt).get(attempt_id)
    teacher_id = flask.request.form.get("grade_teacher")
    attempt.assign_grade_teacher(teacher_id or None)
    db.session.commit()
    return "OK"
