import json
from flask_login import current_user, login_required
from ..app import db, app, get_base_url, schedule_job
from ..models.user import User
import os
import flask
import requests
from urllib.parse import urlencode

DISCORD_FILE_PATH = "/tmp/lms42-discord.txt"


def get_redirection_url():
    return f"{get_base_url()}/discord/authorize"


def get_auth_url():
    base_url = "https://discord.com/api/oauth2/authorize?"
    parameters_dict = {
        "redirect_uri": get_redirection_url(),
        "response_type": "code",
        "scope": "identify guilds.join",
        "client_id": os.environ.get("DISCORD_CLIENT_ID")
    }
    parameters = urlencode(parameters_dict)
    return base_url + parameters


@app.route('/discord/remove', methods=['POST'])
@login_required
def discord_remove():
    # Strip all system-assigned roles, but leave on server, so that when
    # a user temporarily unlinks and links their account, they don't loose
    # custom roles.
    manage_discord_user(current_user, unlink=True)

    # Kick user from discord server
    # discord_server = get_discord_server()
    # if discord_server:
    #     bot_command(f'guilds/{discord_server.get("id")}/members/{user.discord_id}', 'DELETE')

    current_user.discord_id = None
    db.session.commit()

    flask.flash("Discord account unlinked.")
    return flask.redirect(f"/people/{current_user.short_name}")


@app.route('/discord/authorize')
@login_required
def discord_authorize():
    if "code" not in flask.request.args:
        flask.flash("Error in redirect URL.")
        return flask.redirect(f"/people/{current_user.short_name}")

    code = flask.request.args['code']
    token_url = "https://discord.com/api/oauth2/token"
    
    body = {
        "client_id": os.environ.get("DISCORD_CLIENT_ID"),
        "client_secret": os.environ.get("DISCORD_CLIENT_SECRET"),
        "grant_type": "authorization_code",
        "redirect_uri": get_redirection_url(),
        "code": code,
    }
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    
    bearer_response = requests.post(token_url, data=body, headers=headers)
    response = json.loads(bearer_response.text)
    
    if "access_token" not in response:
        flask.flash("Received error when retrieving token.")
        return flask.redirect(f"/people/{current_user.short_name}")

    bearer_token = response["access_token"]

    info_url = "https://discord.com/api/oauth2/@me"
    headers = {"Authorization": f"Bearer {bearer_token}"}
    user_info = requests.get(info_url, headers=headers)
    discord_user_id = json.loads(user_info.text)["user"]["id"]
    
    user = User.query.get(current_user.id)
    user.discord_id = discord_user_id
    db.session.commit()
    
    if error := manage_discord_user(user, bearer_token):
        flask.flash(f"Could not join the SD42 Discord server: {error}.")
    else:
        flask.flash("Linked Discord account!")

    return flask.redirect(f"/people/{current_user.short_name}")


@app.route('/discord/notifications/same_exercise', methods=['POST'])
@login_required
def discord_toggle_same_exercise_notification():
    user = User.query.get(current_user.id)
    user.discord_notification_same_exercise = bool(flask.request.form.get("notification_same_exercise", False)) # Checkbox value is either "on" or None
    db.session.commit()
    
    return flask.redirect(f"/people/{current_user.short_name}")


@app.route('/discord/notifications/exam_reviewed', methods=['POST'])
@login_required
def discord_toggle_exam_reviewed_notification():
    user = User.query.get(current_user.id)
    user.discord_notification_exam_reviewed = bool(flask.request.form.get("notification_exam_reviewed", False))
    db.session.commit()
    
    return flask.redirect(f"/people/{current_user.short_name}")


def get_discord_server():
    # Get all discord servers the bot is in
    response = bot_command('users/@me/guilds', 'GET')
    
    if response.status_code != 200:
        print('get_discord_server: Error getting servers', flush=True)
        return None
    
    servers = response.json()
    
    if len(servers) == 0:
        print('get_discord_server: No server found', flush=True)
        return None

    return servers[0]


def create_discord_role(role_name, discord_server_id):
    response = bot_command(f'guilds/{discord_server_id}/roles', 'POST', {'name': role_name})

    if response.status_code != 200:
        print(f'create_discord_role: failed to create {role_name}', flush=True)
        return None

    return response.json().get('id')


def get_desired_user_role_ids(user, discord_server_id, current_user_role_ids=None, unlink=False):
    if not current_user_role_ids:
        current_user_role_ids = []
        
    # Get all discord server roles
    response = bot_command(f'guilds/{discord_server_id}/roles', 'GET')
    
    if response.status_code != 200:
        print(f'get_desired_user_role_ids: could not get roles from {discord_server_id}', flush=True)
        return []
    
    discord_roles = response.json()
    
    # Make new system roles based on user info from database
    new_user_role_names = set()
    
    if unlink:
        pass
    elif user.is_active:
        if user.group:
            new_user_role_names.add(f'group={user.group.name}')
            new_user_role_names.add(f'location={user.group.name[:1]}')

        attempts = user.attempts.filter_by(status="passed")
        for attempt in attempts:
            if attempt.node:
                role = attempt.node.get("discord_role_on_pass")
                if role:
                    new_user_role_names.add(role)
            
        if user.coach:
            new_user_role_names.add(f'coach={user.coach.short_name}')
        
        if user.is_teacher:
            new_user_role_names.add(f'coach={user.short_name}')
            new_user_role_names.add('level=teacher')
        elif user.is_student:
            new_user_role_names.add('level=student')
    else:
        new_user_role_names.add('level=inactive')
    
    # Search for discord role ids of existing roles to assign the user to
    role_ids_to_assign = []
    for discord_role in discord_roles:
        role_id = discord_role.get('id')
        role_name = discord_role.get('name')
        
        # Check if the role is a system role
        if '=' not in role_name and role_id in current_user_role_ids:
            # Keep non-system role
            role_ids_to_assign.append(role_id)
            continue
        
        if role_name not in new_user_role_names:
            continue
        
        role_ids_to_assign.append(role_id)
        new_user_role_names.remove(role_name)
        
    # Create new roles if they do not exist in the discord server
    created_role_ids = []
    for role_name in new_user_role_names:
        role_id = create_discord_role(role_name, discord_server_id)
        
        if role_id:
            created_role_ids.append(role_id)
    
    return role_ids_to_assign + created_role_ids


def manage_discord_user(user, access_token=None, unlink=False):
    if isinstance(user, int):
        user = User.query.get(user)
    if not user.discord_id:
        return "User has no known Discord id"
    
    discord_server = get_discord_server()
    if not discord_server:
        return "No Discord server configured"
    
    discord_server_id = discord_server["id"]

    # Get all user roles from a discord server
    response = bot_command(f'guilds/{discord_server_id}/members/{user.discord_id}', 'GET')
    current_user_role_ids = response.json().get('roles')

    json_data: dict = {
        'nick': f'{user.first_name} {user.last_name}',
        'roles': get_desired_user_role_ids(user, discord_server_id, current_user_role_ids, unlink=unlink),
    }

    if response.status_code != 200 or current_user_role_ids is None:
        # User does not exists within our server yet
        if not access_token:
            # We're not just linking this user, so this is an error
            print(f'manage_discord_user: could not retrieve current roles or {user.discord_id}: {response.text}', flush=True)
            return f"Could not retrieve current roles: {response.text}"

        # Add user to discord server
        # Append roles to user
        json_data['access_token'] = access_token
        
        # Add the user to the discord server
        response = bot_command(f'guilds/{discord_server_id}/members/{user.discord_id}', 'PUT', json_data)
        
        if response.status_code == 201:
            return None
        print(f"manage_discord_user: could not add user {user.id} = {user.discord_id} to {discord_server_id}: {response.text}", flush=True)
        return f"Could not add user: {response.text}"

    # We'll update the existing Discord user in our server

    # Update the user (profile) in the discord server
    response = bot_command(f'guilds/{discord_server_id}/members/{user.discord_id}', 'PATCH', json_data)

    if response.status_code == 204:
        return None
    print(f"manage_discord_user: could not modify user {user.id} = {user.discord_id}: {response.text}", flush=True)
    return f"Could not modify user: {response.text}"


def send_dm(discord_id, title, content):
    if not os.environ.get("DISCORD_BOT_TOKEN"):
        with open(DISCORD_FILE_PATH, 'w') as file:
            file.write(content)
            return

    dm_channel = bot_command(endpoint="users/@me/channels", method="POST", json_data={"recipient_id": discord_id}).json()
    json_data = {
        "tts": False,
        "embeds": [{
            "title": title,
            "description": content
        }]
    }
    if 'id' in dm_channel:# Make sure the channel id is available
        bot_command(endpoint=f"/channels/{dm_channel['id']}/messages", method="POST", json_data=json_data)


def bot_command(endpoint, method="GET", json_data=None) -> requests.Response:
    request_url = f'https://discord.com/api/{endpoint}'
    headers = {
        "Authorization": f'Bot {os.environ.get("DISCORD_BOT_TOKEN")}'
    }

    if method == "GET":
        response = requests.get(request_url, headers=headers)
    elif method == "POST":
        response = requests.post(request_url, headers=headers, json=json_data)
    elif method == "PATCH":
        response = requests.patch(request_url, headers=headers, json=json_data)
    elif method == "PUT":
        response = requests.put(request_url, headers=headers, json=json_data)
    elif method == "DELETE":
        response = requests.delete(request_url, headers=headers, json=json_data)
    else:
        raise ValueError(f"Unknown method: {method}")

    return response


def rename_groups(old_to_new: dict):
    discord_server = get_discord_server()
    
    if not discord_server:
        print('rename_groups: no server found', flush=True)
        return
    
    discord_server_id = discord_server['id']
    
    # Get all discord server roles
    response = bot_command(f'guilds/{discord_server_id}/roles', 'GET')
    
    if response.status_code != 200:
        print(f'rename_groups: could not get roles from {discord_server_id}', flush=True)
        return
    
    discord_roles = response.json()
    
    for discord_role in discord_roles:
        role_id = discord_role.get('id')
        role_name = discord_role.get('name')
        
        # Check if the role is a system role
        system_role = role_name.split('=')
        if len(system_role) != 2:
            continue
        
        # Check if the role is a group role
        group_key, group_name = system_role
        if group_key != 'group':
            continue
        
        # Check if the role needs to be renamed
        if group_name not in old_to_new:
            continue
        
        new_name = old_to_new[group_name]
        new_role_name = f'group={new_name}'
    
        # Update role by renaming the role name
        response = bot_command(f'guilds/{discord_server_id}/roles/{role_id}', 'PATCH', json_data={'name': new_role_name})
        
        if response.status_code != 200:
            print(f'rename_groups: could not rename {role_name} with {new_name}', flush=True)

def delete_empty_roles():
    discord_server = get_discord_server()
    
    if not discord_server:
        print('delete_empty_roles: no server found', flush=True)
        return
    
    discord_server_id = discord_server['id']
    
    # Get all discord server roles
    response = bot_command(f'guilds/{discord_server_id}/roles', 'GET')
    
    if response.status_code != 200:
        print(f'delete_empty_roles: could not get roles from {discord_server_id}', flush=True)
        return
    
    discord_roles = response.json()
    
    # Get all discord users
    # NOTE default limit is set to 1 member
    response = bot_command(f'guilds/{discord_server_id}/members?limit=1000', 'GET')
    
    if response.status_code != 200:
        print(f'delete_empty_roles: could not get users in {discord_server_id}', flush=True)
        return
    
    discord_users = response.json()
        
    for discord_role in discord_roles:
        role_id = discord_role.get('id')
        role_name = discord_role.get('name')
        
        # Check if role is a system role
        if '=' not in role_name:
            continue
        
        # Go through all users if it has the discord role assigned to them
        is_empty_role = True
        for user in discord_users:
            user_roles = user.get('roles')
            
            # Check if user has role
            if role_id in user_roles:
                is_empty_role = False
                break
            
        # If no one has the role, delete it
        if is_empty_role:
            response = bot_command(f'guilds/{discord_server_id}/roles/{role_id}', 'DELETE')
            
            if response.status_code != 204:
                print(f'delete_empty_roles: could not remove role {role_name}', flush=True)
            

# Every weekday (mon-fri) in the middle of the night delete any empty discord system roles.
schedule_job(delete_empty_roles, trigger='cron', day_of_week='mon-fri', hour=0, minute=0, second=0)
