import flask
import os
from flask_login import current_user, login_required
from datetime import date
from mollie.api.client import Client

from ..app import app, db, get_base_url, csrf, retry_commit
from ..models.user import User
from ..models.event import BuxPayment
from ..forms.event import AddBuxForm

mollie_client = Client()
if mollie_api_key := os.environ.get("MOLLIE_API_KEY", ""):
    mollie_client.set_api_key(mollie_api_key)
TUX_BUX_TO_EURO = 2.5


@app.route('/payment', methods=['GET', 'POST'])
@login_required
def create_transaction():
    form = AddBuxForm()

    if form.validate_on_submit():
        transaction = BuxPayment(
            student_id=current_user.id,
            bux=form.bux.data,
            transaction_date=date.today(),
        )
        db.session.add(transaction)
        db.session.commit()

        try:
            payment = mollie_client.payments.create({
                'amount': {
                    'currency': 'EUR',
                    'value': f'{(form.bux.data * TUX_BUX_TO_EURO):.2f}'
                },
                'description': f'Saxion {form.bux.data} Bux (#{transaction.id})',
                'redirectUrl': get_base_url() + f"/events/payments/{transaction.id}",
                'webhookUrl':  get_base_url() + "/payment/webhook",
                'metadata': {
                    'transaction_id': transaction.id
                }
            })

            transaction.provider_reference = f"mollie:{payment['id']}"
            db.session.commit()
            checkout_url = payment['_links']['checkout']['href']
            return flask.redirect(checkout_url)
        except Exception as e:
            print(f"Error creating Mollie payment: {e}")
            flask.flash("An error occurred while processing your payment. Please try again or talk to a teacher!")

    return flask.render_template(
        'generic-form.html',
        intro="""
<img src="/static/bux.png" class="bux-coin" style="max-width: 8em; height: auto; float: right; margin: 0 0 0.25em 0.5em;">

- Bux can be used to register for intro events.
- Please don't buy more than you need, as they can only be spent for activities during this year's intro.
- You can pay using any Dutch bank through iDeal. (If you don't have a Dutch bank account (yet) and want additional credits, please talk to a teacher.)
- Your money will be transferred to study association Tuks. They will make sure the event organizers will get reimbursed.
- In case you experience any problems regarding payments, please talk to teacher, preferably Frank.
        """,
        form=form,
        title='<a href="/events">Intro Events</a><div>Add Bux</div>'
    )


@app.route('/payment/webhook', methods=['GET', 'POST'])
@csrf.exempt
@retry_commit
def payment_webhook():
    if flask.request.method == 'GET':
        return '', 200

    print("/payment/webhook", flask.request)
    payment_id = flask.request.form['id']
    payment = mollie_client.payments.get(payment_id)

    transaction_id = payment['metadata']['transaction_id']
    transaction = BuxPayment.query.get(transaction_id)

    user = User.query.get(transaction.student_id)

    if payment.is_paid():
        if transaction.status != 'paid':
            transaction.status = 'paid'
            user.intro_bux += transaction.bux
    else:
        if transaction.status == 'paid':
            # Transaction somehow got reverted
            user.intro_bux -= transaction.bux
        transaction.status = 'cancelled' if payment.is_canceled() else "unknown"

    return '', 200
