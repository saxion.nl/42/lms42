import time
import os
from ..app import app, retry_commit
from ..models.user import User
from flask_login import login_required, current_user

if os.environ.get('FLASK_ENV','').startswith('dev'):

    @app.route('/test/ok', methods=['GET'])
    def ok():
        User.query.get(1)
        return "Ok"


    @app.route('/test/stall_transaction', methods=['GET'])
    def stall_transaction():
        User.query.get(1)
        time.sleep(3)
        User.query.get(2)
        return "Ok"

    @app.route('/test/increment', methods=['GET'])
    @retry_commit
    @login_required
    def test():
        level = current_user.level
        time.sleep(3)
        current_user.level = level + 1
        return str(current_user.level)
