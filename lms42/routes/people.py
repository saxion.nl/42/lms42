from ..app import db, app, schedule_job
from ..models.user import User, get_all_people, STUDENT_LEVEL
from ..models.group import Group
from ..models import curriculum
from ..utils import role_required
from .. import working_days
from . import discord
import os
import flask
import datetime
import wtforms as wtf
import wtforms.validators as wtv
import flask_wtf
import sqlalchemy as sa
from flask_login import login_required, current_user


class FilterForm(flask_wtf.FlaskForm):
    present = wtf.BooleanField('Present')
    hidden = wtf.BooleanField('Hidden')
    inactive = wtf.BooleanField('Inactive')
    group = wtf.SelectField('Group')


@app.route('/people', methods=['GET', 'POST'])
@login_required
def list_people():
    show_hidden = show_inactive = False
    group_filter = '%'
    filter_form = FilterForm(flask.request.args)
    group_names = [group.name for group in Group.query]
    filter_form.group.choices = ['All groups'] + group_names + list({n[0]+'%' for n in group_names})
    group_filter = flask.request.args.get('group')
    presence_filter = flask.request.args.get('present')

    if not group_filter or group_filter == 'All groups':
        group_filter = '%'
        
    if current_user.is_teacher:
        show_hidden = flask.request.args.get('hidden')
        show_inactive = flask.request.args.get('inactive')
    
    return flask.render_template('people-list.html',
        students=get_all_people(show_hidden=show_hidden, show_inactive=show_inactive, group_filter=group_filter, presence_filter=presence_filter),
        filter_form=filter_form,
        group_by="group_name" if (group_filter == '%') else None,
    )


def get_progress_graph(user_id, start_date):
    days_per_month = working_days.calculate_per_month(datetime.datetime.strptime(start_date, "%Y-%m-%d").date())

    month_positions = {month: position for position, month in enumerate(days_per_month)}
    labels = [month for month in days_per_month]

    sql = f"""select month, short_name as name, sum(avg_days) as progress
    from (
        select u.short_name, max(a.avg_days) avg_days, a.node_id, to_char(min(a.submit_time), 'YYYY-MM') as month
        from attempt a
        join "user" u on a.student_id=u.id
        where a.status='passed' and a.student_id = {int(user_id)}
        group by u.id, a.node_id
    ) as passed
    group by short_name, month
    order by short_name, month
    """

    series = [
        {"name": "progress", "data": [0 for _ in days_per_month]},
        {"name": "hours", "data": [0 for _ in days_per_month]}
    ]
    for row in db.session.execute(sql):
        month = row['month']
        if month in month_positions:
            series[0]["data"][month_positions[month]] = round(100 * row["progress"] / days_per_month[month])

    if "2021-06" in days_per_month:
        days_per_month["2021-06"] -= 7 # days before time_log started

    for row in User.query.get(user_id).query_hours():
        month = row['period']
        if month in month_positions:
            series[1]["data"][month_positions[month]] = round(100 * row["seconds"] / days_per_month[month] / 8 / 60 / 60)

    return {"labels": labels, "series": series}


PROGRESS_LABELS = ["Passed", "Awaiting grading", "Failed"]


def get_progress_chart_data(user_id):
    # This query makes use of the Row number and partition by clauses, what this does is:
    # The partition by goes over all of the distinct node_id's and gives those a number, 1 for passed, 2 for awaiting grading
    # and 3 for failed, the row number function than assign's a unique row number for every row with the same node id starting at 1 ordered ASC by the rownumber.
    # The where clause only selects the attempts where the rn assigned by the row number function is selected,
    # This means that the most important one and only one attempt is selected for each unique node id.
    # The last part of the query is just doing a sum of the attempts.
    # The coalesce selects the first not null value, since when there is no value to sum, the sum will be null.
    query_result = db.session.execute("""
    SELECT
        COALESCE(SUM(CASE
            WHEN filtered_attempts.status = 'passed' THEN filtered_attempts.credits
        END), 0) AS passed_exam_credits,
        COALESCE(SUM(CASE
            WHEN filtered_attempts.status = 'needs_grading' OR filtered_attempts.status = 'needs_consent' THEN filtered_attempts.credits
        END), 0) AS awaiting_exam_credits,
        COALESCE(SUM(CASE
            WHEN filtered_attempts.status = 'failed' THEN filtered_attempts.credits
        END), 0) AS failed_exam_credits
    FROM (
        SELECT *
        FROM (
            SELECT *,
                ROW_NUMBER() OVER (PARTITION BY attempt.node_id ORDER BY
                    CASE
                        WHEN attempt.status = 'passed' THEN 1
                        WHEN attempt.status = 'needs_grading' OR attempt.status = 'needs_consent' THEN 2
                        WHEN attempt.status = 'failed' THEN 3
                        ELSE 4
                    END ASC) AS rn
            FROM attempt
            WHERE attempt.student_id = :user_id AND attempt.credits > 0
        ) AS ranked_attempts
        WHERE ranked_attempts.rn = 1
    ) AS filtered_attempts
    """,
    {"user_id": user_id}).fetchone()

    # series[0] & is exams that have been passed
    # series[1] & is exams that are awaiting grading or needing consent
    # series[2] & is exams that have been failed
    # series[3] & is exams that have not been made yet
    series = [
        query_result["passed_exam_credits"],
        query_result["awaiting_exam_credits"],
        query_result["failed_exam_credits"]
    ]

    # To get the total number of not started exam ects. Subtract all of the above from 120
    series.append(120 - sum(series))

    # The labels for the custom legend
    labels = [f"{label}: {series[idx]}" for idx, label in enumerate(PROGRESS_LABELS)]

    # Return the series and the labels for the progress chart
    return {"series": series, "legend_labels": labels}


class EditForm(flask_wtf.FlaskForm):

    def validate(self, extra_validators) -> bool: # type: ignore
        result = True
        super_result = super().validate(extra_validators)

        existing_users = User.query.filter(
            User.id != self.id.data,
            sa.or_(
                User.email == self.email.data,
                User.short_name == self.short_name.data,
                sa.and_(
                    User.student_number == self.student_number.data,
                    User.student_number.isnot(None)
                )
            )
        ).all()

        for existing_user in existing_users:
            result = False
            if existing_user.email == self.email.data:
                self.email.errors.append('Email address already in use.')
            if existing_user.short_name == self.short_name.data:
                self.short_name.errors.append('Shortname already in use.')
            if existing_user.student_number == self.student_number.data:
                self.student_number.errors.append('Student number already in use.')

        return super_result and result

    id = wtf.HiddenField("id")

    first_name = wtf.StringField('First name', validators=[
        wtv.DataRequired()
    ])

    last_name = wtf.StringField('Last name', validators=[
    ])

    email = wtf.StringField('Email', validators=[
        wtv.Email(), wtv.DataRequired()
    ])
    student_number = wtf.IntegerField('Student number', validators=[wtv.Optional(strip_whitespace=True)])
    english = wtf.BooleanField('English language', default=False)
    
    bke_certification = wtf.BooleanField('BKE Certification', default=False)

    short_name = wtf.StringField('Short name', validators=[
        wtv.DataRequired(), wtv.Regexp('^[a-z0-9\\-]{2,}$', 0, 'Only numbers, lower case English letters and dashes are allowed.')
    ])

    level = wtf.SelectField('Level', choices=[(10, 'Student'), (30, 'Inspector'), (50, 'Teacher'), (80, 'Admin'), (90, 'Owner')], default=10, coerce=int)
    group_id = wtf.SelectField('Group', coerce=(lambda value: None if value == "None" or value is None else int(value)))
    study_coach = wtf.StringField('Study coach', render_kw={'readonly': True})
    legacy_keys = wtf.SelectMultipleField('Legacy plans')

    is_active = wtf.BooleanField('Active', default=True)
    is_hidden = wtf.BooleanField('Hidden', default=False)

    choices = [("present","Present"), ("home_structural","Home"), ("absent_structural","Absent")]

    monday = wtf.RadioField("Mon", choices=choices, render_kw={"class": "default-schedule"}, default='present')
    tuesday = wtf.RadioField("Tue", choices=choices, render_kw={"class": "default-schedule"}, default='present')
    wednesday = wtf.RadioField("Wed", choices=choices, render_kw={"class": "default-schedule"}, default='present')
    thursday = wtf.RadioField("Thu", choices=choices, render_kw={"class": "default-schedule"}, default='present')
    friday = wtf.RadioField("Fri", choices=choices, render_kw={"class": "default-schedule"}, default='present')

    submit = wtf.SubmitField('Save')


def get_group_choices() -> list[tuple]:
    return [(None, '')] + [(g.id, g.name) for g in Group.query.order_by(Group.name)]


@app.route('/people/<int:user_id>', methods=['GET', 'POST'])
@login_required
def user_profile_id(user_id):
    person = User.query.get(user_id)
    if person:
        return flask.redirect(flask.url_for("user_profile", short_name=person.short_name))
    return flask.render_template('error.html', message='No such person'), 404


@app.route('/people/me', methods=['GET', 'POST'])
@login_required
def user_profile_me():
    return flask.redirect(flask.url_for("user_profile", short_name=current_user.short_name))


@app.route('/people/<string:short_name>', methods=['GET', 'POST'])
@login_required
def user_profile(short_name):
    person = User.query.filter_by(short_name=short_name).first()
    if not person:
        return flask.render_template('error.html', message='No such person'), 404
    
    privileged = current_user.is_teacher or current_user.id == person.id

    edit_form = None
    if current_user.is_admin and current_user.level >= person.level:
        edit_form = EditForm(obj=person,
            monday=person.default_schedule[0],
            tuesday=person.default_schedule[1],
            wednesday=person.default_schedule[2],
            thursday=person.default_schedule[3],
            friday=person.default_schedule[4],
            study_coach=person.coach.first_name if person.coach else ""
        )
        edit_form.group_id.choices = get_group_choices()
        edit_form.legacy_keys.choices = curriculum.get('legacy_keys')
        if person.level < 50:
            del edit_form.bke_certification
        if edit_form.validate_on_submit():
            try:
                if edit_form.level.data > current_user.level:
                    # Users cannot give anyone a level that is higher than their own
                    edit_form.level.data = max(person.level, current_user.level)

                edit_form.populate_obj(person)

                if not person.is_active and person.current_attempt_id:
                    person.current_attempt.submit_time = datetime.datetime.utcnow()
                    person.current_attempt.status = "needs_grading"
                    person.current_attempt.grade_teacher_id = current_user.id
                    person.current_attempt_id = None

                schedule = [edit_form.monday.data, edit_form.tuesday.data, edit_form.wednesday.data, edit_form.thursday.data, edit_form.friday.data]
                person.default_schedule = schedule
                schedule_job(discord.manage_discord_user, args=(person.id,), next_run_time=datetime.datetime.now())
                db.session.commit()
                return flask.redirect('/people')
            except sa.exc.IntegrityError:
                db.session.rollback()
                edit_form.email.errors.append('Email address already in use.')
        
    performance = person.performance if privileged else None
    # TODO: These two should probably also be moved to Performance:
    graph = get_progress_graph(person.id, person.start_date) if privileged and person.is_student else None
    progress_chart = get_progress_chart_data(person.id) if privileged and person.is_student else None
    attempts = person.get_attempts() if privileged else None
    check_in_teachers = User.query.filter_by(is_teacher=True, is_active=True, is_hidden=False).order_by(User.short_name).all() if current_user.is_teacher else None

    discord_url = ''
    discord_status = ''
    if os.environ.get("DISCORD_CLIENT_ID") and current_user.id == person.id:
        if person.discord_id:
            discord_status = 'authorized'
        else:
            discord_status = 'unauthorized'
            discord_url = discord.get_auth_url()

    return flask.render_template('people-detail.html',
        person=person,
        edit_form=edit_form,
        update_avatar=privileged,
        graph=graph,
        progress_chart=progress_chart,
        attempts=attempts,
        performance=performance,
        discord_status=discord_status,
        discord_url=discord_url,
        check_in_teachers=check_in_teachers,
    )


@app.route('/people/new', methods=['GET','POST'])
@login_required
def new():
    if not current_user.is_admin:
        return 'Only admins can do that.', 403

    edit_form = EditForm()
    edit_form.group_id.choices = get_group_choices()
    if edit_form.validate_on_submit():
        person = User()
        edit_form.populate_obj(person)
        try:
            db.session.add(person)
            db.session.commit()
            return flask.redirect(f"/people/{person.id}")
        except sa.exc.IntegrityError:
            db.session.rollback()
            flask.abort(500)

    return flask.render_template('people-add.html', person={}, edit_form=edit_form)


@app.route('/people/<int:user_id>/avatar', methods=['POST'])
@login_required
def upload_avatar(user_id):
    if current_user.short_name in ["robin", "indy"]:
        flask.flash("Nope!")
    elif current_user.is_teacher or current_user.id == user_id:
        user = User.query.get(user_id)
        user.avatar = flask.request.form['avatar']
        db.session.commit()
    else:
        flask.flash("Permission denied.")
    return flask.redirect(f"/people/{user_id}")


@app.route('/people/<int:user_id>/check_in_teacher', methods=['PUT'])
@role_required('teacher')
def set_check_in_teacher(user_id):
    user = User.query.get(user_id)
    teacher_id = flask.request.form.get('check_in_teacher')
    if teacher_id:
        user.check_in_teacher_id = teacher_id
    else:
        user.check_in_teacher_id = None
        user.last_check_in_time = datetime.datetime.now()
    db.session.commit()
    return "OK"
