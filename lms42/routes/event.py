import os
import random
import string

import flask
from flask_login import current_user, login_required
from werkzeug.utils import secure_filename
from werkzeug.datastructures import FileStorage
from sqlalchemy import func
from datetime import datetime, timedelta

from ..app import app, db, retry_commit
from ..models.event import Event, EventRegistration, BuxPayment
from ..forms.event import AddEventForm
from ..utils import role_required

IMAGE_DIR = app.config['DATA_DIR'] + '/event-images'
os.makedirs(IMAGE_DIR, exist_ok=True)


def save_image(event, form):
    if isinstance(form.image.data, FileStorage):
        image_file = form.image.data
        original_filename = secure_filename(image_file.filename)
        file_ext = os.path.splitext(original_filename)[1]
        rand_str = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
        filename = f'{rand_str}{file_ext}'
        image_file.save(os.path.join(IMAGE_DIR, filename))
        return filename
    return event.image


def get_pending():
    pending = db.session.query(func.sum(Event.cost))\
        .join(EventRegistration, Event.id == EventRegistration.event_id)\
        .filter(
            EventRegistration.student_id == current_user.id,
            Event.registration_deadline >= datetime.now()
        ).scalar()

    return pending or 0


@app.route('/events', methods=['GET'])
@login_required
def show_events():
    events = Event.query.filter(Event.event_time >= datetime.now() - timedelta(hours=12))
    if current_user.group_id: # For users with a group (so not teachers), filter by city
        events = events.filter_by(city_code=current_user.group.name[0])
    events = events.order_by(Event.event_time).all()
    
    # payment_transactions = BuxPayment.query.filter_by(student_id=current_user.id).all()
    return flask.render_template('events.html', events=events, pending=get_pending())


@app.route('/events/<int:event_id>', methods=['GET'])
def show_event(event_id):
    event = Event.query.get(event_id)
    if not event:
        flask.abort(404)
    registration = EventRegistration.query.filter_by(event_id=event.id, student_id=current_user.id).first()
        
    return flask.render_template('event.html', event=event, registration=registration, now=datetime.utcnow())


@app.route('/event/add', methods=['GET', 'POST'])
@role_required('teacher')
def add_event():
    event_form = AddEventForm()

    if not event_form.validate_on_submit():
        return flask.render_template(
            'generic-form.html',
            form=event_form,
            title="Add an event"
        )
    
    event = Event()
    event_form.populate_obj(event)
    event.image = save_image(event, event_form)

    db.session.add(event)
    db.session.commit()
    
    return flask.redirect(flask.url_for('show_events'))


@app.route('/events/<int:event_id>/edit', methods=['GET', 'POST'])
@role_required('teacher')
def edit_event(event_id):
    event = Event.query.get(event_id)

    event_form = AddEventForm(obj=event)

    if not event_form.validate_on_submit():
        return flask.render_template(
            'generic-form.html',
            form=event_form,
            title="Edit event"
        )
        
    event_form.populate_obj(event)
    event.image = save_image(event, event_form)

    db.session.commit()

    return flask.redirect(
        flask.url_for(
            'show_event',
            event_id=event_id)
    )


@app.route('/events/<int:event_id>', methods=['POST'])
@login_required
@retry_commit
def post_user_register_event(event_id):
    event = Event.query.filter_by(id=event_id).first()

    registration = EventRegistration.query.filter_by(event_id=event.id, student_id=current_user.id).first()
    
    if "event_register" in flask.request.form:
        if registration:
            flask.flash("Already registered.")
        elif datetime.utcnow() > event.registration_deadline:
            flask.flash("Sorry, passed the registration deadline. Talk to the organizers to see if something can be arranged.")
        elif not event.available_spots:
            flask.flash("No more spots available.")
        elif current_user.intro_bux < event.cost:
            flask.flash(f"Not enough Bux. You need {event.cost} but you have { current_user.intro_bux }. <a href='/payment'>Buy some!</a>")
        else:
            registration = EventRegistration(event_id=event.id, student_id=current_user.id, bux=event.cost)
            current_user.intro_bux -= event.cost
            db.session.add(registration)
    
    # elif "event_deregister" in flask.request.form:
    #     if not registration or registration.event_id != event.id:
    #         flask.flash("Not registered.")
    #     else:
    #         current_user.intro_bux += registration.bux
    #         db.session.delete(registration)
    else:
        flask.abort(403)
    
    return flask.redirect(flask.request.url)


@app.route('/events/<int:event_id>/delete', methods=['POST'])
@role_required('admin')
def delete_event(event_id):
    event = Event.query.get(event_id)
    for registration in event.registrations:
        registration.student.intro_bux += event.cost
        db.session.delete(registration)

    if not event:
        flask.abort(404)
    
    db.session.delete(event)
    db.session.commit()
    
    return flask.redirect("/events")


@app.route('/events/payments/<transaction_id>')
@login_required
def get_payment_transaction(transaction_id):
    transaction = BuxPayment.query.get(transaction_id)
    if transaction.student_id == current_user.id:
        return flask.render_template('event-payment.html', transaction=transaction)
    return flask.redirect('get_events')
