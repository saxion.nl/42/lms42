import requests
from html5lib import HTMLParser
from ..app import app
import flask
import os
import re
import urllib


PREVIEWS_DIR = f"{os.getcwd()}/{app.config['DATA_DIR']}/link_previews"
os.makedirs(PREVIEWS_DIR, exist_ok=True)


@app.route('/link_preview/<path:quoted_url>', methods=['GET'])
def get_link_preview(quoted_url):
    url = urllib.parse.unquote(quoted_url)
    quoted_url = urllib.parse.quote(url, safe='') # make sure its quoted, to use in a path
    preview_file = f"{PREVIEWS_DIR}/{quoted_url}"

    if not os.path.isfile(preview_file):  # noqa: PLR1702
        success = False
        youtube_match = re.match(r'^https?://(www.)?youtube\.com/watch\?v=(?P<video_id>[^&]+)', url) or re.match(r'^https?://youtu\.be/(?P<video_id>[^?]+)', url)
        if youtube_match:
            print(f"link_preview {url} try youtube", flush=True)
            try:
                img_url = f"https://img.youtube.com/vi/{youtube_match.group('video_id')}/mqdefault.jpg"
                urllib.request.urlretrieve(img_url, preview_file)
                success = True
            except Exception as e:
                print("link_preview youtube thumbnail failed", e, flush=True)

        if not success:
            print(f"link_preview {url} try og:image", flush=True)
            try:
                response = requests.get(url)
                parser = HTMLParser(strict=False)
                document = parser.parse(response.text)

                for element in document.iter():
                    if element.tag == 'meta' and element.attrib.get('property') == 'og:image':
                        og_image_url = element.attrib.get('content')
                        img_data = requests.get(og_image_url).content
                        with open(preview_file, 'wb') as handler:
                            handler.write(img_data)
                        if os.path.getsize(preview_file) > 3000:
                            success = True
                            break
                        print("link_preview link_preview blank image?", flush=True)
                        os.unlink(preview_file)
            except Exception as e:
                print("link_preview og:image failed", e, flush=True)

        if not success:
            print(f"link_preview {url} try thum.io", flush=True)
            try:
                img_url = f"https://image.thum.io/get/noanimate/width/240/{url}"
                urllib.request.urlretrieve(img_url, preview_file)
                if os.path.getsize(preview_file) > 3000:
                    success = True
                else:
                    print("link_preview thum.io blank image?", flush=True)
                    os.unlink(preview_file)
            except Exception as e:
                print("link_preview thum.io failed", e, flush=True)

        if not success:
            print(f"link_preview {url} try thumbnail.ws", flush=True)
            try:
                img_url = f"https://api.thumbnail.ws/api/aba89ad67def69da2e0dead46cc67bc0677e63e1b432/thumbnail/get?url={quoted_url}&width=240"
                urllib.request.urlretrieve(img_url, preview_file)
            except Exception as e:
                print("link_preview thumbnail.ws failed", e, flush=True)
                preview_file = os.getcwd()+"/lms42/static/404.jpg"
    
    return flask.send_file(preview_file, cache_timeout=3*86400)
