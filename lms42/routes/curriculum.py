from lms42.models.ai import AiGrading, examine_attempt
from lms42.models import inbox
from .. import utils, working_days
from ..app import db, app, retry_commit, schedule_job
from ..assignment import Assignment, get_all_variants_info
from ..models import curriculum
from ..models.attempt import Attempt, get_notifications, FORMATIVE_ACTIONS
from ..models.user import User
from ..models.feedback import NodeFeedback
from ..routes.time_log import CHECK_IN_DEADLINE
from ..models.grading import Grading
from ..models.group import Group
from flask_login import login_required, current_user
from flask_wtf.csrf import generate_csrf
from pathlib import Path
import wtforms as wtf
import wtforms.validators as wtv
import flask_wtf
import datetime
import flask
import json
import os
import random
import shutil
import subprocess
import sqlalchemy
from . import discord


DEFAULT_GIT_IGNORE = """.venv
node_modules
build
target
postgresqlite
__pycache__
"""

GITLAB_URL = "https://gitlab.com/saxionnl/42/lms42/-/issues?sort=created_date&state=all&label_name[]=feedback"

LMS_DIR = os.getcwd()

DEADLINE_MULTIPLIER = 1.5



# Work-around from: https://github.com/wtforms/wtforms/issues/477#issuecomment-716417410
# Should no longer be required after the next release of WTForms (after 2.3.3).
class FieldsRequiredForm(flask_wtf.FlaskForm):
    """Require all fields to have content. This works around the bug that WTForms radio
    fields don't honor the `DataRequired` or `InputRequired` validators.
    """

    class Meta:
        def render_field(self, field, render_kw):
            if field.type == "_Option" and field.name == "finished":
                render_kw.setdefault("required", True)
            return super().render_field(field, render_kw)



class SubmitForm(FieldsRequiredForm):
    finished = wtf.RadioField("Do you consider your submission finished?", choices=[
        ('yes', "Yes, I think it should be good enough"),
        ('forfeit', "No, but I'm giving up (for now)"),
    ], validators=[wtv.InputRequired()], description="If you answered no, feel free to skip the questions below.")

    resource_quality = wtf.RadioField('What did you think about the provided learning resources?', choices=[
        (1, 'Terrible'),
        (2, 'Not so good'),
        (3, 'Just okay'),
        (4, 'Pretty good'),
        (5, 'Excellent'),
    ])


    assignment_clarity = wtf.RadioField("Was it clear what the assignment wanted you to do?", choices=[
        (1, 'Not at all'),
        (2, 'No'),
        (3, 'A bit vague'),
        (4, 'Mostly clear'),
        (5, 'Perfectly clear'),
    ])

    difficulty = wtf.RadioField("How difficult did you find the assignment?", choices=[
        (1, 'Way too easy'),
        (2, 'Too easy'),
        (3, 'Just right'),
        (4, 'Too hard'),
        (5, 'Way too hard'),
    ])

    hours = wtf.IntegerField("How many (effective) hours did you spent on the assignment and learning resources?", validators=[wtv.NumberRange(0, 200)])

    fun = wtf.RadioField("Did you enjoy working on the assignment?", choices=[
        (1, 'Hated every moment'),
        (2, 'No'),
        (3, 'It was okay'),
        (4, 'For the most part'),
        (5, 'It was fun!'),
    ])

    comments = wtf.TextAreaField(app.jinja_env.filters['safe'](
        f"Any hints to help us improve? Anything you type here will be posted as a public (but anonymous to anyone but the teachers) <a href='{flask.escape(GITLAB_URL)}' target='_blank'>GitLab issue</a>."
    ), render_kw={"placeholder": "Your comment will be saved here until you submit the assignment\nDo:\n - Provide feedback on resources or the subject matter of the assignment\n - Leave this field empty if you have no feedback (yes that is allowed ;)\nDon't:\n - Provide short and/or meaningless feedback (for example \"I submitted it to be able to open another assignment\")"})

    submit = wtf.SubmitField('Submit attempt')


class EmptySubmitForm(FieldsRequiredForm):
    submit = wtf.SubmitField('Submit attempt')


@app.route('/curriculum', methods=['GET'])
def curriculum_get():
    student = get_student()
    periods = curriculum.get_periods_with_status(student)

    # Find the appropriate period tab to start with
    initial_period = None
    for period, modules in periods.items():
        if initial_period is None:
            initial_period = period
        if any(module.get("status") in {"in_progress", "future"} and module.get("count_as_in_progress", True) for module in modules):
            initial_period = period
            break

    return flask.render_template("curriculum.html",
        student=student,
        periods=periods,
        initial_period=initial_period,
        errors=curriculum.get('errors'),
        warnings=curriculum.get('warnings'),
        stats=curriculum.get("stats") if current_user.is_authenticated and current_user.is_inspector else None,
    )


def get_deadline(extnode):
    deadline = None
    if 'ects' in extnode and not extnode.get('allow_longer'):
        # Some time on the day of the deadline, in local time:
        deadline = datetime.datetime.now() + datetime.timedelta(days=(extnode['days']-1))
        # 17:00 on the day of the deadline, in local time:
        deadline = deadline.replace(**working_days.DEADLINE_TIME)
        # Convert to UTC
        deadline = utils.local_to_utc(deadline)
    return deadline


@app.route('/attempts/<attempt_id>/handle_approval', methods=['POST'])
@utils.role_required('teacher')
def approve_attempt(attempt_id):
    attempt = Attempt.query.get(attempt_id)
    if attempt.status == "awaiting_approval":
        if "approve" in flask.request.form:
            extnode = curriculum.get_extnode(attempt.node_id, attempt.student)

            attempt.status = "in_progress"
            attempt.start_time = datetime.datetime.utcnow()
            attempt.deadline_time = get_deadline(extnode)

            db.session.commit()
        else:
            # Somehow, this doesn't work in the unit test (non-deterministic FK error), probably
            # due to pytest-flask-sqlalchemy rollback magic.
            if attempt.student.current_attempt_id == attempt.id:
                attempt.student.current_attempt_id = None
                resume_paused_attempt(attempt.student)
                db.session.commit()
            delete_attempt(attempt)
    else:
        flask.flash("Approval was not needed.")
    return flask.redirect("/inbox")


@app.route('/attempts/<attempt_id>/consent', methods=['POST'])
@utils.role_required('teacher')
def consent_grading(attempt_id):
    attempt = Attempt.query.get(attempt_id)
    grading = attempt.latest_grading

    if attempt.status != "needs_consent":
        flask.flash("No consent is required.")

    elif grading.grader_id == current_user.id:
        flask.flash("You cannot give consent to a grade you determined yourself.")
    
    else:
        grading.needs_consent = False
        grading.consent_user_id = current_user.id
        grading.consent_time = datetime.datetime.now(tz=datetime.timezone.utc)

        attempt.status = "passed" if grading.passed else "failed"
        db.session.commit()

        if attempt.credits > 0:
            grading.announce()


    return flask.redirect("/inbox")


@app.route('/mark_objective/', methods=["PUT"])
@login_required
@retry_commit
def mark_objective():
    if not current_user.is_student and not current_user.current_attempt:
        return {"error": "Cannot modify objective mark"}

    attempt = current_user.current_attempt
    if not attempt:
        return {"error": "No attempt"}

    data = json.loads(flask.request.data)

    objective_id = data['id']
    checked = data['checked']

    if checked:
        attempt.done_objectives.append(objective_id)
    else:
        attempt.done_objectives.remove(objective_id)

    db.session.commit()

    return {"success": True}

@app.route('/curriculum/<node_id>', methods=['POST'])
@login_required
@retry_commit
def node_action(node_id):
    if "start" in flask.request.form:
        return start_node(node_id)
    if "switch-paused" in flask.request.form:
        return switch_paused_node(node_id)
    if "force-submit" in flask.request.form: # Needs to come before "submit" check
        return submit_node(node_id, force=True)
    if "submit" in flask.request.form:
        return submit_node(node_id)
    if "retract" in flask.request.form:
        return retract_approval_request(node_id)
    if "login" in flask.request.form:
        return flask.redirect("/user/login")
    if "teacher-start-submit" in flask.request.form:
        return teacher_start_submit(node_id)
    raise Exception(f"Invalid action {' '.join(flask.request.form.keys())}")


def retract_approval_request(node_id):
    attempt = current_user.current_attempt
    if attempt and attempt.node_id == node_id and attempt.status == 'awaiting_approval':
        current_user.current_attempt_id = None
        resume_paused_attempt(current_user)
        db.session.commit()
        delete_attempt(attempt)
    else:
        flask.flash("Nothing to retract.")
    return flask.redirect('#')


def teacher_start_submit(node_id):
    if not current_user.is_teacher:
        flask.abort(403)
    
    student = get_student()
    assert student
    extnode = curriculum.get_extnode(node_id, student)
    assert extnode and not extnode.get("upload", True)

    create_attempt(student, extnode, "needs_grading")
    return flask.redirect(flask.request.url+"#new")


def delete_attempt(attempt):
    assert attempt.status in {"awaiting_approval", "awaiting_recording"}
    directory = attempt.directory
    db.session.delete(attempt)
    db.session.commit()
    shutil.rmtree(directory)


def switch_paused_node(node_id):
    extnode = curriculum.get_extnode(node_id, current_user)
    assert extnode
    errors, _approvals, _, action = check_node_startable(extnode, current_user)
    if action != 'switch-paused':
        flask.flash(" ".join(errors))
        return flask.redirect(flask.request.url)

    assert pause_current_attempt(current_user)
    assert resume_paused_attempt(current_user)
    db.session.commit()
    return flask.redirect(flask.request.url)


def start_node(node_id):
    extnode = curriculum.get_extnode(node_id, current_user)
    assert extnode
    errors, approvals, _, action = check_node_startable(extnode, current_user)
    if action != 'start':
        flask.flash(" ".join(errors))
        return flask.redirect(flask.request.url)
    
    # Check for previous attempt that need to be graded
    prev_attempt = Attempt.query.filter_by(student_id=current_user.id, node_id=node_id, status="needs_grading").first()
    if prev_attempt and 'ects' not in extnode:
        prev_attempt.set_ungraded()

    if not extnode.get('allow_ai') and extnode.get('ects'):
        attempt_status = "awaiting_recording"
    elif approvals:
        attempt_status = "awaiting_approval"
    else:
        attempt_status = "in_progress"

    attempt = create_attempt(current_user, extnode, attempt_status)
    if not attempt:
        return flask.redirect(flask.request.url)

    # Get all the people doing the same assignment as the user
    student = current_user
    others = User.query \
        .filter_by(is_active=True, is_hidden=False) \
        .join(Group, User.group_id == Group.id) \
        .filter(Group.name.like(student.group.name[0]+'%' if student.group else '%')) \
        .join(Attempt, User.id == Attempt.student_id) \
        .filter(Attempt.node_id == attempt.node_id) \
        .filter(Attempt.status == 'in_progress') \
        .filter(User.id != attempt.student_id) \
        .all()

    # Send an dm to the user if its not an exam attempt, has other people also doing the same assignment,
    # Has an discord id linked.
    # And has the message notification setting enabled.
    if len(others) >= 1 and attempt.credits == 0:
        for other_student in others:
            if other_student.discord_id and other_student.discord_notification_same_exercise:
                content = f"{student.short_name} just started working on the same assignment as you."
                discord.send_dm(discord_id=other_student.discord_id, title="Assignment info", content=content)

    return flask.redirect(flask.request.url+"#new")


def create_attempt(student: User, extnode, attempt_status):
    # Select a semi-random variant giving preference to variants that the student has
    # attempted the least often.
    variant_occurrences = {}
    variant_id = 1
    while extnode.get(f'assignment{variant_id}'):
        # By default, take the first assignment first, unless it's an exam, than take a random assignment
        variant_occurrences[variant_id] = random.uniform(0, 1) + extnode.get(f"variant_penalty_{variant_id}", 0) if "ects" in extnode else variant_id/10
        variant_id += 1

    if not variant_occurrences:
        flask.flash("Sorry, there is no assignment available. Talk to a teacher!")
        return None

    last_attempt = None
    attempts = Attempt.query.filter_by(student_id=student.id, node_id=extnode["id"]).order_by(Attempt.number)
    for attempt in attempts:
        last_attempt = attempt
        if attempt.status not in {"repair", "not_graded"} and attempt.variant_id in variant_occurrences:
            variant_occurrences[attempt.variant_id] += 2

    variant_id = sorted(variant_occurrences.items(), key=lambda x: x[1])[0][0]
    assignment = extnode.get(f'assignment{variant_id}')

    alternatives = Assignment(assignment, extnode).get_alternatives()
    last_alternatives = last_attempt and last_attempt.status in {"repair", "not_graded"} and last_attempt.credits == 0 and last_attempt.alternatives
    for name, options in alternatives.items():
        if last_alternatives and name in last_alternatives and last_alternatives[name] in options:
            # Try to preserve alternatives from the previous attempt to be repaired
            alternatives[name] = last_alternatives[name]
        else:
            # Select a random alternative
            alternatives[name] = random.choice(options)

    done_objectives = []

    if last_attempt and last_attempt.latest_grading and last_attempt.status == "repair":
        def filter_objectives(index: int):
            score = last_attempt.latest_grading.objective_scores[index]
            motivation = last_attempt.latest_grading.objective_motivations[index]
            # remove those that the teacher graded with less than 3 points or with 3 points and a comment.
            return not ((score < 3) or (score == 3 and motivation))
        
        done_objectives = list(filter(filter_objectives, last_attempt.done_objectives))

    last_number = db.session.query(Attempt.number) \
        .filter_by(student_id=student.id, node_id=extnode["id"]) \
        .order_by(Attempt.number.desc()).limit(1).scalar() or 0

    attempt = Attempt(
        student_id=student.id,
        number=last_number + 1,
        node_id=extnode["id"],
        variant_id=variant_id,
        deadline_time=get_deadline(extnode), # The deadline date gets set when a teacher approves the exam
        status=attempt_status,
        credits=extnode.get("ects", 0),
        avg_days=extnode["avg_attempts"] * extnode["days"],
        alternatives=alternatives,
        done_objectives=done_objectives
    )

    if attempt_status == "needs_grading":
        attempt.finished = "yes"
        attempt.submit_time = datetime.datetime.utcnow()

    error = None
    for _ in range(100): # High number, for tests
        try:
            os.makedirs(attempt.directory)
            break
        except FileExistsError as _error:
            error = _error
            attempt.number += 1
    else:
        print(f"Could not create attempt directory {attempt.directory} ({error} / {os.getcwd()})", flush=True)
        flask.flash("Error! Could not create attempt directory.")
        return None

    db.session.add(attempt)

    if attempt_status != "needs_grading":
        pause_current_attempt(student)

        db.session.commit() # causes attempt.id to be available
        student.current_attempt_id = attempt.id
    db.session.commit()

    # Write the attempt as json
    attempt.write_json()

    # Write the assignment as json
    with open(attempt.directory+"/assignment.json", "w") as file:
        file.write(json.dumps(assignment, indent=4))

    # Strip out useless data from the node, and write it to json
    node_copy = extnode.copy()
    variant_id2 = 1
    while node_copy.get(f'assignment{variant_id2}'):
        node_copy.pop(f'assignment{variant_id2}')
        variant_id2 += 1
    for name in ['directory', 'attempts', 'status', 'periods_with_status']:
        node_copy.pop(name, None)
    node_copy['module'] = extnode['module'].copy()
    node_copy['module'].pop('nodes')
    node_copy['module'].pop('description', None)

    with open(attempt.directory+"/node.json", "w") as file:
        file.write(json.dumps(node_copy, indent=4))

    # Copy the solution if it exists
    sol_dir = f"{extnode['directory']}/solution{variant_id}"
    if Path(sol_dir).is_dir():
        shutil.copytree(sol_dir, attempt.directory+"/solution")

    # Copy the template if it exists
    tmpl_src = f"{extnode['directory']}/template{variant_id}"
    tmpl_dst = attempt.directory+"/template"
    if Path(tmpl_src).is_dir():
        shutil.copytree(tmpl_src, tmpl_dst)
    else:
        os.mkdir(tmpl_dst)

    # Create a default .gitignore file if it doesn't exist
    gitignore = f"{tmpl_dst}/.gitignore"
    if not Path(gitignore).is_file():
        with open(gitignore, "w") as file:
            file.write(DEFAULT_GIT_IGNORE)

    # Do a Git init in the template directory
    subprocess.run(["git", "init", "-q"], cwd=tmpl_dst)
    subprocess.run(["git", "add", "."], cwd=tmpl_dst)
    subprocess.run(["git", "commit", "-q", "--author", "sd42 <info@sd42.nl>", "-m", "Teacher-provided template"],
        cwd=tmpl_dst,
        env={'GIT_COMMITTER_NAME': 'sd42', 'GIT_COMMITTER_EMAIL': 'info@sd42.nl'}
    )

    return attempt


def submit_node(node_id, force=False):
    attempt = Attempt.query.filter_by(student_id=current_user.id, node_id=node_id, status="in_progress").first()
    if attempt:
        submit_form = SubmitForm()
        node = curriculum.get('nodes_by_id').get(attempt.node_id)

        # Store feedback
        feedback = NodeFeedback()
        submit_form.populate_obj(feedback)
        feedback.node_id = node_id
        feedback.student_id = current_user.id
        db.session.merge(feedback)

        # If the assignment was never uploaded or >10 minutes ago, give a warning
        if node and node.get('upload', True) and (attempt.upload_time is None or attempt.upload_time + datetime.timedelta(minutes=10) < datetime.datetime.utcnow()) and not force:
            if attempt.upload_time is None:
                flask.flash("No upload yet. Continue submission?")
            else:
                flask.flash("Your last upload was pretty long ago. Continue submission?")
            return flask.render_template('confirm_submit.html', form=submit_form, node_id=node_id)

        # Update the attempt state
        if node and node.get('autopass') and submit_form['finished'].data == "yes":
            attempt.status = "passed"
            grading = Grading(
                attempt_id=attempt.id,
                grader_id=attempt.student_id,
                objective_scores=[],
                objective_motivations=[],
                grade=10,
                grade_motivation='Autopass',
                passed=True,
                needs_consent=False,
            )
            db.session.add(grading)
        else:
            attempt.status = "needs_grading"
            if datetime.datetime.now().time() < CHECK_IN_DEADLINE:
                inbox.add_to_queue(current_user.id, db.session)
        attempt.finished = submit_form['finished'].data
        attempt.total_hours = attempt.hours # must be done before setting submit_time (see hours @property)
        attempt.submit_time = datetime.datetime.now(datetime.timezone.utc)
                
        # Set record status to finished
        if attempt.record_status in {"in_progress", "error"}:
            attempt.record_status = "finished"

        if current_user.current_attempt_id == attempt.id:
            current_user.current_attempt_id = None

        # Resume attempt
        resume_paused_attempt(current_user)

        # Flush
        db.session.commit()
        attempt.write_json()

        schedule_job(examine_attempt, args=(attempt.id,), next_run_time=datetime.datetime.now(), id=f"examine_attempt_{attempt.id}")

        flask.flash("Assignment submitted!")
    else:
        flask.flash("Assignment already submitted?")
    return flask.redirect('/curriculum')


def resume_paused_attempt(user):
    attempt = user.paused_attempt
    if not attempt or user.current_attempt_id:
        return False

    user.current_attempt_id = attempt.id
    attempt.status = "in_progress"
    attempt.submit_time = None
    attempt.start_time = datetime.datetime.utcnow()
    return True


def pause_current_attempt(user):
    pausing_attempt = user.current_attempt
    if not pausing_attempt:
        return False
    pausing_attempt.status = "paused"
    pausing_attempt.total_hours = pausing_attempt.hours # must be done before setting submit_time (see hours @property)
    pausing_attempt.submit_time = datetime.datetime.utcnow()
    user.current_attempt_id = None
    flask.flash("Attempt paused!")
    return True


def user_has_access(user, node):
    is_public = node.get('public_for_students') if user.is_authenticated else node.get('public_for_externals')
    return is_public or (user.is_authenticated and (user.is_inspector or not node.get('ects')))


@app.route('/curriculum/<node_id>/<int:variant_id>/template.zip', methods=['GET'])
def node_get_template(node_id, variant_id):
    node = curriculum.get('nodes_by_id')[node_id]
    if user_has_access(current_user, node):
        zip_proc = subprocess.Popen(['zip', '-r', '-', f'template{variant_id}'], stdout=subprocess.PIPE, cwd=node["directory"])
        return flask.Response(zip_proc.stdout, content_type='application/zip')
    return 'Lesson is not public.', 403


@app.route('/curriculum/<node_id>', methods=['GET'])
def node_get(node_id):
    status_code = 200
    if flask.request.args.get("poll_approved") and current_user.current_attempt and current_user.current_attempt.node_id == node_id:
        if current_user.current_attempt.status == "awaiting_approval":
            return "Waiting..", 204 # HTMX interprets this status code as no data
        status_code = 286 # HTMX interprets this status code as 'stop polling'

    student = get_student()
    status = None
    extnode = curriculum.get_extnode(node_id, student)
    if not extnode:
        return flask.render_template('error.html', message='No such lesson'), 404

    pair_options = []
    assignments = {}

    if student:
        # Show all attempts for a specific student in tabs
        passed = False
        for attempt in reversed(extnode["attempts"]):
            regrade = int(flask.request.args.get("regrade", "-1")) == attempt.id
            assignments[f"Attempt {attempt.number}"] = get_attempt_info(extnode, attempt, student, regrade)
            if attempt.status == 'passed':
                passed = True

        if (passed and not extnode.get('ects')) or extnode.get("public_for_students"):
            variants = get_all_variants_info(extnode, current_user.is_authenticated and current_user.is_inspector)
            if len(variants) == 1 and assignments:
                assignments["Latest"] = list(variants.values())[0]
            else:
                assignments.update(variants)

        if extnode["status"] != "in_progress":
            errors, approvals, warnings, action = check_node_startable(extnode, student)
            action_text = None
            if current_user.id != student.id:
                action = None
                if not extnode.get("upload", True) and current_user.is_teacher and extnode["status"] != "needs_grading":
                    # Allow user to start+submit the assignment
                    action = "teacher-start-submit"
                    action_text = "Create gradable attempt"
            elif action in {'start', 'switch-paused'}:
                if approvals:
                    action_text = 'Request teacher approval'
                elif warnings:
                    action_text = 'Start attempt anyway'
                elif action == 'switch-paused':
                    action_text = 'Switch to paused attempt'
                else:
                    action_text = 'Start attempt'
            elif action == 'retract':
                action_text = 'Retract approval request'

            status = {'action': action, 'action_text': action_text, 'errors': errors, 'approvals': approvals, 'warnings': warnings}

        if extnode.get('pair') and not passed and extnode["node_index"] > 0:
            # Look for students who have not finished this node, but *have* finished the node before (the node before).
            TargetAttempt = sqlalchemy.orm.aliased(Attempt)  # noqa: N806
            # Select other active students from the same city
            pair_options = User.query.filter_by(is_student=True, is_active=True, is_hidden=False) \
                .filter(User.id != student.id) \
                .join(Group, User.group_id == Group.id) \
                .filter(Group.name.like(student.group.name[0]+'%' if student.group else '%'))

            PrevAttempt = sqlalchemy.orm.aliased(Attempt)  # noqa: N806
            periods_with_status = extnode["periods_with_status"]
            if extnode["node_index"] > 1:
                # Look for students who have finished the assignment before the previous assignment
                prev_prev_node_id = periods_with_status[extnode["period"]][extnode["module_index"]]["nodes"][extnode["node_index"]-2]["id"]
                pair_options = pair_options.join(PrevAttempt, sqlalchemy.and_(PrevAttempt.student_id == User.id, PrevAttempt.status == 'passed', PrevAttempt.node_id == prev_prev_node_id))
            else:
                # Look for students who have an attempt (in any state) for the previous assignment
                prev_node_id = periods_with_status[extnode["period"]][extnode["module_index"]]["nodes"][extnode["node_index"]-1]["id"]
                pair_options = pair_options.join(PrevAttempt, sqlalchemy.and_(PrevAttempt.student_id == User.id, PrevAttempt.node_id == prev_node_id))

            # Only select students that have not yet passed the target node
            pair_options = pair_options \
                .outerjoin(TargetAttempt, sqlalchemy.and_(TargetAttempt.student_id == User.id, TargetAttempt.status != 'failed', TargetAttempt.node_id == extnode['id'])) \
                .filter(TargetAttempt.student_id == None)  # noqa: E711 (sqlalchemy can't work with 'is')

    elif not student and ((current_user.is_authenticated and current_user.is_inspector) or extnode.get('public_for_externals')):
        # View assignment, not attached to a specific student
        assignments = get_all_variants_info(extnode, current_user.is_authenticated and current_user.is_inspector, bool(flask.request.args.get("template")))
    else:
        status = {'action': 'login', 'action_text': 'Login', 'errors': ["You'll need to login to view this assignment."]}
    
    grade_teachers = None
    if current_user.is_authenticated and current_user.is_teacher:
        grade_teachers = User.query.filter_by(is_teacher=True, is_active=True, is_hidden=False).order_by(User.short_name).all()

    return flask.render_template('node.html',
        student=student,
        extnode=extnode,
        pair_options=pair_options,
        assignments=assignments,
        status=status,
        base_url=f"/curriculum/{extnode['id']}/static/",
        outcomes_by_id=curriculum.get('outcomes_by_id'),
        endterms=curriculum.get('endterms'),
        grade_teachers=grade_teachers,
    ), status_code


RECORD_STATES = {"in_progress", "awaiting_approval", "awaiting_recording"}


def get_attempt_info(extnode, attempt, student, force_new_grading=False):
    """View assignment attempt for a specific student."""

    result: dict = {
        'html': '',
        'submit_warnings': [],
        'attempt_id': attempt.id,
        'attempt': attempt,
    }

    if current_user == student:
        # Determine if this assignment currently requires recording
        if attempt.status in RECORD_STATES and extnode.get("ects") and not extnode.get("allow_ai"):
            result['require_recording'] = attempt.record_status or 'start'

        if attempt.status == 'awaiting_approval':
            result['html'] += f'''Awaiting teacher approval...<span hx-get="/curriculum/{extnode["id"]}?poll_approved=true" hx-trigger="every 5s" hx-target="body"></span>'''

        # While the student is waiting to start an exam, don't show anything yet
        if attempt.status in {"awaiting_approval", "awaiting_recording"}:
            return result
    
    needs_grading = (attempt.status == "needs_grading") or force_new_grading
    show_rubrics = (True if needs_grading else "disabled") if current_user.is_teacher else False
    
    # Load the grades, if any
    latest_grading = attempt.latest_grading
    if latest_grading is None and needs_grading and current_user.is_teacher:
        # Help the teacher by prefilling the grading from a previous attempt that needed to be repaired.
        prev_attempt = Attempt.query.filter(
            Attempt.student_id == attempt.student_id,
            Attempt.node_id == attempt.node_id,
            Attempt.variant_id == attempt.variant_id,
            Attempt.status == "repair"
        ).order_by(Attempt.submit_time.desc()).first()

        if prev_attempt:
            latest_grading = prev_attempt.latest_grading

    # Render the actual assignment to HTML
    if attempt.data_deleted:
        result['html'] = "The content for this attempt is no longer available."
    else:
        ao = Assignment.load_from_directory(attempt.directory)
        ai_grading = AiGrading.query.filter_by(attempt_id=attempt.id).order_by(AiGrading.id.desc()).first() if current_user.is_teacher else None

        ai_grading_feedback = None
        if current_user.is_teacher and ai_grading:
            try:
                ai_grading_feedback = json.loads(ai_grading.output)
            except:
                pass
        
        if not current_user.is_teacher and "ects" in extnode and attempt.status != "in_progress":
            result['html'] = "<div>Exams can only be viewed while taking them. If you'd like to review your exam and see how it was graded, ask a teacher.</div>"
            shown_grading = latest_grading if latest_grading and not latest_grading.needs_consent else None
            result['html'] += ao.render_grading(ao.assignment, ao.goals, show_rubrics, shown_grading)
        else:
            result['html'] = ao.render(show_rubrics, latest_grading, attempt, ai_grading_feedback)

    result['notifications'] = get_notifications(attempt, attempt.latest_grading)
    
    if current_user.is_teacher:
        if attempt.max_screenshot_id > 0:
            result['html'] += f'''
            <div class="buttons is-right mt-2" style="margin-top: 2.4rem;">
                <input class="button" type="button" value="Review screen recording" onclick='reviewRecording("{attempt.id}")'>
                <script src="/static/record-review.js"></script>
            </div>
            '''

        if needs_grading:
            grading_upload = ""
            if extnode.get("grading_upload"):
                req = "required" if extnode.get("grading_upload") == "required" else ""
                grading_upload = f'<input type="file" name="grading_upload" multiple {req}>'
            
            result['html'] = f'''
                <form method="post" action="/inbox/grade/{attempt.id}" name="grading" enctype="multipart/form-data">
                    {result['html']}
                    <input type="hidden" name="csrf_token" value="{generate_csrf()}">
                    <h1>Finalize grading</h1>
                    {"" if "ects" in extnode else render_formative_action(attempt.status)}
                    {grading_upload}
                    <textarea placeholder="Motivation..." class="textarea" name="motivation">{flask.escape(latest_grading.grade_motivation) if latest_grading and latest_grading.grade_motivation else ''}</textarea>
                    <div class="buttons is-right mt-2">
                        <script src="/static/fill-not-graded.js"></script>
                        <button type="button" class="outline" onclick="fillAllGood(this)">All good</button>
                        {'<input type="submit" name="not-graded" class="outline" value="Not graded" formnovalidate>' if "ects" not in extnode else ''}
                        {'<input class="button is-important" type="submit" name="mark_fraudulent" value="Mark as fraudulent">' if "ects" in extnode else ''}
                        {'<input class="button is-warning" type="submit" name="request_consent" value="Request consent">' if "ects" in extnode else ''}
                        {'' if "ects" in extnode and not current_user.bke_certification else '<input class="button is-primary" type="submit" name="publish" value="Publish">'}
                    </div>
                </form>
            '''

            others = User.query \
                .filter_by(is_active=True, is_hidden=False) \
                .join(Attempt, User.id == Attempt.student_id) \
                .filter_by(node_id=attempt.node_id, variant_id=attempt.variant_id, status='needs_grading') \
                .filter(User.id != attempt.student_id)

            for other in others:
                other.presence_class = other.get_presence_class(db.session)

            result["others"] = [
                (other, f"/curriculum/{attempt.node_id}?student={other.short_name}")
                for other in others
            ]
            result["others_message"] = "Other node attempts in need of grading:"
        else:
            # The form is required to keep the name spaces for radio buttons separate when we're showing multiple attempts
            result['html'] = f'<form>{result["html"]}</form>'

            if attempt.status == "awaiting_approval":
                _errors, approvals, _warnings, _action = check_node_startable(extnode, student)
                approvals = "".join(f"<li>{approval}</li>" for approval in approvals)
                result['notifications'].append(f'''
                    Start of this assignment needs to be approved.
                    <ul>{approvals}</ul>
                    <form method="post" action="/attempts/{attempt.id}/handle_approval" class="buttons is-right mt-2 is-primary">
                        <input type="hidden" name="csrf_token" value="{generate_csrf()}">
                        <input role="button" class="outline" type="submit" name="decline" value="Decline">
                        <input role="button" type="submit" name="approve" value="Approve">
                    </form>
                ''')
            elif attempt.status in {"needs_consent", "passed", "repair", "failed", "fraudulent"}:
                if attempt.status == "needs_consent":
                    consent = '<input class="button is-primary" type="submit" name="consent" value="Consent grade">'
                else:
                    consent = ''
                if attempt.data_deleted:
                    regrade = ''
                else:
                    regrade = f"""<a class="button" href="/curriculum/{extnode['id']}?student={attempt.student_id}&amp;regrade={attempt.id}">Regrade</a>"""
                result['html'] += f'''
                    <form method="post" action="/attempts/{attempt.id}/consent" class="buttons is-right mt-2 is-primary">
                        <input type="hidden" name="csrf_token" value="{generate_csrf()}">
                        {regrade}
                        {consent}
                    </form>
                '''

    # It's the actual student. Show submit action in case this is the current attempt.
    if current_user == student and attempt.status == 'in_progress':
        assert attempt == student.current_attempt

        # Create the submit form
        old_values = NodeFeedback.query.filter_by(node_id=attempt.node_id, student_id=student.id).first()
        result['submit_form'] = SubmitForm(obj=old_values) if extnode.get('feedback', True) else EmptySubmitForm()
        if result['submit_warnings']:
            result['submit_form'].submit.label.text += " ignoring warnings"
            result['submit_form'].submit.render_kw = {'class_': 'button is-warning'}

        # Suggest classmates to seek out for help
        if not extnode.get("ects"):
            others = User.query \
                .filter_by(is_active=True, is_hidden=False) \
                .join(Group, User.group_id == Group.id) \
                .filter(Group.name.like(student.group.name[0]+'%' if student.group else '%')) \
                .join(Attempt, User.id == Attempt.student_id) \
                .filter(Attempt.node_id == attempt.node_id) \
                .filter(Attempt.student_id != attempt.student_id) \
                .order_by(Attempt.start_time.desc()) \
                .limit(5)
            
            result['others'] = [(other, f"/people/{other.short_name}") for other in others]
            result['others_message'] = "Most recent classmates to work on this assignment:"

    return result


def render_formative_action(status):
    options = [f'<option value="{k}"{" selected" if k == status else ""}>{v}</option>' for k, v in FORMATIVE_ACTIONS.items()]
    return f'''
        <div class="select mb-2 mt-2">
            <select name="formative_action" required>
                <option value="">What should happen?</option>
                {"".join(options)}
            </select>
        </div>
    '''


def check_node_startable(extnode, student):
    action = 'start'
    warnings = []
    approvals = []
    errors = []
    
    warn_exam = False
    status = extnode["status"]
    if status in {"startable", "paused"}:
        pass
    elif status == "wip":
        approvals.append("Sorry, this module is not ready yet. Please talk to your teachers!")
    elif status == "in_progress":
        action = None
    elif status == "future":
        if not extnode.get("ignore_previous_nodes", False):
            approvals.append("At least one previous node hasn't been completed yet.")
    elif status == "almost_startable":
        if not extnode.get("ignore_previous_nodes", False):
            warnings.append("The previous node hasn't been graded yet.")
    elif status == "needs_grading" or status == "needs_consent":
        warnings.append("You have already submitted this assignment, but it hasn't been graded yet.")
        warnings.append("If you start another attempt your previous attempt won't be graded.")
    elif status in {"passed", "provided"}:
        warnings.append("There's no need to start this assignment again.")
    elif status == "awaiting_recording":
        warn_exam = True
    else:
        errors.append(f"Unknown status: {status}.")

    if extnode.get("ects") and extnode.get("exam_approval", True):
        approvals.append("Teacher approval is required to start the exam.")
        warn_exam = True

    if warn_exam:
        warnings.append("Please review the <a href='/coc#exams' target='_blank'>exam CoC</a> before you start.")
        if not extnode.get("allow_ai"):
            warnings.append("In particular: remember to disable and refrain from using AI tools!")
        warnings.append("Close all windows and tabs unrelated to the exam, in order to protect your privacy and stay within the CoC exam rules. Put your phone in your bag and keep it there (except during lunch breaks when you may use your phone while away from the keyboard).")

    if extnode.get("start_when_ready"):
        warnings.append("As you'll want to interleave other modules with your work on this assignment, you should delay starting the attempt until you're ready to submit your work.")

    if extnode.get('wip'):
        approvals.append("Sorry, this lesson is not ready yet. Please talk to your teachers!")

    current_attempt = student.current_attempt
    if current_attempt:
        if current_attempt.node_id == extnode['id']:
            if current_attempt.status == "awaiting_approval":
                errors.append("Go find a teacher to approve the assignment!")
                action = 'retract'
            elif current_attempt.status == "awaiting_recording":
                errors.append("Start screen recording or find a teacher in case of a problem.")
        elif current_attempt.credits > 0:
            errors.append("You cannot start another assignment while you are working on an exam.")
        elif student.paused_attempt:
            if student.paused_attempt.node_id == extnode['id']:
                action = 'switch-paused'
            else:
                errors.append("You are already working on another assignment and you already have a paused attempt.")
        else:
            warnings.append("You are already working on another assignment. Starting anyway will pause your current attempt.")

    if extnode["module"]["status"] == "future" and extnode["module"].get("count_as_in_progress", True):
        # We're starting a new module. Let's count if we don't have 3 modules open already...
        in_progress = 0
        for other_modules in extnode["periods_with_status"].values():
            for other_module in other_modules:
                if other_module.get("status") == "in_progress" and other_module.get("count_as_in_progress", True):
                    in_progress += 1
        if in_progress >= 3:
            approvals.append(f"You're already working on {in_progress} modules.")

    if action in {'start', 'switch-paused'} and errors:
        action = None

    return errors, approvals, warnings, action


@app.route('/curriculum/<node_id>/static/<name>', methods=['GET'])
def node_file(node_id, name):
    node = curriculum.get('nodes_by_id').get(node_id)
    if not node:
        return "No such assignment", 404
    if not node.get('public_for_externals') and not current_user.is_authenticated:
        return "Permission denied", 403
    static_dir = f"{LMS_DIR}/{node['directory']}/static"
    return flask.send_from_directory(static_dir, name)


def get_student():
    if current_user.is_authenticated:
        student_id = flask.request.args.get('student')
        if student_id and current_user.is_teacher:
            if student_id.isdigit():
                return User.query.get(student_id)
            return User.query.filter_by(short_name=student_id).first()
        if current_user.is_authenticated and not current_user.is_inspector:
            return current_user
    return None
