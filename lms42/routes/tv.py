from ..app import app
from ..models.user import get_all_people
import flask
from flask_login import login_required


@app.route('/tv', methods=['GET'])
@app.route('/tv/<device>', methods=['GET'])
@login_required
def tv(device=None):
    fragment: str = flask.request.args.get('fragment', '')
    
    # Only allow the 'queue' fragment for HTMX requests.
    # If the request is not coming from HTMX, redirect to the standard /tv URL.
    if fragment == 'queue' and not flask.request.headers.get("HX-Request"):
        return flask.redirect(flask.url_for('tv', device=device))

    group_filter = None
    presence_filter = False
    if device == 'd':
        group_filter = "d%"
        presence_filter = True
    elif device in {'e', 'a', 'b', 'iglo'}:
        group_filter = "e%"

    students = get_all_people(group_filter=group_filter, presence_filter=presence_filter) if group_filter else None
    template = fragment if fragment == 'queue' else 'tv'
    return flask.render_template(template+'.html',
        header=False,
        device=device,
        students=students
    )
