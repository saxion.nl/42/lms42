from lms42.utils import render_fragment_template
from ..app import db, app
from ..models.user import UserFlashcard
from ..models import curriculum
from flask_login import login_required, current_user
import datetime
import flask
import time


@app.route('/flashcards', methods=["GET"])
@login_required
def get_cards():
    while True:
        card = current_user.next_flashcard
        
        if card is None:
            return render_fragment_template("message-fragment.html", 'Daily flashcards', message="No more cards for today!", html="<script>document.getElementById('dailyFlashcardsIcon').remove()</script>")
        
        node = curriculum.get('nodes_by_id').get(card.node_id)
        flashcard = node.get("flashcards", {}).get(card.flashcard_id) if node else None
        if flashcard:
            break
        
        # if the card does not exist anymore
        card.next_view = None
        db.session.commit()

    flask.session['flashcard_time'] = time.time()
    
    return render_fragment_template("flashcard.html", "Daily flashcards", daily=True, question=flashcard.get('question'), url=f"/flashcards/{card.node_id}/{card.flashcard_id}?fragment=y")


@app.route('/flashcards/<node_id>/<flashcard_id>', methods=["POST"])
@login_required
def update_card(node_id, flashcard_id):
    duration = time.time() - float(flask.session.get('flashcard_time', 999999))

    node = curriculum.get('nodes_by_id').get(node_id)
    flashcard = node["flashcards"].get(flashcard_id)
    assert flashcard
    
    user_flashcard = load_user_flashcard(node_id, flashcard_id)
    assert user_flashcard

    q, accepted_answer = check_answer(flask.request.form.get('answer'), flashcard, duration)

    user_flashcard.update_interval(q)
    db.session.commit()

    return render_fragment_template("flashcard.html", "Daily Flashcards",
        daily=True,
        correct=bool(accepted_answer),
        answer=accepted_answer or flashcard["answers"][0],
        question=flashcard['question'],
        method="get",
        url="/flashcards?fragment=y"
    )


@app.route('/curriculum/<node_id>/flashcards/<flashcard_id>', methods=["POST"])
def validate_flashcard_answer(node_id, flashcard_id):
    """The route that handles the form of the flashcard that is submitted,
        controls the answer and updates the question in the database accordingly.

    Args:
        node_id (str): the name of the node(assignment)
        card_set_index (str): index of the card list in the assignment
        card_index (str): index of the current card in the card list

    Returns:
        Redirects to the next template if the answer is correct,
        else it renders the right answers for the question.
    """
    # first get the time as quick as possible
    duration = time.time() - float(flask.session.get('flashcard_time', 999999))

    node = curriculum.get('nodes_by_id').get(node_id)
    assert node
    flashcard = node["flashcards"].get(flashcard_id)
    assert flashcard

    q_score, accepted_answer = check_answer(flask.request.form.get('answer'), flashcard, duration)
    
    if current_user.is_authenticated and not load_user_flashcard(node_id, flashcard_id):
        # Don't update the database if the user already has this flashcard in his deck.
        current_date = datetime.datetime.today()
        flash_card = UserFlashcard(
            user_id=current_user.id,
            flashcard_id=flashcard_id,
            node_id=node_id,
            repetitions=0,
            interval=0,
            next_view=current_date,
            easiness_factor=2.5
        )
        db.session.add(flash_card)

        flash_card.update_interval(q_score)
        db.session.commit()

    return flask.render_template("flashcard.html", correct=bool(accepted_answer), question=flashcard["question"], answer=accepted_answer, url=f"../flashcards/{flashcard_id}")


def load_user_flashcard(node_id, flashcard_id):
    return db.session.query(UserFlashcard) \
    .filter_by(
        node_id=node_id,
        flashcard_id=flashcard_id,
        user_id=current_user.id) \
    .first()


def check_answer(input_answer, flashcard, duration):
    # calculates the Q score based on if the answer is correct and the time it took to fill in.
    translation = str.maketrans('', '', flashcard.get("strip_chars", ""))
    input_answer = (input_answer or '').lower().translate(translation)
    
    q = 5 if duration < 8 else (4 if duration < 16 else 3)

    best_answer = flashcard["answers"][0]
    # get the levenshtein_distance
    best_distance = 999 # max distance
    for card_answer in flashcard["answers"]:
        canon_card_answer = card_answer.lower().translate(translation)
        if canon_card_answer == input_answer:
            distance = 0
        elif len(input_answer) > 32: # Might get too slow otherwise
            continue
        else:
            distance = calculate_levenshtein(canon_card_answer, input_answer)
        if distance < best_distance:
            best_answer = card_answer
            best_distance = distance

    if best_distance > flashcard.get("max_errors", 0.2 * len(best_answer)):
        # Large error, wrong!
        accepted_answer = False
        q = 0
    else:
        # Typo, kind of right
        accepted_answer = best_answer
        q *= best_distance / len(best_answer)
    return q, accepted_answer


def calculate_levenshtein(s1, s2):
    # Levenshtein calculates the number of modifications needed to s1(string) in order to get s2(string).
    # create the matrix '*' unpacks the range into a list
    matrix = [[*range(len(s1) + 1)] for _ in range(len(s2) + 1)]
    for i in range(len(s2) + 1):
        matrix[i][0] = i
    for i in range(1, len(s2) + 1):
        for j in range(1, len(s1) + 1):
            matrix[i][j] = min(matrix[i-1][j] + 1, matrix[i][j-1] + 1, matrix[i-1][j-1] + (s2[i-1] != s1[j-1]))

    return matrix[-1][-1]
