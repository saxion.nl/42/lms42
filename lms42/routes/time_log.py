import lms42.models.inbox as inbox
from ..app import db, app, retry_commit
import flask
from flask_login import current_user
import datetime
import sqlalchemy
from ..models.user import Location

CHECK_IN_DEADLINE = datetime.time(10, 30)

TIME_LOG_QUERY = sqlalchemy.text("""
insert into time_log(user_id, date, start_time, end_time, location)
values(:user_id, :date, :time, :time, :location)
on conflict (user_id, date)
do update set end_time = :time
returning start_time
""")

CHECK_IN_ASSIGN_QUERY = sqlalchemy.text("""
update "user"
set check_in_teacher_id = :teacher_id
where id=:student_id
""")

SUSPICIOUS_QUERY = sqlalchemy.text("""
insert into time_log(user_id, date, start_time, end_time, suspicious)
values(:user_id, :date, '7:00', '7:00', true)
on conflict (user_id, date)
do update set suspicious = true
""")


@app.route('/time_log/ping', methods=['GET'])
@retry_commit
def add_time_log():
    now = datetime.datetime.now().replace(microsecond=0)
    ip = flask.request.headers.get('X-Forwarded-For', flask.request.remote_addr)

    if not current_user or not current_user.is_authenticated:
        return {"error": "Not logged in"}
    
    location = Location.ENSCHEDE if ip.startswith(("145.76.", "::ffff:145.76."))\
        else Location.DEVENTER if ip.startswith(("145.2.", "::ffff:145.2."))\
        else Location.LOCAL if ip in {"127.0.0.1", "::1"}\
        else Location.OTHER
    
    if location == Location.OTHER:
        return {"error": f"Not a Saxion IP ({flask.request.remote_addr})"}
    
    if now.hour < 7 or now.hour >= 22:
        db.session.execute(SUSPICIOUS_QUERY, {"user_id": current_user.id, "date": now.date()})
        print(f"Time log outside time window: user_id={current_user.id} now={now}")
        return {"error": "Outside 7:00-22:00 time window"}
    
    time = now.time()
    start_time = db.session.execute(TIME_LOG_QUERY, {"user_id": current_user.id, "date": now.date(), "time": time, "location": location.name}).fetchone()[0]

    if start_time == time:
        if time <= CHECK_IN_DEADLINE:
            # The person is first showing up today, and it's before the check-in deadline.
            teacher_id = current_user.todays_group_teacher_id
            if teacher_id:
                db.session.execute(CHECK_IN_ASSIGN_QUERY, {"student_id": current_user.id, "teacher_id": teacher_id})
            # time log needs to be committed before calling assign_grade_teacher_to_two_oldest_attempts because it uses location
            inbox.add_to_queue(current_user.id, db.session)
            db.session.commit()
        else:
            flask.flash(f"Note: as you seem to have arrived after {CHECK_IN_DEADLINE.strftime('%-I:%M')}, you have not been scheduled for feedback/check-in.")

    return {"success": True}
