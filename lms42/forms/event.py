import flask_wtf
import datetime
import wtforms as wtf
from flask_wtf.file import FileAllowed, FileField
from wtforms import validators as wtv


class AddEventForm(flask_wtf.FlaskForm):
    title = wtf.StringField('Event title', validators=[wtv.InputRequired()])
    description = wtf.TextAreaField('Event description', validators=[wtv.InputRequired()])
    city_code = wtf.SelectField('City', choices=[('e', 'Enschede'), ('d', 'Deventer')], validators=[wtv.InputRequired()])
    address = wtf.StringField('Location', validators=[wtv.InputRequired()], default="Meet in the lounge")
    image = FileField("Poster",
        validators=[FileAllowed(['png', 'jpg'], 'PNG and JPG files only.')],
    )
    event_time = wtf.DateTimeField("Event date and time", validators=[wtv.InputRequired()])
    registration_deadline = wtf.DateTimeField("Registration deadline", validators=[wtv.InputRequired()])
    spots = wtf.IntegerField("Max registration count", validators=[wtv.InputRequired(), wtv.NumberRange(min=0, message='Must enter a number greater than 0')])
    cost = wtf.IntegerField("Cost (Bux)", validators=[wtv.InputRequired(), wtv.NumberRange(min=0, message='Must enter a number greater than 0')])

    submit = wtf.SubmitField('Save')

    def validate(self, **kwargs):
        rv = flask_wtf.FlaskForm.validate(self)
        if not rv:
            return False
        
        now = datetime.datetime.now()

        if self.event_time.data <= now:
            self.event_time.errors.append('Event date has to be in the future.')
            return False
        
        if self.registration_deadline.data <= now:
            self.registration_deadline.errors.append('Registration deadline has to be in the future.')
            return False
        
        if self.event_time.data < self.registration_deadline.data:
            self.event_time.errors.append('Event date has to be after registration deadline.')
            return False

        return True


class JoinEventForm(flask_wtf.FlaskForm):
    submit = wtf.SubmitField('Join event')


class AddBuxForm(flask_wtf.FlaskForm):
    bux = wtf.IntegerField("Number of Bux (€2,50 each)", validators=[wtv.InputRequired(), wtv.NumberRange(min=1, max=20)])
    submit = wtf.SubmitField('Pay up!')
