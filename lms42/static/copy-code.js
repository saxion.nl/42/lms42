function copyCode(button) {
    const container = button.parentElement;
    const codeElement = container.querySelector('pre');
    if (!codeElement) {
      console.error("No <code> or <pre> element found to copy from.");
      return;
    }
    const codeText = codeElement.innerText;
    navigator.clipboard.writeText(codeText).then(() => {
      button.innerText = 'Copied!';
      setTimeout(() => { button.innerText = 'Copy'; }, 2000);
    }).catch(err => {
      console.error('Failed to copy code:', err);
    });
  }
  