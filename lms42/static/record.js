'use strict';

(function() {
    async function fetchWithRetry(url, options = {}, maxRetries = 3) {
        for (let attempt = 1; true; attempt++) {
            try {
                return await fetch(url, options);
            } catch (error) {
                if (error instanceof TypeError && attempt < maxRetries) {
                    await new Promise(resolve => setTimeout(resolve, 2000));
                } else {
                    throw error;
                }
            }
        }
    }

    let recorder;

    // TODO: when in needs_grading state, we'll need to periodically check for approval (using pure htmx?)
    // TODO: somehow mark missing footage in the time line!

    window.recordInit = function(_parentE, _csrfToken, serverStatus) {
        recorder = window.examRecorderInstance = window.examRecorderInstance || new Recorder();

        recorder.parentE = _parentE;
        recorder.csrfToken = _csrfToken;
        recorder.logoE = document.querySelector('#logo-link svg');
        recorder.orgLogoHtml = recorder.logoE.outerHTML;
        recorder.serverStatus = serverStatus;

        recorder.showStatus(recorder.status || (serverStatus=='in_progress' ? 'error' : serverStatus));
    }


    function showBeforeCloseWarning(event) {
        console.log(event.target);
        event.preventDefault();
        return event.returnValue = "This would stop recording! Continue anyway?";
    }


    const STATUS_SVGS = {
        recording: '<svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="6 5 13 15"><path fill="currentColor" d="M12.5 5A7.5 7.5 0 0 0 5 12.5a7.5 7.5 0 0 0 7.5 7.5a7.5 7.5 0 0 0 7.5-7.5A7.5 7.5 0 0 0 12.5 5M7 10h2a1 1 0 0 1 1 1v1c0 .5-.38.9-.86.97L10.31 15H9.15L8 13v2H7m5-5h2v1h-2v1h2v1h-2v1h2v1h-2a1 1 0 0 1-1-1v-3a1 1 0 0 1 1-1m4 0h2v1h-2v3h2v1h-2a1 1 0 0 1-1-1v-3a1 1 0 0 1 1-1m-8 1v1h1v-1"/></svg>',
        error: '<svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 24 24"><path fill="currentColor" d="M1 21L12 2l11 19H1Zm11-3q.425 0 .713-.288T13 17q0-.425-.288-.713T12 16q-.425 0-.713.288T11 17q0 .425.288.713T12 18Zm-1-3h2v-5h-2v5Z"/></svg>',
    };


    class Recorder {

        showStatus(status, msg) {
            this.status = status || this.status;
            // status: start, in_progress, error, finished, removed, 
            
            this.logoE.outerHTML = STATUS_SVGS[this.status] || this.orgLogoHtml;
            this.logoE = document.querySelector('#logo-link svg');
            this.logoE.setAttribute('class', "record "+this.status);
            
            // Add click listener if status is error or finished to restart recording
            if (this.status === 'error' || this.status === 'finished') {
                this.logoE.addEventListener('click', (event) => {
                    event.preventDefault();
                    
                    if (this.status !== 'recording') {
                        this.start(); // Start recording again
                    }
                });
            }
            
            this.parentE.innerHTML = "";
            if (msg) makeNode('div', this.parentE, {text: msg});
            
            if (this.status == 'start' || this.status == 'error') {
                if (this.status == 'error') {
                    makeNode('strong', this.parentE, {text: 'Recording interrupted. Please restart as soon as possible!'});
                }
                const userAgent = navigator.userAgent.toLowerCase();
                if (userAgent.includes('safari') && !userAgent.includes('chrome')) {
                    makeNode('strong', this.parentE, {text: 'Sorry, screen recording with Safari works unreliably. Please use Chrome, a Chrome-based browser or Firefox.'});
                } else {
                    makeNode('div', this.parentE, {html: 'Start screen recording by pressing this button, and then giving your browser permission to share your <strong>entire screen</strong>, and not just a single application or tab.'});
                    let buttonsE = makeNode('.buttons', this.parentE);
                    makeNode('button.m-2', buttonsE, {text: 'Start recording', click: () => this.start()});
                }
            } else if (this.status == 'recording') {
                makeNode('div', this.parentE, {text: 'Screen recording is running.'});
            } else {
                makeNode('div', this.parentE, {text: `Record status: ${this.status}.`});
            }
        }
        

        stop() {
            if (this.stream) {
                this.stream.getTracks().forEach(track => track.stop());
            }
            clearInterval(this.takeScreenshotInterval);

            window.removeEventListener("beforeunload", showBeforeCloseWarning);

            this.takeScreenshotInterval = this.stream = this.videoE = this.canvasE = this.context = this.prevData = this.takeScreenshotInterval = undefined;
        }
        
        async start() {
            
            if (Notification.permission != 'granted') {
                this.parentE.innerHTML = `Please give permission to show popup notifications in case there is recording problem.`;
                Notification.requestPermission();
            }
            
            this.parentE.innerHTML = `Please give permission to capture your entire screen. Are you using multiple screens? Install the <em>plasma-wayland-session</em> Manjaro package, log out, and while on the login screen select a *Wayland* session before entering your password. Now you should be able to choose *Full Workspace* when starting screen capturing.`;
            this.stream = await navigator.mediaDevices.getDisplayMedia({ video: {frameRate: 1}, audio: false });
            let track = this.stream.getVideoTracks()[0];
        
            if (track.label !== '' && !track.label.startsWith('Screen') && !track.label.startsWith('screen:') && track.label !== 'Primary Monitor' && !track.label.includes('_PIPEWIRE_')) {
                this.stop();
                this.showStatus(this.status, `Error! You need to capture the entire screen (but got '${track.label}')!`);
                return;
            }

            this.parentE.innerHTML = `Starting recording...`;
        
            window.addEventListener("beforeunload", showBeforeCloseWarning);
        
            this.videoE = document.createElement('video');
            this.videoE.autoplay = true;
            this.videoE.srcObject = this.stream;
        
            this.videoE.addEventListener('loadedmetadata', wrapWithErrorHandler(() => {
                this.canvasE = document.createElement('canvas');
                this.canvasE.width = this.videoE.videoWidth;
                this.canvasE.height = this.videoE.videoHeight;
        
                this.context = this.canvasE.getContext("2d", {willReadFrequently: true});
                this.prevData = new Uint8ClampedArray();
                this.forceTime = 0;
            
                this.takeScreenshotInterval = setInterval(this.takeScreenshot.bind(this), 3000);
                this.takeScreenshot();
            }));
        
            this.stream.addEventListener('inactive', () => this.handleError() );
            this.stream.addEventListener('ended', () => this.handleError() );
        }

        takeScreenshot() {
            if (!this.stream.active) {
                this.handleError();
                return;
            }
            
            let now = Date.now();
            let lastSaved = localStorage.getItem('lastScreenshot');
            let timeDiff = now - lastSaved;
            if (lastSaved && timeDiff < 770) {
                // Some other browser window already did this.
                return;
            }
            localStorage.setItem('lastScreenshot', now);

            this.context.drawImage(this.videoE, 0, 0);
            let imageData = this.context.getImageData(0, 0, this.canvasE.width, this.canvasE.height);

            let data = imageData.data;
            let prevData = this.prevData;
            let dataLen = imageData.width * imageData.height * 4;
            let diff;

            if (now > this.forceTime) {
                this.forceTime = now + 59.5*1000;
                diff = 100;
            } else {
                diff = 0;

                for(let i=0; i<10000; i++) {
                    let index = Math.floor(Math.random() * dataLen);
                    if (data[index] !== prevData[index]) diff++;
                }

                diff /= 100;
            
                if (diff < 2) {
                    return;
                } 
            }

            if (diff > 8) {
                this.uploadScreenshot();
            }

            this.prevData = data;
        }

        async uploadScreenshot() {
            const blob = await new Promise((resolve, reject) => {
                this.canvasE.toBlob((b) => {
                    if (b) {
                        resolve(b);
                    } else {
                        reject(new Error('Error creating blob'));
                    }
                }, 'image/png');
            });
        
            const response = await fetchWithRetry('/record/upload', {
                method: 'POST',
                headers: {
                    'Content-Type': 'image/png',
                    'X-CSRFToken': this.csrfToken
                },
                body: blob
            }, 5);
        
            if (response.status != 200) {
                console.error('Upload failed', response);
                this.showStatus('error', 'Recorder failed to upload screenshot. Please contact a teacher!');
                return;
            }
        
            let responseJson = await response.json();
        
            // image.src = canvasE.toDataURL('image/png'); 
        
            if (responseJson.status == 'stop') {
                this.showStatus('finished');
                localStorage.removeItem('lastScreenshot');
                window.removeEventListener("beforeunload", showBeforeCloseWarning);    
            } else {
                if (this.status != 'recording') {
                    this.showStatus('recording');
                    if (this.serverStatus === 'start') {
                        htmx.ajax('GET', location.path, {
                            target: document.body,
                            // select: 'main',
                            // swap: 'outerHTML',
                        })
                    }
                    // TODO: reload while preserving stream
                }
            }
        }

        handleError(msg) {
            console.error('Recording error', msg);
            msg = msg || 'Stream was interrupted! Please restart it immediately or contact a teacher.';
            this.showStatus('error', msg);

            if (Notification.permission === 'granted') {
                new Notification('Recording interrupted!', {
                    body: msg,
                    icon: '/static/favicon.svg'
                });
            }    
            this.stop();
        }
    }

    function wrapWithErrorHandler(func) {
        return async function(...args) {
            try {
                return await func.apply(this, args);
            }
            catch(e) {
                console.error('Caught exception', e);
                this.handleError('Stream stopped due to browser error: '+e)
                throw e;
            }
        };
    }

    // Wrap some Recorder methods in an error catcher that alerts the user.
    for(const name of ['start', 'takeScreenshot', 'uploadScreenshot']) {
        Recorder.prototype[name] = wrapWithErrorHandler(Recorder.prototype[name]);
    }

})();
