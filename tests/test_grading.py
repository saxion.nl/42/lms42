import json
from common import run_attempt, grade_attempt, find_objective, set_grade_teacher


def test_auto_grade_previous_attempt(client):
    # Log student in
    client.login("student1")

    # Run an attempt and start a new one after its submitted with the 'Start new attempt' button
    run_attempt(client, "linux", submit=True, start_approval=False, start_anyway=False)
    run_attempt(client, "linux", submit=False, start_approval=False, start_anyway=True)

    client.open("/curriculum/linux")
    # Check if the old attempt has been set as failed
    client.find("div", text="Not finished after 0.0 working days.").require(1)

    # Check if the old attempt has been set as failed in the student dashboard
    client.open("/people/student1")
    client.find("tr.not_graded").find("td", text="-").require(1)


def test_not_graded_button(client):
    # Log student in
    client.login("student1")

    # Run an attempt and start a new one after its submitted with the 'Start new attempt' button
    run_attempt(client, "linux", submit=True)

    client.login("teacher1")
    client.open("/curriculum/linux?student=student1")

    # Find the not graded button and click it
    client.find("input", value="Not graded").submit()
    client.open("/curriculum/linux?student=student1")

    # Check if the old attempt has been set as failed
    client.find("div", text="Not finished after 0.0 working days.").require(1)

    # Check if the old attempt has been set as failed in the student dashboard
    client.open("/people/student1")
    client.find("tr.not_graded").find("td", text="-").require(1)


def test_checkbox_grading(client):
    client.login("student1")
    run_attempt(client, "vars", submit=False)
    vars_objective_count = 9

    # marking each objective true
    for index in range(vars_objective_count):
        client.put('/mark_objective/', no_html=True, data=json.dumps({
            "id": index,
            "checked": True
        }))

    # sanity check each objective
    client.open("/curriculum/vars")
    for index in range(vars_objective_count):
        find_objective(client, index).find('.completed-objective').require(1)

    run_attempt(client, "vars", start=False, submit=True)

    passing_grade = ("4", "", "")
    objectives_grades: list[tuple] = [passing_grade for _ in range(vars_objective_count)]

    objectives_grades[0] = ("1", "", "") # An objective with a low grade (should no longer be checked)
    objectives_grades[1] = ("3", "a comment", "") # An objective with a 3 and a comment (should no longer be checked)
    objectives_grades[2] = ("3", "", "") # An objective with a 3 and no comment (should still be checked)

    objectives_grades[7] = ("no", "", "") # must objective

    grade_attempt(client, "vars", "student1", action="repair", objectives_grades=objectives_grades)
    
    client.login("student1")

    run_attempt(client, "vars", submit=False)
    client.open("/curriculum/vars")

    find_objective(client, 0).find('.completed-objective').require(0)
    find_objective(client, 1).find('.completed-objective').require(0)
    find_objective(client, 2).find('.completed-objective').require(1)


def test_autopass(client):
    client.login("student1")
    run_attempt(client, "lms-impl", submit=True)

    client.open("/curriculum/lms-impl")
    client.find(text="Autopass").require()
    client.find(text="The student passes.").require()


def test_teacher_start_and_submit(client):
    client.login("teacher1")

    client.open("/curriculum/vars?student=student1")
    client.find(name="teacher-start-submit").require(0) # Needs student upload

    client.open("/curriculum/teamwork-project?student=student1")
    client.find("input", name="request_consent").require(0) # Not gradable
    client.find(name="teacher-start-submit").submit()
    client.find("input", name="request_consent").require(1) # Gradable!


def test_provided_by(client):
    client.login("student1")
    run_attempt(client, "oss-project", submit=True, start_approval=True, start_anyway=False, grade="passed")
    client.open("/curriculum")
    client.find(".provided", href="/curriculum/lms-project").require()
    client.find(".provided", href="/curriculum/lms-merge").require()


def test_request_consent_removes_assigned_grade_teacher(client):
    run_attempt(
        client,
        "ux-exam",
        start_anyway=False,
        start_approval=True,
        submit=True,
        student_name="student1",
    )

    set_grade_teacher(
        client,
        student_short_name="student1",
        exam="ux-exam",
        teacher_short_name="teacher1",
    )

    # request consent
    grade_attempt(
        client,
        "ux-exam",
        "student1",
        action=None,
        teacher_name="teacher1",
        objectives_grades=None,
        exam_action="Request consent",
    )

    client.open("curriculum/ux-exam?student=student1")
    # teacher1 should not be selected
    client.find("select", name="grade_teacher").find(
        "option", selected="", text="teacher1"
    ).require(0)


def test_consent_required_removes_assigned_grade_teacher(client):
    run_attempt(
        client,
        "ux-exam",
        start_anyway=False,
        start_approval=True,
        submit=True,
        student_name="student1",
    )

    set_grade_teacher(
        client,
        student_short_name="student1",
        exam="ux-exam",
        teacher_short_name="teacher1",
    )

    # grade with a 6 and publish, this triggers a consent
    grade_attempt(
        client,
        "ux-exam",
        "student1",
        action=None,
        teacher_name="teacher1",
        objectives_grades=[
            ("yes", "jobs", ""),
            ("3", "example mapping", ""),
            ("3", "questions", ""),
            ("3", "domain model", ""),
            ("2", "flows", ""),
            ("2", "wireframes", ""),
            ("2", "desktop", ""),
            ("2", "test", ""),
        ],
        exam_action="Publish",
    )

    client.open("curriculum/ux-exam?student=student1")
    # teacher1 should not be selected
    client.find("select", name="grade_teacher").find(
        "option", selected="", text="teacher1"
    ).require(0)
