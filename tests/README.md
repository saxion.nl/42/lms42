# lms42 automated tests

Setting up integration testing for a Flask application turns out to be rather involving, which is why we didn't do it initially. But as the project grows, so does the need for automated testing. We have now set up a testing architecture that should get us pretty far - but we don't have many tests yet. This doc is to help test-writers to get started.

**Note:** this testing framework is rather... untested. So if you run into unexpected things, you might have found a glitch. Feel free to poke around and see if you can fix it, or ask for help.

## Files and test functions

When you run `bin/sd42 test`, all files matching `tests/test_*.py` will be scanned for functions named `test_*`, which will be run as test cases.

It is recommended to create a file per topic, each contain say 3 up to 10 test cases.

Test functions look like this:

```python
def test_sanity():
    assert True == True
```

You can run tests selectively (which is convenient when you're working on a test), by specifying a part of the tests function name like so:

```sh
bin/sd42 test -k auth
```

In this case `auth` would match a function named `test_authorization`, for instance.

## Fake server

The tests do not require an actual lms42 server to be running. Instead, requests can be handed over to the Flask library running within the testing process. This has some advantages, including the ability to see server-side back-traces in the error messages, and to set up the Flask environment such that it works with fake data (see _Database_).

In order to make such requests, tests can use the `client` fixture, just by having a function argument named `client`. The testing framework will provide a `FlaskClient` object (with some custom extensions that we'll see later), that allows us to make fake requests:

```python
def test_example1(client):
    response = client.get("/curriculum")
    assert b"Introduction to Python" in response.data

def test_example2(client):
    response = client.post("/user/login", data=dict(
        email="nosuchuser@example.com",
    ))
    assert b"This address is not registered." in response.data
```

When a test case fails (raises an exception), you'll be shown debug output, including any requests that have been sent and the filename (like `tests/responses/12.html`) for a file that contains the complete response body, for debugging.

When the response contains invalid HTML5, an error including line numbers will be raised automatically.

## Fake browser

In order to make the tests catch a broader range of errors, we've added a tiny bit of browser logic to `client`. Through the `client.find` method you can search for HTML elements within the last response HTML. It returns a `TreeSet` that can be queried further. In order to verify that there are 5 elements with tag `textarea` and a `comment` CSS class on a page:

```python
def test_example3(client):
    client.get("/page/with/five/comment/inputs")
    result_set = client.find("textarea.comment")
    result_set.require(5)
```

You can also chain calls, and specify any HTML attribute you want. So this would give us all `<input>` elements with name `abc` within `<forms>` that have action `/comments`:

```python
def test_example4(client):
    client.get("/page/with/five/comment/inputs")
    result_set = client.find("form", action="/comments").find("input", name="abc")
```

There are a couple of special attributes, the most important of which are:

-   `text` will match the direct text content of an HTML node (after it has been stripped of surrounding spaces).
-   `in_text` will match if the HTML node text _contains_ the provided string.

In order to follow the link on an HTML `<a>` element, you can call `open` on the result set that contains just one such element:

```python
def test_example5(client):
    client.get("/some/page")
    client.find("a", text="Terms of service").open()
    client.find(in_text="you must pay us $1000000 per day").require()
```

To submit a `<form>` you can call `submit` on the form element, or any of its descendants. If that descendant is an `<input type=submit>`, the form will be submitted as if that button was pressed. You can provide form data as keyword arguments to the `submit` method. This data will be checked with the form, supplemented with default values from the form, and then submitted according to the form's `method` and `action`.

```python
def test_example6(client):
    client.get("/user/123")
    client.find("input", type="submit", text="Update").submit(first_name="Piet")
    client.find("h2", text="Piet de Vries")
```

It is also possible to inspect the individual elements of a result set, which are [ElementTree](https://docs.python.org/3/library/xml.etree.elementtree.html) object as provided by the Python standard library.

The following finds all `.grade` elements contained within `section.attempts` elements, checks that there are exactly 5 on the pages, and loops over them. For each, it checks that the element has an `id` attribute and that the text contained is an integer between 1 and 10.

```python
def test_example6(client):
    client.get("/some/page")
    for grade_el in client.find("section.attempts").find(".grade").require(5):
        assert grade_el.get("id") != None
        assert 1 <= int(grade_el.text) <= 10
```

`TreeSet` has some more tricks that you can read about in its inline documentation.

## Database

One of the hard things about testing is getting your database to a known consistent
state before each test. We've set up testing to behave as follows:

-   When starting a new testing session, a whole new PostgreSQL data directory is created (within `tests/data/<random>`) and a PostgreSQL server is started.
-   All database migrations are ran on the empty database.
-   A couple of user accounts (admin@example.com, student[1-5]@example.com and teacher[12]@example.com) are created using `POST /people/new` calls.
-   Each individual tests runs within a database transaction that is reverted after the test completed. This means that changes made during a test will not be visible to the next test.
-   After all tests are done, the PostgreSQL server is shutdown and the database directory is deleted.

## Data

Besides a database, lms42 also stores some data (avatar images, link previews and attempt solutions, etc) in files. As it is now, each test _session_ gets an empty data directory that it can use, which is deleted after the session.

We may want to consider emptying the data directory before each test, but that would also cause expected subdirectories (like `avatars/`), that are created when the lms42 code first gets loaded, to disappear.

## Test Inspector

You can inspect what specific line of code created an response using the LMS Test Inspector.
On the left you will see the styled HTML and on the right the corresponding line of code.

Navigating can be done by means of the arrow keys and you can jump to a specific response by specifying the number in the input box.

Run this tool by running `bin/sd42 test-inspector`

## VS code Test Explorer

You can run tests via VS code Test Explorer. Debugging does not work, because the test fixtures connect to a database which is not allowed from the debugger environment.

## Debugging

You can debug tests by passing a debug flag to the test command.

```sh
bin/sd42 test --debug -k auth
```

This will start the test run, initialize the database connection and then wait for a debugpy process to attach at port 5678.

You can start a debugger in VS code by launching the "Attach to Flask App (debugpy)" debug configuration.

You can put breakpoints in tests, routes and even in Jinja templates.
