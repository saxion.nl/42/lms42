import random


def run_attempt(
    client, node_id, *, start=True, submit=True, grade=None, start_anyway=None, start_approval=None, student_name=None
):
    """Start and/or submit and/or grade an attempt.

    Args:
        client (Client): Test client object.
        node_id (str): Node identifier, like "funcstr" or "vars".
        start (bool, optional): Whether to start the attempt. Defaults to True.
        submit (bool, optional): Whether to submit the attempt. Defaults to True.
        grade (str, optional): Whether and how to grade the attempt. Can be `None` or `"passed` or `"failed"`. Defaults to None.
        start_anyway (bool, optional): How to handle starting attempts with warnings. Can be `None` (may require 'anyway'), `True` (should be 'anyway'), `False` (should not be 'anyway'). Defaults to None.
        start_approval (bool, optional): How to handle starting attempts that need approval. Can be `None` (may be approved), `True` (must be approved), `False` (may not be approved). Defaults to None.
        student_name (str, optional): Login as this student first. Defaults to None.
    """

    if student_name:
        client.login(student_name)

    if start:
        client.open(f'/curriculum/{node_id}')
        start_button = client.find('input', value='Start attempt')
        if start_button:
            assert start_anyway is not True
        else:
            start_button = client.find('input', value='Start attempt anyway')
            assert not start_button or start_anyway is not False

        if start_button:
            assert start_approval is not True
            start_button.submit()
        else:
            assert start_approval is not False
            assert start_anyway is not True
            client.find('input', value='Request teacher approval').submit()
            client.find(text='Go find a teacher to approve the assignment!')
            # todo: start recording
            student_name = client.user_name
            client.login('teacher1')
            client.open(f'/curriculum/{node_id}?student={student_name}')
            client.find('input', value='Approve').submit()
            client.login(student_name)

    if submit:
        client.open(f'/curriculum/{node_id}')
        client.find('input', value='Submit attempt').submit(
            **({'finished': 'yes'} if client.find('input', name='finished') else {})
        )
        confirm_no_upload = client.find('input', value="Yes, I'm sure")
        if confirm_no_upload:
            confirm_no_upload.submit()

    if grade is not None:
        student_name = client.user_name
        grade_attempt(client, node_id, student_name, grade)
        client.login(student_name)


def grade_attempt(
    client,
    node_id,
    student_short_name,
    action='passed',
    teacher_name='teacher1',
    objectives_grades=None,
    mark_fraudulent=False,
    exam_action="Publish",
):
    # Formative action should be `None` for exams
    client.login(teacher_name)
    client.open(f'/curriculum/{node_id}?student={student_short_name}')
    form_data = {}
    if client.find("input", name="formative-action"):
        form_data["formative_action"] = action

    if objectives_grades:
        for index, objective_grade in enumerate(objectives_grades):
            score, motivation, score_type = objective_grade

            form_data[f"score_{index}{score_type}"] = score
            form_data[f"motivation_{index}"] = motivation
    else:
        for element in client.find('input', type='radio', value='3'):
            form_data[element.get('name')] = '3' if action == 'passed' else '1'

        for element in client.find('input', type='number', value=''):
            form_data[element.get('name')] = '10' if action == 'passed' else '1'

        for element in client.find('input', type='radio', value='yes'):
            form_data[element.get('name')] = 'yes' if action == 'passed' else 'no'

    if mark_fraudulent:
        client.find("input", value="Mark as fraudulent", limit=1).submit(**form_data)
        return

    random.seed(0)  # make sure consent is not needed
    client.find('input', value=exam_action, limit=1).submit(**form_data)


def find_objective(client, objective_id: int):
    return client.find('.objective').containing(attributes={'data-pos': objective_id}).require(1)


def set_grade_teacher(client, student_short_name: str, exam: str, teacher_short_name: str):
    # set grade teacher, can not do this via fake html page, because htmx and no js
    client.login(teacher_short_name)
    client.open(f'curriculum/{exam}?student={student_short_name}')
    select_el = client.find('select', name='grade_teacher')
    htmx_path = select_el.get().get('hx-put')
    teacher_id = select_el.find('option', text=teacher_short_name).get().get('value')
    client.put(
        htmx_path,
        data={'grade_teacher': teacher_id},
        no_html=True,
    )
