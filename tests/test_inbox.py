from lms42.models import inbox
from tests.common import run_attempt
from freezegun import freeze_time
from lms42.app import db
from lms42.models.attempt import Attempt
from lms42.models.user import User


DEVENTER_IP = "145.2.0.0"
ENSCHEDE_IP = "145.76.0.0"
OTHER_IP = "145.3.0.0"


def test_internship_filter(client):
    # Login as student1
    client.login('student1')

    # Submit an internship assignment/project
    run_attempt(client, "internship1-project", submit=True)

    # Login as teacher1
    client.login('teacher1')

    # Open the inbox with the exam filter applied
    client.open('/inbox?internship=y')
    
    # Check if student1 appears in the inbox
    client.find("a", text="student1").require(1)
    client.find("a", text="student2").require(0)


def test_exam_filter(client):
    # Login as student2
    client.login('student2')

    # Submit an exam
    run_attempt(client, "ux-exam", submit=True)

    # Login as teacher1
    client.login('teacher1')

    # Open the inbox with the exam filter applied
    client.open('/inbox?exam=y')
    
    # Check if student2 appears in the inbox
    client.find("a", text="student2").require(1)
    client.find("a", text="student1").require(0)


def test_internship_and_exam_filter(client):
    # Login as student1
    client.login('student1')

    # Submit an internship assignment/project
    run_attempt(client, "internship1-project", submit=True)

    # Login as student2
    client.login('student2')

    # Submit an exam
    run_attempt(client, "ux-exam", submit=True)

    # Login as teacher1
    client.login('teacher1')

    # Open the inbox with both filters applied
    client.open('/inbox?internship=y&exam=y')

    # Check if student1 and student2 appear in the inbox
    client.find("a", text="student1").require(1)
    client.find("a", text="student2").require(1)


def enqueue_student(client, nr: int, assignment="linux", approval=False):
    client.login(f"student{nr}")
    run_attempt(client, assignment, start_anyway=False, start_approval=approval, submit=True)
    client.open("/time_log/ping", environ_base={"REMOTE_ADDR": DEVENTER_IP}, no_html=True)


@freeze_time('2025-02-01 10:30:01')
def test_not_queued_after_half_past_ten(client):
    client.login("teacher0")
    client.open("/time_log/ping", environ_base={"REMOTE_ADDR": DEVENTER_IP}, no_html=True)

    enqueue_student(client, 0)

    client.login("teacher0")
    client.open("/inbox")
    client.find("h2", text="Empty!").require(1)


@freeze_time('2025-02-01 10:00:00')
def test_queued_at_same_location(client):
    client.login("teacher0")
    client.open("/time_log/ping", environ_base={"REMOTE_ADDR": DEVENTER_IP}, no_html=True)

    enqueue_student(client, 0)

    client.login("teacher0")
    client.open("/inbox")
    client.find("div#queue-teacher0").find("a", text="student0").require(1)


@freeze_time('2025-02-01 10:00:00')
def test_queued_at_different_location(client):
    client.login("teacher0")
    client.open("/time_log/ping", environ_base={"REMOTE_ADDR": ENSCHEDE_IP}, no_html=True)

    enqueue_student(client, 0)

    client.login("teacher0")
    client.open("/inbox")
    client.find("h2", text="Empty!").require(1)


@freeze_time('2025-02-01 10:00:00')
def test_only_grade_with_student_attempts_are_queued_for_a_teacher(client):
    # register location for teacher
    client.login("teacher0")
    client.open("/time_log/ping", environ_base={"REMOTE_ADDR": DEVENTER_IP}, no_html=True)
   
    # grading with student on same location
    enqueue_student(client, 0)
   
    # grading without student on same location
    enqueue_student(client, 1, "ux-exam", True)

    client.login("teacher0")
    client.open("/inbox")

    # only grading with student should be queued for teacher
    client.find("div#queue-none").find("a", text="student1").require(1)
    # grading without student should be visible in general queue
    client.find("div#queue-teacher0").find("a", text="student0").require(1)


@freeze_time("2025-02-01 10:00:00", tick=True)
def test_two_oldest_attempts_are_queued_three_possible(client):
    client.login("teacher0")
    client.open("/time_log/ping", environ_base={"REMOTE_ADDR": DEVENTER_IP}, no_html=True)

    client.login("student0")
    run_attempt(client, "linux", start_anyway=False, start_approval=False, submit=True)
    run_attempt(client, "vars", start_anyway=True, start_approval=False, submit=True)
    run_attempt(client, "loops", start_anyway=True, start_approval=False, submit=True)
    client.open("/time_log/ping", environ_base={"REMOTE_ADDR": DEVENTER_IP}, no_html=True)

    client.login("teacher0")
    client.open("/inbox")
    client.find("div#queue-teacher0").find("a", text="student0").require(1)
    client.find("div#queue-teacher0").find("div.tags").find("a", text="linux").require(1)
    client.find("div#queue-teacher0").find("div.tags").find("a", text="vars").require(1)
    client.find("div#queue-teacher0").find("div.tags").find("a", text="loops").require(0)


def set_grade_teacher_for_all_attempts(short_name):
    # Currently the grade teacher is set to None after grading the intro assignments.
    # To simulate older attempts that are still in the database reset the grade teacher id
    teacher = User.query.filter_by(short_name=short_name).first()
    attempts = Attempt.query.all()
    for attempt in attempts:
        attempt.grade_teacher_id = teacher.id
    db.session.commit()


@freeze_time('2025-02-01 10:00:00')
def test_coach_first_if_queue_shorter_than_five(client):
    num_teachers = 2
    for i in range(num_teachers):
        client.login(f"teacher{i}")
        client.open("/time_log/ping", environ_base={"REMOTE_ADDR": DEVENTER_IP}, no_html=True)
    
    # check if attempts that do not need grading, but have grade_id set to coach are ignored
    set_grade_teacher_for_all_attempts("teacher0")

    # students 0-5 have teacher 0 as coach, students 6-11 have teacher 1 as coach
    num_students = 5
    for i in range(num_students):
        enqueue_student(client, i)

    client.login("teacher0")
    client.open("/inbox")
    for i in range(num_students):
        client.find("div#queue-teacher0").find("a", text=f"student{i}").require(1)
    client.find("div#queue-teacher1").require(0)


@freeze_time('2025-02-01 10:00:00')
def test_shortest_queue_if_coach_queue_longer_than_five(client):
    num_teachers = 2
    for i in range(num_teachers):
        client.login(f"teacher{i}")
        client.open("/time_log/ping", environ_base={"REMOTE_ADDR": DEVENTER_IP}, no_html=True)
    
    # check if attempts that do not need grading, but have grade_id set to teacher other than coach are ignored
    set_grade_teacher_for_all_attempts("teacher1")

    # students 0-5 have teacher 0 as coach, students 6-11 have teacher 1 as coach
    students_teacher0 = range(6)
    for i in students_teacher0:
        enqueue_student(client, i)
    
    # Teacher0 students fill his queue (max = 5) first
    # The rest of teacher0 students should be added to teacher1 queue
    client.login("teacher0")
    client.open("/inbox")
    max_queue_length = 5
    for i in students_teacher0[:max_queue_length]:
        client.find("div#queue-teacher0").find("a", text=f"student{i}").require(1)
    
    for i in students_teacher0[max_queue_length:]:
        client.find("div#queue-teacher1").find("a", text=f"student{i}").require(1)


@freeze_time('2025-02-01 10:00:00')
def test_shortest_queue_if_coach_queue_longer_than_five_but_also_shortest(client):
    num_teachers = 2
    for i in range(num_teachers):
        client.login(f"teacher{i}")
        client.open("/time_log/ping", environ_base={"REMOTE_ADDR": DEVENTER_IP}, no_html=True)
    
    # students 0-5 have teacher 0 as coach, students 6-11 have teacher 1 as coach
    # if enqueue for student that has coach
    # coach 5 queue has 5
    students_teacher0 = range(6)
    max_queue_length = 5
    for i in students_teacher0[:max_queue_length]:
        enqueue_student(client, i)

    # other queue has 5
    students_teacher1 = range(6, 12)
    max_queue_length = 5
    for i in students_teacher1[:max_queue_length]:
        enqueue_student(client, i)
    # when equal goes to first in table
    enqueue_student(client, 11)

    # when not equal goes to shortest
    enqueue_student(client, 5)

    client.login("teacher0")
    client.open("/inbox")
    for i in students_teacher0[:max_queue_length]:
        client.find("div#queue-teacher0").find("a", text=f"student{i}").require(1)
    client.find("div#queue-teacher0").find("a", text="student11").require(1)

    for i in students_teacher1[:max_queue_length]:
        client.find("div#queue-teacher1").find("a", text=f"student{i}").require(1)
    client.find("div#queue-teacher1").find("a", text="student5").require(1)


def test_checkins_not_displayed_in_queue_no_attempt(client):
    client.login("teacher0")
    client.open("/time_log/ping", environ_base={"REMOTE_ADDR": DEVENTER_IP}, no_html=True)
    client.open("/people/student0")
    # need put, because htmx javascript is not running in test environment
    client.put("/people/104/check_in_teacher", no_html=True, data='check_in_teacher:"101"')
    client.open("/inbox")
    client.find("h2", text="Empty!").require(1)


@freeze_time('2025-02-01 10:00:00')
def test_checkins_not_displayed_in_queue_with_attempt(client):
    client.login("teacher0")
    client.open("/time_log/ping", environ_base={"REMOTE_ADDR": DEVENTER_IP}, no_html=True)

    client.open("/people/student0")
    # need put, because htmx javascript is not running in test environment
    client.put("/people/104/check_in_teacher", no_html=True, data='check_in_teacher:"101"')
    enqueue_student(client, 0)

    client.login("teacher0")
    client.open("/inbox")
    client.find("div#queue-teacher0").find("a", text="student0").require(1)
    client.find("div#queue-teacher0").find("div.tags").find("a", text="linux").require(1)
    client.find("div#queue-teacher0").find("div.tags").find("dev.tag", text="check-in").require(0)


@freeze_time('2025-02-01 10:00:00')
def test__remove_attempts_that_require_teacher_feedback_from_queues_at_six(client):
    client.login("teacher0")
    client.open("/time_log/ping", environ_base={"REMOTE_ADDR": DEVENTER_IP}, no_html=True)

    enqueue_student(client, 0)
    enqueue_student(client, 1, "ux-exam", True)

    client.login("teacher0")
    client.open("/inbox")
    client.find("div#queue-teacher0").find("div.tags").find("a", text="linux").require(1)
    client.find("div#queue-none").find("div.tags").find("a", text="ux-exam").require(1)

    # jobs not scheduled in tests, so call method now
    inbox.remove_attempts_that_require_teacher_feedback_from_queues()

    client.open('/inbox')
    client.find("div#queue-teacher0").find("div.tags").find("a", text="linux").require(0)
    client.find("div#queue-none").find("div.tags").find("a", text="ux-exam").require(1)
