import json
from common import run_attempt



def test_permission(client):
    client.login("student1")
    client.open('/curriculum/linux')
    client.open('/curriculum/vars')
    client.find("input", value="Request teacher approval").require()


def test_assignment(client):
    run_attempt(client, "linux", student_name="student1", start_anyway=False, start_approval=False, submit=True)


def test_pause_attempt(client):
    client.login("student1")
    run_attempt(client, "linux", start_anyway=False, start_approval=False, submit=True)

    # test pause
    client.open('/curriculum/html')
    client.find(text="The previous node hasn't been graded yet.").require()
    client.find("input", value="Start attempt anyway").submit()

    run_attempt(client, "vars", start_anyway=True, start_approval=False, submit=False)
    client.find(text="Attempt paused!").require()

    client.open('/curriculum/queries')
    client.find(text="You are already working on another assignment and you already have a paused attempt.").require()

    run_attempt(client, "vars", start=False, submit=True)

    client.open('/curriculum/html')
    client.find("input.button", value='Submit attempt').require()


def test_auto_grade_previous_attempt(client):
    # Log student in
    client.login("student1")

    # Run an attempt and start a new one after its submitted with the 'Start new attempt' button
    run_attempt(client, "linux", submit=True, start_approval=False, start_anyway=False)
    run_attempt(client, "linux", submit=False, start_approval=False, start_anyway=True)

    client.open("/curriculum/linux")
    # Check if the old attempt has been set as failed
    client.find("div", text="Not finished after 0.0 working days.").require(1)

    # Check if the old attempt has been set as failed in the student dashboard
    client.open("/people/student1")
    client.find("tr.not_graded").find("td", text="-").require(1)


def test_not_graded_button(client):
    # Log student in
    client.login("student1")

    # Run an attempt and start a new one after its submitted with the 'Start new attempt' button
    run_attempt(client, "linux", submit=True)

    client.login("teacher1")
    client.open(f'/curriculum/linux?student=student1')

    # Find the not graded button and click it
    client.find("input", value="Not graded").submit()
    client.open(f'/curriculum/linux?student=student1')

    # Check if the old attempt has been set as failed
    client.find("div", text="Not finished after 0.0 working days.").require(1)

    # Check if the old attempt has been set as failed in the student dashboard
    client.open("/people/student1")
    client.find("tr.not_graded").find("td", text="-").require(1)


def test_no_not_graded_button_on_exam(client):
    # Log student in
    client.login("student1")

    # Start and submit exam attempt
    run_attempt(client, "internship1-project", submit=True)

    # Log teacher in and navigate to the submitted attempt
    client.login("teacher1")
    client.open('/curriculum/internship1-project?student=student1')

    # Validate that there is no 'Not graded' button
    client.find("input", value="Not graded").require(0)
    
    
def test_pause_twice(client):
    client.login("student1")
    run_attempt(client, "linux", start_anyway=False, start_approval=False, submit=False)
    run_attempt(client, "vars", start_anyway=False, start_approval=True, submit=False)

    client.open('/curriculum/queries')
    client.find(text="You are already working on another assignment and you already have a paused attempt.").require()


def test_pause_switch(client):
    client.login("student1")
    run_attempt(client, "linux", start_anyway=False, start_approval=False, submit=False)
    run_attempt(client, "vars", start_anyway=False, start_approval=True, submit=False)

    client.open('/curriculum/linux')
    client.find("input", name="switch-paused").submit()
    client.find("a", **{"href": "/curriculum/linux", "data-tooltip": "Current lesson"}).require()

    client.open('/curriculum/vars')
    client.find("input", name="switch-paused").submit()
    client.find("a", **{"href": "/curriculum/vars", "data-tooltip": "Current lesson"}).require()


def test_pause_exam(client, _db):
    client.login("student1")
    run_attempt(client, "lms-project", start_anyway=False, start_approval=True, submit=False)

    client.open('/curriculum/linux')
    client.find(text="You cannot start another assignment while you are working on an exam.").require()


def test_active_module_limit(client):
    client.login("student1")
    # Start, finish and grade the linux topic (should not count as active)
    run_attempt(client, "linux", submit=True, grade="passed")

    # Start 3 other modules
    run_attempt(client, "vars", submit=True, start_approval=False)
    run_attempt(client, "indico", submit=True, start_approval=True)
    run_attempt(client, "html", submit=True, start_approval=False)

    # Verify that we can't start another without approval
    client.open('/curriculum/queries')
    client.find(text="You're already working on 3 modules.").require()
    client.find("input", value="Request teacher approval").require()

    # Finish one of the 3 modules (by passing the exam)
    run_attempt(client, "lms-project", submit=True, start_approval=True, grade='passed')

    # Check that we can now start another module without consent
    run_attempt(client, "ip", submit=True, start_approval=False)

    # Test that we can start a fourth module with approval
    client.open('/curriculum/queries')
    client.find(text="You're already working on 3 modules.").require()
    run_attempt(client, "queries", submit=True, start_approval=True, grade='passed')

    # Test that we can start a second node within an active module
    run_attempt(client, "joins", submit=True, start_approval=False)

    # Test that we cannot start a fifth module without approval
    client.open('/curriculum/flask')
    client.find(text="You're already working on 4 modules.").require()


def test_retry_attempt(client):
    client.login("student1")
    run_attempt(client, "linux", submit=True, start_approval=False, start_anyway=False, grade="failed")
    run_attempt(client, "linux", submit=True, start_approval=False, start_anyway=False, grade="passed")


def test_retract_approval(client):
    client.login("student1")

    client.open("/curriculum/lms-project")
    client.find("input", value="Request teacher approval").submit()
    client.find(text="Go find a teacher to approve the assignment!").require()

    client.find("input", value="Retract approval request").submit()

    client.open("/curriculum/lms-project")
    client.find("input", value="Request teacher approval").require()


def find_objective(client, objective_id: int):
    return client.find('.objective') \
        .containing(attributes={"data-pos": objective_id}) \
        .require(1)


def test_checkbox_for_objectives(client):
    client.login("student1")
    run_attempt(client, "linux", submit=False)
    
    client.put('/mark_objective/', no_html=True, data=json.dumps({
        "id": 1,
        "checked": True
    }))

    client.open("/curriculum/linux")
    find_objective(client, 1).find('.completed-objective').require(1)

    client.put('/mark_objective/', no_html=True, data=json.dumps({
        "id": 1,
        "checked": False
    }))

    client.open("/curriculum/linux")
    find_objective(client, 1).find('.completed-objective').require(0)

    run_attempt(client, "linux", start=False, submit=True)
    client.find('.checkbox').require(0)
