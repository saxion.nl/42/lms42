from lms42.models.registration_templates import RegistrationTemplate
from lms42.models.user import User
import datetime


IN_A_WEEK = datetime.date.today() + datetime.timedelta(days=7)


def create_template(client, group_id="e24a", date=IN_A_WEEK, count=1):
    """Creates a registration template

    Args:
        client (FlaskClient): The Flask test client used to send the request.
        group_id (str, optional): group_id for the template. Defaults to "e24a".
        date (str, optional): date the template will expire on. Defaults to IN_A_WEEK.
        count (int, optional): How many templates to make. Defaults to 1
    """
    for _ in range(count):
        client.open("/registration_templates/create")
        client.find("select", name="group_id").require()
        client.find("input", name="date").require()
        client.find("input", type="submit", value="Save").submit(group_id=group_id, date=date)


def unprivileged_entry(client, url):
    """Opens the url and expects to be blocked and required to login

    Args:
        client (FlaskClient): The Flask test client used to send the request.
        url (str): url to try
    """
    client.open(url)
    client.find(in_text="Please log in to access this page.").require()


def require_templates(client, count):
    """Open the templates overview page and expects 'count' templates

    Args:
        client (FlaskClient): The Flask test client used to send the request.
        count (int): The number of templates to expect.
        filters (dict, optional): Dictionary that accepts filters for group_id and expired. Defaults to None.
    """
    client.open("/registration_templates")
    client.find("main").find(".card").require(count)


def create_user(client, template, user):
    """Creates a user based on given template

    Args:
        client (FlaskClient): The Flask test client used to send the request.
        template (RegistrationTemplate): The template to create a user from
        user (dict): The user data to use for the user
    """
    client.open(f"/register/{template.secret}")
    client.find(
        "form",
        action=f"/register/{template.secret}"
    ).submit(
        first_name=user["first_name"],
        last_name=user["last_name"],
        email=user["email"],
        english=user["english"],
        student_number=user["student_number"]
        )
 

def test_create_registration_template_privileged(client):
    # login
    client.login("teacher1")

    # confirm button in the corner of groups
    client.open("/groups")
    client.find(text="Self sign-up").require(1)

    # confirm the create button
    client.open("/registration_templates")
    client.find("a", in_text="Create registration template").require()

    # create template
    create_template(client)
    
    # confirm template in overview
    require_templates(client, 1)


def test_register_from_template(client):
    # login as teacher
    client.login("teacher1")

    # create and get template
    create_template(client)
    tmp = RegistrationTemplate.query.first()

    # get all users for checking after
    users = User.query.all()

    # register a user with template
    client.open(f"/register/{tmp.secret}")
    client.find("form", action=f"/register/{tmp.secret}").submit(first_name="test_user", last_name="0", email="test_user0@example.com", english='False', student_number=210456)
    
    # confirm that there is an extra entry in DB
    assert len(User.query.all()) == len(users) + 1

    # confirm new account can login
    client.login("test_user0")
    
    # confirm new student is in people
    client.open("/people")
    client.find("div.people").find(text="test_user").require()


def test_edit_registration_template(client):
    # Login as teacher and create template
    client.login("teacher1")
    create_template(client)

    # confirm template is created
    require_templates(client, 1)

    # go to the edit page of template
    tmp = RegistrationTemplate.query.first()
    client.open(f"/registration_templates/{tmp.id}/edit")
    
    # submit the edited form
    client.find("select", name="group_id").require()
    client.find("input", name="date").require()
    client.find("input", type="submit", value="Save").submit(group_id="d24a", date=datetime.date.today())

    # find the template and check the new values
    client.open("/registration_templates")
    template_div = client.find(".card")
    template_div.find(in_text=str(datetime.date.today())).require()
    template_div.find(in_text="d24a").require()


def test_delete_registration_template(client):
    # login as teacher
    client.login("teacher1")

    # create 4 templates and confirm if they exists
    create_template(client, count=4)
    require_templates(client, 4)

    # get the template and delete it
    tmp = RegistrationTemplate.query.first()
    client.open(f"/registration_templates/{tmp.id}/delete")

    # confirm the delete message for the template showed up
    client.find(in_text=f"Registration template {tmp.id} deleted.")
    
    # confirm only 3 of the 4 templates remain
    require_templates(client, 3)


def test_registration_duplicate_short_name(client):
    # login and create template
    client.login('teacher1')
    create_template(client)

    # get template and create users
    tmp = RegistrationTemplate.query.first()
    for num in range(5):
        user = {
            "first_name": "test",
            "last_name": "van laar",
            "email": f"testvl{num}@example.com",
            "english": 'False',
            "student_number": num + 100_200
        }
        create_user(client, tmp, user)

    # confirm users are present with different short names
    client.open("/people")
    client.find(text="test").require()
    client.find(text="testvl").require()
    client.find(text="test2").require()
    client.find(text="test3").require()
    client.find(text="test4").require()


def test_registration_template_unprivileged(client):
    # login as student
    client.login("student1")

    # try going to the template links
    for url in ["", "/create", "/1/edit", "/1/delete"]:
        unprivileged_entry(client, "/registration_templates" + url)
