import re


def test_request_example(client):
    response = client.get("/")
    assert b"Welcome to the learning management system" in response.data


def test_invalid_login(client):
    response = client.post("/user/login", data=dict(
        email="nosuchuser@example.com",
    ))
    assert b"This address is not registered." in response.data
    assert b"User profile" not in response.data


def test_login(client):
    email = "teacher1@example.com"
    response = client.post("/user/login", data=dict(
        email=email,
    ))
    
    assert f"A login link has been sent to: {email}.".encode("utf-8") in response.data

    with open("/tmp/lms42-email.0") as file:
        data = file.read()
        print("data", data)
        login_path = re.search(r"https?://[^/]+(/.*)", data).group(1)

    response = client.get(login_path, follow_redirects=True)

    assert b"User profile" in response.data


def test_authorization(client):
    client.open("/dashboard")
    client.find(in_text="Select a query above.").require(0)
    client.find("input", value="Send login link").require()
    client.login("student1")
    client.open("/dashboard")
    client.find(in_text="Select a query above.").require(1)
