def add_teacher_only_query(client):
    client.login("admin")
    client.open("/dashboard/new")
    
    client.find("input", value="Save").submit(name="Student Not Viewable Query", sql='select * from "user";', explanation="Test query", student_viewable="false")


def add_student_query(client):
    client.login("admin")
    client.open("/dashboard/new")
    
    client.find("input", value="Save").submit(name="Student Viewable Query", sql='select * from "user";', explanation="Test query", student_viewable="true")


def test_student_no_access(client):
    add_teacher_only_query(client)
    client.login("student1")
    client.get("/dashboard/1")
    client.find("h1", text="Stop right there!").require(1)


def test_student_has_access(client):
    add_student_query(client)
    client.login("student1")
    client.get("/dashboard/2")
    client.find(".notification", text="Test query").require(1)


def test_query_not_visible_for_student(client):
    add_student_query(client)
    add_teacher_only_query(client)
    client.login("student1")
    client.get("/dashboard")
    client.find("option", text="Student Not Viewable Query").require(0)
    client.find("option", text="Student Viewable Query").require(1)
