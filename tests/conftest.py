import re
import pytest
import os
import inspect
import json
import random
import string
import shutil
import alembic
from db_magic import _transaction
import html5lib
import flask
import atexit
from glob import glob
from tree_set import TreeSet
from common import run_attempt
import debugpy

# Causes email to be written to /tmp/lms42-email.0
os.environ['SMTP_HOST'] = ''
os.environ["ALLOW_TILDE_LOGIN"] = "yes"

# Create a new data directory and database for each test session
# TODO: while the database uses rollbacks to reset between individual tests
# the rest of the data directory does not. It probably should, somehow.
data_dir = "tests/data/" + "".join([random.choice(string.ascii_lowercase) for _ in range(8)])
os.environ["DATA_DIR"] = data_dir

# Always delete our private data directory when the main test process exits.
main_pid = os.getpid()
atexit.register(lambda: shutil.rmtree(data_dir) if os.getpid()==main_pid else None)


# Import lms42.app *after* we have set the environment variables.
from lms42.app import app, db
import lms42.run

# Run all migrations
alembic_cfg = alembic.config.Config()
alembic_cfg.set_main_option('script_location', "migrations/")
alembic_cfg.set_main_option('sqlalchemy.url', app.config["SQLALCHEMY_DATABASE_URI"])
alembic.command.upgrade(alembic_cfg, 'head')
# with alembic.context.begin_transaction():
#     alembic.context.run_migrations()

# Create a html5 parser for validating all test results
html_parser = html5lib.HTMLParser(namespaceHTMLElements=False)

# Make sure the responses dir exists and is empty
os.makedirs("tests/responses", exist_ok=True)
for filename in glob("tests/responses/*"):
    os.unlink(filename)


class TestClient(flask.testing.FlaskClient):
    """A custom FlaskClient that adds:
    - Logging into the site with a single statement (`login`).
    - Checking that responses are valid HTML5.
    - Writing response documents to tests/responses/ for debugging.
    - And, by means of TreeSet, documented in `tree_set.py`:
        - Exposing the HTML5 tree from the last response (through `tree`).
        - Submitting a form within the last response (`TreeSet.submit`).
        - Following a link within the last response (`TreeSet.open`).
    """
    request_id = 0

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.tree = None
        self.__last_path = None
        self.__last_query_string = None
        self.user_name = None

    def open(self, *args, follow_redirects=True, as_tuple=False, no_html=False, **kwargs):
        """
        Opens a link and provide a navigable tree

        Args:
            follow_redirects (bool, optional): Follow 301,302 links. Defaults to True.
            as_tuple (bool, optional): Return (environ, response). Defaults to False.
            no_html (bool, optional): Don't require a valid HTML5 response. Defaults to False.

        Raises:
            AssertionError: HTML5 parsing error

        Returns:
            Response: flask.Response
        """
        environ, response = super().open(*args, follow_redirects=follow_redirects, as_tuple=True, **kwargs)

        self.tree = None
        self.__last_path = None
        self.__last_query_string = None

        if isinstance(response, flask.wrappers.Response):

            if args and type(args[0]) is str:
                method = kwargs.get('method', 'GET')
                TestClient.request_id += 1

                filename = f"tests/responses/{TestClient.request_id}"

                with open(filename + '.html', 'wb') as file:
                    file.write(response.data)

                stack = []
                
                for frame in inspect.stack():
                    # stop if the stack runs into internal files
                    if "pytest" in frame.filename:
                        break

                    # stop if the frame runs into open
                    if frame.function == "open":
                        continue

                    try:
                        function_body = inspect.getsourcelines(frame.frame)
                    except OSError:
                        continue

                    stack.append({
                        'file': frame.filename,
                        'line': frame.lineno,
                        'function': frame.function,
                        'function_body': function_body[0],
                        'starting_line': function_body[1]
                    })

                with open(filename + '.json', 'w') as file:
                    file.write(json.dumps(stack, indent=4))

                print(method, args[0], kwargs.get('data', {}), "->", f"[{response.status_code}]", filename)

            if response.status_code not in {301, 302} and not no_html:
                self.__last_path = environ['PATH_INFO']
                self.__last_query_string = environ.get('QUERY_STRING')
                self.tree = TreeSet(self, html_parser.parse(response.data))
                for loc, code, error_args in html_parser.errors:
                    raise AssertionError(f"HTML5 error at {loc[0]}:{loc[1]}: {html5lib.constants.E[code] % error_args}")

        return (environ, response) if as_tuple else response
    

    def resolve_url(self, url=''):
        assert self.__last_path
        if not url:
            url = self.__last_path
            if self.__last_query_string:
                url += '?' + self.__last_query_string
        elif url is not None and "://" not in url and not url.startswith("/"):
            path = self.__last_path.split("/")
            path.pop()
            for part in url.split('/'):
                if part == '.':
                    pass
                elif part == '..':
                    if len(path) > 0:
                        path.pop()
                else:
                    path.append(part)
            url = path.join('/')
        return url

    def login(self, name):
        response = self.post("/user/login", data={
            "email": f"~{name}@example.com",
        }, follow_redirects=True)

        assert b"User profile" in response.data
        self.user_name = name

    def find(self, *args, **kwargs):
        assert self.tree
        return self.tree.find(*args, **kwargs)
        

if os.getenv("SD42_DEBUG") and not debugpy.is_client_connected():
    debugpy.listen(("0.0.0.0", 5678))  # Choose an appropriate port
    print("Waiting for debugger to attach...")
    debugpy.wait_for_client()


NUM_TEST_TEACHERS = 3
NUM_TEST_STUDENTS = 12
app.test_client_class = TestClient


@pytest.fixture(scope='session', autouse=True)
def test_app():
    """This autouse fixture fills the (freshly created) database with
    a couple of user accounts (admin@example.com, student[0-NUM_TEST_STUDENTS]@example.com
    and teacher[0-NUM_TEST_TEACHERS]@example.com)."""
    app.config.update({
        "TESTING": True,
        "WTF_CSRF_CHECK_DEFAULT": False,
        "WTF_CSRF_ENABLED": False,
    })

    client: TestClient = app.test_client() # type: ignore

    response = client.post("/user/login", data={
        "email": "admin@example.com",
    }, follow_redirects=True)

    assert b"User profile" in response.data

    for nr in range(NUM_TEST_TEACHERS):
        response = client.post("/people/new", data={
            "first_name": f"Teacher{nr}",
            "last_name": "",
            "email": f"teacher{nr}@example.com",
            "short_name": f"teacher{nr}",
            "level": 50,
            "is_active": "y",
            "bke_certification": "" if nr == 2 else "y",
        }, follow_redirects=True)
        assert b"Impersonate" in response.data # Verify that we landed on the user's profile page

    client.open("/groups")
    client.find(id="add_group").submit()
    client.find(id="add_group").submit()
    response = client.find("input", name="save").submit(**{
        "groups-0-name": "e24a",
        "groups-0-study_coach_id": "teacher0",
        "groups-0-monday_teacher_id": "teacher0",
        "groups-0-tuesday_teacher_id": "teacher1",
        "groups-1-name": "d24a",
        "groups-1-study_coach_id": "teacher1",
        "groups-1-monday_teacher_id": "teacher1",
        "groups-1-tuesday_teacher_id": "teacher0",
    })

    assert b"Search by name" in response.data # Verify that we landed on the people page

    for nr in range(NUM_TEST_STUDENTS):
        client.open("/people/new")
        response = client.find("input", value="Save").submit(
            first_name=f"Student{nr}",
            last_name="",
            email=f"student{nr}@example.com",
            short_name=f"student{nr}",
            group_id="e24a" if nr < NUM_TEST_STUDENTS // 2 else "d24a"
        )

        assert b"Impersonate" in response.data # Verify that we landed on the user's profile page

    for nr in range(NUM_TEST_STUDENTS):
        client.login(f"student{nr}")
        run_attempt(client, "intro", start_approval=False, start_anyway=False, submit=True, grade="passed")

    return app


@pytest.fixture()
def _db(scope='session'):
    """Required by pytest-flask-sqlalchemy."""
    return db

@pytest.fixture(autouse=True)
def enable_transactional_test(_transaction):
    """Have pytest-flask-sqlalchemy run each test within a transaction, which is
    reverted at the end."""
    org_session = db.session
    db.session = _transaction[2]
    try:
        yield
    finally:
        db.session = org_session

@pytest.fixture()
def db_session(enable_transactional_test):
    return db.session

@pytest.fixture()
def client(test_app):
    """Fixture that allows tests to simulate requests to a fake lms42 server."""
    return test_app.test_client()


def pytest_sessionfinish():
    with open("tests/responses/max_id.txt", 'w') as file:
        file.write(f"{TestClient.request_id}")
