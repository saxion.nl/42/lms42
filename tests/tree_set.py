from __future__ import annotations
from xml.etree import ElementTree


def find(element, query, results, *, text=None, in_text=None, attributes=None, is_=None, recursive=True, match_self=False, limit=None, **kwargs):
    if match_self:
        element = [element]

    for child in element:
        if recursive:
            find(child, query, results, text=text, in_text=in_text, attributes=attributes, is_=is_, recursive=recursive, limit=limit, **kwargs)

        if limit!=None and len(results) >= limit:
            return
        
        if attributes and not all(child.get(k, '') == str(v) for k, v in attributes.items()):
            continue

        if text != None or in_text != None:
            element_text = child.text or ''
            for grandchild in child:
                element_text += grandchild.tail or ''

            if text != None and element_text.strip() != text:
                continue
            if in_text != None and in_text not in element_text:
                continue

        if is_ != None and is_ is not child:
            continue
        # combination of id and tag returns all tags. The combination tag#id or tag.class is NOT more specific
        if query != None:
            query_without_id, *id_list = query.split('#', 2)
            if id_list and id_list[0] != child.get('id', ''):
                continue
            tag, *classes = query_without_id.split('.')
            if tag != '' and tag != child.tag:
                continue

            actual_classes = child.get('class','').split(' ')
            if any((cls for cls in classes if cls not in actual_classes)):
                continue

        for name,value in kwargs.items():
            # no default because otherwise you cannot test for attributes with an empty string value such as selected or checked
            if child.get(name) != value:
                break # this will skip the else
        else: # this is a match!
            results.add(child)


class TreeSet:
    def __init__(self, client, element_set: set[ElementTree] | ElementTree):
        self.client = client
        self.element_set: set[ElementTree] = element_set if isinstance(element_set, set) else {element_set}

    def find(self, query=None, *, limit=None, **kwargs):
        """Search through the (children of) the elements in the current set, delivering a
        new TreeSet with the results.

        Args:
            query (str, optional): String describing the desired tag name, css classes and id for a single
                element, in the format known from CSS selectors: `tag_name.first_class.another_class#the_id`.
            text (str, optional): Text that should exactly match the text of an element (after whitespace has
                been stripped), not including text contained by its children.
            in_text (str, optional): Text that should be contained within the text of an element, not
                including text contained by its children.
            recursive (bool, optional): When `True` (the default), recursively search all descendants.
            match_self (bool, optional): When `False` (the default), the elements in the current TreeSet
                will not attempt to match, only their children/descendants.
            limit (int, optional): The maximum number of elements to include in the set. Throws an exception if there are more.

        Returns:
            TreeSet: A new TreeSet containing the resulting elements.
        """

        results = set()
        for element in self.element_set:
            find(element, query, results, **kwargs)
        assert not limit or len(results) <= limit
        return TreeSet(self.client, results)

    def __iter__(self):
        """Iterate the elements of the TreeSet."""
        yield from self.element_set

    def containing(self, query=None, **kwargs):
        """Filter the result set, returning only those elements that contain at least one element
        matching the arguments.

        Args:
            The same as for `find`.

        Returns:
            TreeSet: A new TreeSet containing the filtered set of elements.
        """
        results = set()
        for element in self.element_set:
            element_results = set()
            find(element, query, element_results, **kwargs)
            if element_results:
                results.add(element)
        return TreeSet(self.client, results)

    def __add__(self, other):
        """Returns:
            TreeSet: A new TreeSet that has all the elements of the two original
                TreeSets combined.
        """
        assert self.client == other.client
        return TreeSet(self.client, set.union(self.element_set, other.element_set))

    def __bool__(self):
        """Returns true when the TreeSet is not empty."""
        return len(self.element_set) > 0
    
    def __len__(self):
        """Returns the length of the element_set"""
        return len(self.element_set)

    def require(self, count=1):
        """Raises an exception when there is an unexpected number of elements in the set.

        Args:
            count (int|bool, optional): The expected number of elements. Defaults to 1.
                When True, any number larger than 0 is accepted.

        Returns:
            TreeSet: Self.
        """
        if count is True:
            if len(self.element_set) == 0:
                raise AssertionError("Found 0 elements but expected at least 1")
        else:  # noqa: PLR5501
            if len(self.element_set) != count:
                raise AssertionError(f"Found {len(self.element_set)} elements but expected {count}")
        return self

    def get(self):
        """Asserts that there is just one element in the set, and returns it.
        """
        self.require()
        for el in self.element_set:
            return el
        raise AssertionError()

    def open(self):
        """Asserts that there is just one element in the set and that it is an <a>, 
        and simulates opening the target page for that link.

        Raises:
            AssertionError: No <a> in result set.

        Returns:
            Response: A test client response object for the new page request.
        """
        el = self.get()
        assert el
        if el.tag != 'a':
            raise AssertionError("<a> expected")
        url = self.client.resolve_url(el.get('href',''))
        return self.client.get(url)

    def submit(self, **data):
        """Submit a <form> that was on the last page the client opened.

        The TreeSet must contain a single element that is either a <form> or any element within it.
        If the element is an <input type="submit"> , the form will be submitted as if that button
        was pressed.

        Args:
            **data: Name/value pairs that will be used to submit form data. If no data is specified for a
                given input name, its default value will be submitted.

        Raises:
            AssertionError: No form or invalid data.
        """

        self.require()
        form_set = self
        if form_set.get().tag != "form":
            form_set = self.client.tree.find("form").containing(is_=self.get())
        form_el = form_set.get()

        clean_data = {}
        for input_el in form_set.find("input") + form_set.find("textarea") + form_set.find("select") + form_set.find("button"):
            name = input_el.get('name', None)
            if name is None:
                continue

            html_value = input_el.get('value', '')
            if input_el.tag == "input":
                input_type = input_el.get('type', '').lower()
                if input_type == 'submit':
                    if input_el == self.get():
                        clean_data[name] = input_el.get('value')
                    continue
                if input_type == 'radio':
                    if name in data:
                        if html_value != data[name]:
                            continue
                    elif input_el.get('checked') is None:
                        continue
                elif input_type == 'checkbox':
                    html_value = '' if input_el.get('checked') is None else html_value or 'y'

            elif input_el.tag == "textarea":
                html_value = input_el.get('text', '')

            elif input_el.tag == "select":
                if name in data:
                    if not TreeSet(self.client, input_el).find("option", value=data[name]):
                        opt = TreeSet(self.client, input_el).find("option", text=data[name])
                        if opt:
                            data[name] = opt.get().get('value')
                        else:
                            raise AssertionError(f"<select name={name}> has no <option value={data[name]}>")
                else:
                    html_value = None
                    for option in TreeSet(self.client, input_el).find("option"):
                        if option.get("selected") is not None:
                            html_value = option.get('value')

            elif input_el.tag == "button":
                if input_el == self.get():
                    clean_data[name] = input_el.get('value')
                continue

            if name in data:
                clean_data[name] = data[name]
                del data[name]
            elif name not in clean_data:
                clean_data[name] = html_value

        if data:
            raise AssertionError(f"non-existing field(s) in <form>: {', '.join(data.keys())}")

        url = self.client.resolve_url(form_el.get('action', ''))
        if form_el.get('method', '').lower() == "post":
            return self.client.post(url, data=clean_data)
        # method get
        return self.client.get(url, query_string=clean_data)

    def __str__(self):
        tags = {}
        for el in self.element_set:
            tags[el.tag] = tags.get(el.tag, 0) + 1
        return f"TreeSet{tags}"
