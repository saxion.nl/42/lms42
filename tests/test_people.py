from common import run_attempt, grade_attempt, set_grade_teacher
from lms42.models.attempt import Attempt


def test_donut_chart_privileged(client):
    # Login and open the people page of the user
    client.login('student1')
    client.open('/people/student1')

    # Check if the correct labels are in the graph
    for label in ["Passed: 0", "Awaiting grading: 0", "Failed: 0"]:
        client.find('.ct-donut-legend-label', text=label).require(1)

    # Do the same for the teacher
    client.login('teacher1')
    client.open('/people/student1')

    for label in ["Passed: 0", "Awaiting grading: 0", "Failed: 0"]:
        client.find('.ct-donut-legend-label', text=label).require(1)


def test_donut_graph_unprivileged(client):
    # Login and open the people page of another user
    client.login('student1')
    client.open('/people/student2')

    # Check if the ECTS with the correct class is not present
    client.find(text="ECTS").require(0)
    client.find(".ct-chart-donut").require(0)


def test_zero_ects_assignments_do_not_appear_in_donut_graph(client):
    # Login to the user
    client.login('student1')

    # Pass a 0 ects assignment
    run_attempt(client, "linux", submit=True, grade="passed")

    # Open the people page of the user
    client.open('/people/student1')

    # This line get's the content of the script tag because the svg has not been made here,
    # because that gets created by the javascript
    # Check that the donut chart does not show any ECTS's, Check that there is are three zero's
    client.find("script", in_text="[0, 0, 0, 120]").require(1)


def test_correct_values_in_donut_graph(client):
    # Login to the user
    client.login('student1')

    # Submit a exam
    run_attempt(client, "lms-project", submit=True)

    # Pass a exam
    run_attempt(client, "ux-exam", submit=True, grade="passed")

    # Fail a exam
    run_attempt(client, "android-project", submit=True, grade="failed")

    # Go to the users page
    client.open("/people/student1")

    # This line get's the content of the script tag because the svg has not been made here,
    # because that gets created by the javascript
    # Check that the donut chart shows the correct amount of ECTS's
    client.find("script", in_text="[5, 5, 5, 105]").require(1)


def test_first_fail_then_pass_an_exam(client):
    # Login to the user
    client.login('student1')

    # Fail a new exam
    run_attempt(client, "ux-exam", submit=True, grade="failed")

    # Pass an attempt for that exam
    run_attempt(client, "ux-exam", submit=True, grade="passed")

    # To ensure that the order of submitting does not matter
    run_attempt(client, "lms-project", submit=True, grade="passed")
    run_attempt(client, "lms-project", submit=True, grade="failed")

    # Check if the correct values are in the donut script tag
    client.open("/people/student1")
    client.find("script", in_text="[10, 0, 0, 110]").require(1)


def test_first_pass_then_submit_an_exam(client):
    # Login to the user
    client.login('student1')

    # Pass a exam
    run_attempt(client, "ux-exam", submit=True, grade="passed")

    # Submit the same exam
    run_attempt(client, "ux-exam", submit=True)

    # Check if the correct values are in the donut script tag
    client.open("/people/student1")
    client.find("script", in_text="[5, 0, 0, 115]").require(1)


def test_first_fail_then_submit_an_exam(client):
    # Login to the user
    client.login('student1')

    # Fail a new exam
    run_attempt(client, "ux-exam", submit=True, grade="failed")
    
    # Submit an attempt for that exam
    run_attempt(client, "ux-exam", submit=True)

    # Check if the correct values are in the donut script tag
    client.open("/people/student1")
    client.find("script", in_text="[0, 5, 0, 115]").require(1)


def test_valid_bke_certification(client):
    # Student submits an exam
    run_attempt(client, "ux-exam", submit=True, student_name="student1")

    # Attempt at publishing a grade with a BKE certification
    grade_attempt(client, "ux-exam", 'student1', action="passed", teacher_name="teacher1")


def test_invalid_bke_certification(client):
    # Student submits an exam
    run_attempt(client, "ux-exam", submit=True, student_name="student1")

    # Attempt publishing a grade without a BKE certification
    try:
        grade_attempt(client, "ux-exam", 'student1', action="passed", teacher_name="teacher2")
    except AssertionError:
        pass # test passed
    else:
        raise AssertionError("Teacher is not supposed to grade exams without a BKE certificate")


def test_contributor_badge(client):
    client.login('admin')

    response = client.post("/people/new", data={
            "first_name": "Frank",
            "last_name": "van Viegen",
            "email": "teacher10@example.com",
            "short_name": "frank",
            "location": "",
            "level": 50,
            "is_active": "y",
        }, follow_redirects=True)

    assert b"Impersonate" in response.data # Verify that we landed on the user's profile page
    assert b"Contributor" in response.data


def test_filter_presence(client):
    client.login("teacher1")
    client.open("/time_log/ping", no_html=True)
    client.open("/people?present=true")
    client.find("a", text="teacher1").require(1)
    client.find("a", text="student1").require(0)

    client.login("student1")
    client.open("/time_log/ping", no_html=True)
    client.open("/people?present=true")
    client.find("a", text="student1").require(1)
    client.find("a", text="teacher1").require(1)


def test_filter_group(client):
    client.login("student1")
    
    client.open("/people?group=e24d")
    client.find("h2", text="Empty!").require()

    client.open("/people?group=e24a")
    client.find("a", text="student0").require(1)


def test_filters_teacher_only(client):
    client.login("admin")
    client.open("/people/student0")
    client.find("input.is-primary", value="Save").submit(is_active='false')
    client.open("/people/student1")
    client.find("input.is-primary", value="Save").submit(is_hidden='true')

    client.login("student2")
    client.open("/people?inactive=n&hidden=y")
    client.find("a", text="student1").require(0)
    client.open("/people?inactive=y&hidden=n")
    client.find("a", text="student0").require(0)

    client.login("teacher1")
    client.open("/people?inactive=n&hidden=y")
    client.find("a", text="student1").require(1)
    client.open("/people?inactive=y&hidden=n")
    client.find("a", text="student0").require(1)


def test_show_teacher_name_in_grade_column_for_ungraded_attempts(client):
    # Log in as student1 and submit attempt
    client.login("student1")
    run_attempt(client, "linux", submit=True)

    # Log in as teacher1 and assign teacher1 as grade_teacher
    client.login("teacher1")
    set_grade_teacher(client, "student1", "linux", "teacher1")

    # Verify that teacher name is in the row of the attempt that needs grading
    client.login("student1")
    client.open("/people/student1")
    client.find("tr.needs_grading").find("td", in_text="teacher1").require(1)

    # Grade the attempt
    client.login("teacher1")
    grade_attempt(client, "linux", "student1")

    # Verify that the now graded attempt does not contain the teacher name anymore
    client.login("student1")
    client.open("/people/student1")
    client.find("tr.needs_grading").find("td", in_text="teacher1").require(0)
